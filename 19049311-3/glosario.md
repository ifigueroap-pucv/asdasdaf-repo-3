### Glosario

A continuación se presentarán algunos conceptos básicos o terminos tecnicos para el uso adecuado de Git.

|Termino  | Definición |
| ------ | ------ |
| Control de versiones (VC)| Sistema capaz de registrar los cambios realizados por uno o más usuarios sobre un archivo o documento en ciertos instantes de tiempos, de modo que se puedan recuperar las versiones anteriores cuando se necesiten. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) |
| Control de versiones distribuido (DVC) | El sistema de control de versiones distribuido se encarga de replicar todo el repositorio de un servidor en algún otro servidor que estaba colaborando con él en caso de que ocurra algún accidente o desastre en alguno de los servidores que el usuario utiliza. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)  |
| Repositorio remoto y Repositorio local | Los repositorios remotos son versiones actuales y anteriores que están guardadas en internet, la lectura y escritura de cada una de las versiones dependerá de los permisos que el usuario tenga. Los repositorios locales son aquellos que se encuentran en el directorio local de la estación de trabajo en la cual se encuentra el usuario, en ella estarán las versiones de trabajo, pero solo estarán en la estación de trabajo hasta que el usuario se encargue de subirlas a la estación remota. [Fuente]( https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)|
| Copia de trabajo / Working Copy | Es la copia del trabajo en la cual en ella se realizarán los cambios. [Fuente](https://www.osmosislatina.com/cvs_info/wcopy.htm)|
| Área de Preparación / Staging Area | Es un archivo contenido en el directorio de Git, se encarga de almacenar información de lo que se va a subir al directorio remoto. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio) |
| Preparar Cambios / Stage Changes | Etapa en la cual se realizan cambios sin hacer una modificación en el archivo base. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio) |
| Confirmar cambios / Commit Changes | Etapa mediante la cual se sube el archivo que se estaba modificando, reemplazando el archivo base que estaba en la nube. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio) |
| Commit | Un commit se encarga de confirmar e indicar los cambios que fueron hechos en un archivo. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio) |
| Clone | se encarga de copiar todo el repositorio, incluyendo las ramas y los commit. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio) |
| Pull | Se encarga de recibir los cambios realizados por otros usuarios. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) |
| Push | Se encarga de gestionar y enviar la información modificada a un repositorio remoto.[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) |
| Fetch | Se encarga de traer los cambios, pero los deja en otra rama. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) |
| Merge | se encarga de fusionar ramas o trabajos y busca resolver conflictos entre archivos. [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar) |
| Status | Ayuda a determinar a saber en qué estados se encuentran archivos o directorios que se están utilizando. [Fuente](https://git-scm.com/docs/git-status) |
| Log | Sirve para ver el historial completo de todas las modificaciones realizadas por el usuario, pero solo el de los archivos confirmados. [Fuente](https://git-scm.com/book/es/v1/Los-entresijos-internos-de-Git-Referencias-Git) |
| Checkout | Se encarga de mover el apuntador HEAD de una rama a otra. [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F) |
| Rama / Branch | Se refiere a las líneas de desarrollo, ayudando a gestionar las modificaciones en paralelo de distintas partes de un mismo archivo sin interferir con las de otros. [Fuente](https://es.wikipedia.org/wiki/Control_de_versiones#Ramas_puntuales) |
| Etiqueta / Tag | Se utiliza principalmente para marcar un punto importante de avance en los proyectos, como por ejemplo una versión funcional del sistema. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas) |
