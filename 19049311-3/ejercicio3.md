
# Ejercicio 3

-Cantidad y nombre de los branches:
```sh
1 Branch llamado master.
```

Cantidad y nombre de las etiquetas:
```sh
No existen etiquetas.
```

Nombre, mensaje y código hash de los últimos 3 commits.:
```sh
Nombre: Miguel Jorquera <miguel.jorquera.r@mail.pucv.cl>
Mensaje: Se agrega el glosario al repositorio
Código ash: e2f14024259ae1fe0a58a41880e70a86fe224653
```
```sh
Nombre:Andres Carcamo <carcamo_8@hotmail.com>
Mensaje: Corrige error en el usuario bla explicacion blaaaaa
Código ash: a8fba2d17ae531ad2a9f689b378b4484608d4947
```
```sh
Nombre: Andres Carcamo <carcamo_8@hotmail.com>
Mensaje: Se agregan los datos pedidos bla bla bla tercera explicacion bla bla bla
Código ash:5e39681816081c5553a5fbd3697c3fdf9c193954
```