a. Cantidad y nombre de los branches

| Branch   | Nombre       |
| -------- | ------------:|
|  1       | master       |

b. Cantidad y nombre de las etiquetas

| Tag      | Nombre       |
| -------- | ------------:|
|          |              |

... No exist�an etiquetas.

c. Nombre, mensaje y c�digo hash de los �ltimos 3 commits

| C�digo                                   | Nombre            | Mensaje                                              |
| ---------------------------------------- | :----------------:| ----------------------------------------------------:|
| 0868f919ef858c15aafe99f66e6cacf4d797f82f | Fernando Carvajal | Glosario Agregado                                    |
| ea9092c5d9acc18173af9ae7157462ec55cf593e | David Erazo       | Se agrego archivo datos en la carpeta 18788484-5     |
| d71e962c9e36aa5b17d2c8dcc0dabea0b9d555ea | David Erazo       | Se agrego archivo ejercicio3 en la carpeta 18788484-5|