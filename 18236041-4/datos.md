a. Nombre y RUT

| Nombre Completo                  | RUT          |
| -------------------------------- |-------------:|
| Fernando Adolfo Carvajal Morales | 18.236.041-4 |

b. Cuenta de usuario Bitbucket
... fcarvajalm.

c. Correo electrónico asociado a cuenta Bitbucket
... fernandocarvajal18@gmail.com.