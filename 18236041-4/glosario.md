1. Control de versiones (VC):
... Consiste en un sistema capaz de registrar cambios realizados en un archivo o un conjunto de estos, posibilitando recuperar versiones espec�ficas m�s adelante.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

2. Control de versiones distribuido (DVC):
... Corresponde a un tipo VC en el cual cada vez que un cliente descarga la �ltima instant�nea de los archivos (replicaci�n completa del repositorio), a la vez se hace una copia de seguridad completa de todos los datos. Es decir, si uno d elos servidores muere, se puede restaurar copiando cualquiera de los repositorios de los clientes.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

3. Repositorio remoto y Repositorio local:
... Los repositorios remotos constan de versiones de un proyecto alojadas en internet o alg�n punto d ela red. Por otro lado, los repositorios locales son aquellos almacenados en la m�quina en la cual se est� trabajando. T�cnicamente su diferencia radica en su lugar de almacenamiento.
[Fuente 1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
[Fuente 2](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

4. Copia de trabajo / Working Copy:
... Un Working Copy es una copia de lo que se est� realizando actualmente, lo que se est� desarrollando.
[Fuente](https://stackoverflow.com/questions/581220/what-is-a-working-copy-and-what-does-switching-do-for-me-in-tortoise-svn) 

5. �rea de Preparaci�n / Staging Area:
... Es un �rea intermedia de almacenamiento utilizada en el procesamiento de los datos durante procesos ETL.
[Fuente](https://en.wikipedia.org/wiki/Staging_(data))

6. Preparar Cambios / Stage Changes:
... Consiste simplemente en preparar el archivo junto con los cambios realizados en �l para luego realizar un commit.
[Fuente](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)

7. Confirmar cambios / Commit Changes
... Es el paso siguiente al Stage Changes. Luego de preparar los cambios realizados, estos se confirman como correctos y suben al repositorio remoto. 
[Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

8. Commit:
... Instrucci�n que permite realizar la confirmaci�n de los cambios realizados y enviarlos al repositorio remoto.
[Fuente](https://es.wikipedia.org/wiki/Commit)

9. clone:
... Instrucci�n que permite clonar un repositorio Git existente en otro servidor a la m�quina con la que se est� trabajando.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git) 

10. pull:
... Es un comando que descarga el contenido de un repositorio remoto y actualiza el contenido del repositorio local para hacer coincidir los contenidos de ambos.
[Fuente](https://www.atlassian.com/git/tutorials/syncing/git-pull)

11. push:
... Es un comando que sube el contenido de un repositorio local hacia un repositorio remoto.
[Fuente](https://www.atlassian.com/git/tutorials/syncing/git-push)

12. fetch:
... Es un comando que descarga commits, archivos, referencias, etc. desde un repositorio remoto a nuestro repositorio local. Es utilizado para ver en qu� han estado trabajando los dem�s colaboradores de tu proyecto.
[Fuente](https://www.atlassian.com/git/tutorials/syncing/git-fetch)

13. merge:
... Es un comando que permite tomar las distintas l�neas de desarrollo creadas al trabajar y unirlas todas en una �nica rama.
[Fuente](https://www.atlassian.com/git/tutorials/using-branches/git-merge) 

14. status:
... Comando que revisa el estado actual de un determinado repositorio.
[Fuente](https://githowto.com/checking_status)

15. log:
... Comando que permite revisar el historial de commits realizados dentro del repositorio.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones) 

16. checkout:
... Permite cambiar entre las diferentes versiones de una determinada entidad. El comando checkout opera entre 3 distintas entidades: archivos, commits y ramas.
[Fuente](https://www.atlassian.com/git/tutorials/using-branches/git-checkout)

17. Rama / Branch:
... Pueden ser vistas como una nueva linea o un nuevo contexto de desarrollo dentro de un proyecto.Son una forma de solicitar un nuevo directorio de trabajo, Staging area, etc.
[Fuente 1](https://www.atlassian.com/git/tutorials/using-branches)
[Fuente 2](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms#Important_Terms)

18. Etiqueta / Tag:
... Corresponden a referencias de puntos espec�ficos dentro del histial de Git. Generalmente son utilizadas para marcar puntos donde se han lanzado nuevas versiones del proyecto.
[Fuente 1](https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag)
[Fuente 2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)