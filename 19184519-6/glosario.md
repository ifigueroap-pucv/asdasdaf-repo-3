1. Control de versiones (VC)

���Es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones espec�ficas m�s adelante.

[Fuente 1.](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) 	

2. Control de versiones distribuido (DVC)

���los clientes no s�lo descargan la �ltima instant�nea de los archivos: replican completamente el repositorio. As�, si un servidor muere, y estos sistemas estaban colaborando a trav�s de �l, cualquiera de los repositorios de los clientes puede copiarse en el servidor para restaurarlo. Cada vez que se descarga una instant�nea, en realidad se hace una copia de seguridad completa de todos los datos

[Fuente 2.](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) 	 

3. Repositorio remoto y Repositorio local
���Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en alg�n punto de la red. Puedes tener varios, cada uno de los cuales puede ser de s�lo lectura, o de lectura/escritura, seg�n los permisos que tengas. Colaborar con otros implica gestionar estos repositorios remotos, y mandar (push) y recibir (pull) datos de ellos cuando necesites compartir cosas.
���Los repositorios locales son versiones de tu proyecto que se encuentran alojados en tu computador.

[Fuente 3.](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
	
4. Copia de trabajo / Working Copy

���Una copia de trabajo es el directorio que contiene el directorio del .git
[Fuente 4.](https://git-annex.branchable.com/forum/comprehension_question__58___repository_vs._working_copy_in_direct_mode/)

5. �rea de Preparaci�n / Staging Area

���Es una de las formas en las que se puede almacenar data. Es b�sicamente un muelle de carga donde puedes determinar qu� cambios se despachan. Tambi�n hay acciones que se pueden realizar en el �rea de preparaci�n, como ocultar temporalmente sus cambios.
[Fuente 5.](http://gitready.com/beginner/2009/01/18/the-staging-area.html)

6. Preparar Cambios / Stage Changes

���Es el paso anterior al proceso de "commit".Es como un cache de archivos que quiero que pasen por el proceso de "commit"
[Fuente 6.](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)

7. Confirmar cambios / Commit Changes

���Es cuando se guardan efectivamente los cambios de un proyecto en el cual se est� trabajando. Estos se guardan en el repositorio.
[Fuente 7.](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

8. Commit

���Es un proceso que tiene como idea en confirmar cambios de forma permanente en un repositorio, adem�s, de documentar los cambios realizados.
[Fuente 8.](https://es.wikipedia.org/wiki/Commit)

9. clone

���Clonar es la forma de conseguir la informaci�n almacenada en un repositorio remoto.
[Fuente 9.](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

10. pull

���Es un comando que toma la informaci�n de un repositorio remoto en el cual estas trabajando y lo traes al repositorio local.
[Fuente 10.](https://git-scm.com/docs/git-request-pull)

11. push

���comando que hace los cambios en el repositorio.
[Fuente 11.](https://git-scm.com/docs/git-push)

12. fetch

���El comando fetch se encarga de descargar los commits, archivos y referencias desde un repositorio remoto al local.
[Fuente 12.](https://www.atlassian.com/git/tutorials/syncing/git-fetch)

13. merge

���Es un comando que fusiona ramas ajenas a la activa.
[Fuente 13.](https://www.atlassian.com/git/tutorials/svn-to-git-prepping-your-team-migration#basic-git-commands)

14. status

���Enlista los archivos a los cuales se les hicieron cambios y aquellos que todavia puedes agregar o hacer commit.
[Fuente 14.](https://www.atlassian.com/git/tutorials/svn-to-git-prepping-your-team-migration#basic-git-commands)

15. log

���Comando que muestra los registros del commit, cambios, etc.
[Fuente 15.](https://git-scm.com/docs/git-log)

16. checkout

���Es un comando que te permite crear una rama y trabajar con ella o directamente cambiar de una rama a otra.
[Fuente 16.](https://www.atlassian.com/git/tutorials/svn-to-git-prepping-your-team-migration#basic-git-commands)

17. Rama / Branch

���El comando git branch nos permite crear, listar, cambiar de nombre y eliminar ramas. No le permite cambiar de rama o volver a juntar un historial bifurcado. Una rama representa una l�nea de desarrollo independiente, son una abstracci�n para el proceso de edit, estage y commit. 
[Fuente 17.](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

18. Etiqueta / Tag

���Es un comando que nos permite etiquetar todo aquellos cambios m�s significativos.
[Fuente 18.](https://www.atlassian.com/git/tutorials/svn-to-git-prepping-your-team-migration#basic-git-commands)