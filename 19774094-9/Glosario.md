# Glosario

__1. Control de Verciones(VC):__ El control de versiones básicamente se  encarga de mantener registro sobre todos los aspectos de modificaciones realizadas sobre un archivo, conjunto de archivos, directorios, etc. 
Con lo anterior, el control de versiones permite volver hacia adelante o hacia atrás por las distintas variaciones existentes.

_Source: [Producingoss](https://producingoss.com/es/vc.html)._

__2. Control de versiones Distribuido:__ Consiste en contestar al problema de que al tener muchos usuarios, computadores, servidores, etc, controlen la misma versión actual, y tengan acceso a anteriores versiones en caso de necesitarlo. También permite que cualquier individuo perteneciente al control de versiones, modifique y cree versiones superiores.

_Source: [Documentación Git](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)._

__3. Repositorio remoto y repositorio local:__ El repositorio remoto hace referencia al repositorio global de control de versiones que es compartido varias entidades. En cambio el repositorio local se refiere a aquel que se encuentra dentro de una entidad sin estar	sincronizado con el repositorio remoto.
	
_Source: [Blog de desarrollo Web](http://michelletorres.mx/como-conectar-un-repositorio-local-con-un-repositorio-remoto-de-github/)._

__4. Copia de Trabajo:__ La copia de trabajo refiere a la copia de archivos y directorios la cual se verá afectada por modificaciones.

_Source: [Post StackOverflow](https://stackoverflow.com/questions/581220/what-is-a-working-copy-and-what-does-switching-do-for-me-in-tortoise-svn)._

__5. Staging Area:__ Staging area es un archivo que registra modificaciones realizadas en la copia de trabajo.

_Source: [Git Documentation](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)._

__6. Stage Change:__ Es una previsualización de los cambios realizados sobre los archivos en la copia de trabajo.

_Source: [Githowto](https://githowto.com/staging_changes)._

__7. Stage Confirm:__ El stage commit significa guardar los cambios realizados en la copia de trabajo en el repositorio.

_Source: [Git Documentation](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)._

__8. Commit:__ Commit se refiere a cambios realizados en un repositorio. Estos se pueden realizar o se pueden solicitar.

_Source: [Producingoss](https://producingoss.com/es/vc.html)._

__9. Clone:__ Se refiere a obtener una copia de un repositorio, convirtiendose en una copia de trabajo.

_Source: [Git Documentation](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)._

__10. Pull:__ se refiere a obtener modificaciones realizadas de un repositorio luego de hacer un push.

_Source: [Git Documentation](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)._

__11. Push:__ se refiere a mandar modificaciones realizadas a un repositorio.

_Source: [Git Documentation](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)._

__12. Fetch:__ obtniene la ultima version y todas las ramas automaticamente desde el repositorio remoto.

_Source: [Git Documentation](ttps://git-scm.com/docs/git-fetch)._

__13. Merge:__ permite combinar mas de 2 commit y guardar los cambios de la rama donde fueron hechos dichos cambios.

_Source: [Git Documentation](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)._

__14. Status:__ Permite ver los cambios realizados a un archivo o rama.

_Source: [Git Documentation](https://git-scm.com/docs/git-status)._

__15. Log:__ el log es un registro de todos los cambios realizados en un commit, rama o en archivos:

_Source: [Git Documentation](https://git-scm.com/docs/git-log)._

__16. Checkout:__ actualiza la copia de trabajo con la version mas nueva del repositorio.

_Source: [Git Documentation](https://git-scm.com/docs/git-checkout)._

__17. Branch:__ Es una linea de desarrollo la cual es creada con el fin de no perder la linea original o para implementar distintas soluciones a un problema.

_Source: [Git Documentation](https://git-scm.com/book/en/v1/Git-Branching-What-a-Branch-Is)._

__18. Tag:__ Es una etiqueta que identifica puntos especificos importantes del repositorio.

_Source: [Git Documentation](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)._