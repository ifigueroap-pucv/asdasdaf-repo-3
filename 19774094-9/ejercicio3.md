# Ejercicio 3 

__1. Cantidad de Branches__

* Cantidad de Branches: __1.__
* Nombre: __master.__

__2. Cantidad de Tags__
	
* Cantidad: __0.__

__3. Nombre, Mensaje y código hash de ultimos 3 commit__

 __a. Commit:__ 0b1e7f2f330fcc93a008a9c6ba592f4c42612bed

 * __Autor:__ vicenteign <vicente.navarrete.r@gmail.com>
 * __Fecha:__ Sat Aug 25 16:44:56 2018 -0300
 * __Mensaje:__ Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-3

 __b. Commit:__ 70a476bce0f614fecb25c8b1a6c80087d796b1fa

 * __Autor:__ vicenteign <vicente.navarrete.r@gmail.com>
 * __Fecha:__ Sat Aug 25 16:38:28 2018 -0300
 * __Mensaje:__ Adición de archivo datos
 
  __c. Commit:__ 0ba20460384448077068b19e44d6961a6ca8ec1d

 * __Autor:__ vicenteign <vicente.navarrete.r@gmail.com>
 * __Fecha:__ Sat Aug 25 16:29:43 2018 -0300
 * __Mensaje:__ Glosarios agregados 'Solo cambia el tipo de vista'
