1) **Control de Versiones (VC) :**

Sistema de gesti�n de Cambios realizados sobre un archivo o codigos fuentes en tiempo real, con la posibilidad de almacenar las distintas versiones de este archivo en forma de historial accesible.

2) **Control de versiones distribuido (DVC):**

Sistema de control capaz de interactuar con diferentes clientes en tiempo real,  con la posibilidad de editar versiones desde distintos ordenadores llevando un respaldo a nivel local en cada ordenador de los desarrolladores como forma de copias de seguridad en cada uno.

3) **Repositorio Remoto y Local:** 

remoto: repositorios almacenados en una nube, ya sea en internet o alg�n servidor, este permite ser utilizado de manera local desde cualquier ordenador mediante funciones de red , en que se pueden hacer operaciones como visualizar y editar.
Local: repositorios locales almacenados en un ordenador, repositorios con los cual se trabajan y se realizan todas la tareas previas para despu�s actualizar la versi�n remota o local.

4) **Copia de trabajo/working copy:**
copia total de un repositorio usado para pruebas y testeos de este.

5) **�rea de preparaci�n/ staging �rea:**
archivo que almacena la nueva informaci�n que se transformara en la nueva versi�n del proyecto trabajado, 
el punto medio entre el directorio de trabajo y el repositorio.

6) **Preparar Cambios/ stay change:**
momento de preparaci�n para los pr�ximos cambios a confirmar.

7) **Confirmar cambios/ Commit change:**
ultimo paso para la confirmaci�n de la nueva versi�n, para as� ser subido al repositorio.

8) **Commit:**
Confirma Cambios hechos y los almacena en el repositorio.

9) **Clone:**
clona o copia un repositorio de una carpeta local o de internet.

10) **Pull:** 
acci�n que obtiene los archivo remotos para ser actualizados en el repositorio local

11) **Fetch:** 
Descarga un archivo de un repositorio

12) **Merge**:
Fusiona ramas de un proyecto.

13) **Status:**
examina el estado de una carpeta o repositorio para ver archivos aun no a�adidos, eliminados, etc.

14) **Log:**
Muestra el historial de cambios realizados con el commit.

15) **Checkout:** 
Revierte el archivo a su versi�n anterior.

16) **Rama/Branch : **
Permite trabajar en paralelo con otras ramas para asi no afectar la rama principal

17) **Etiqueta/ tag: **
etiqueta de un objeto que sirve para separar versiones estables en el desarrollo.