# Glosario Para Conceptos Acerca de Git
###### Por Máximo Arnao

1. Control de Versiones (VC): El Control de Versiones es un sistema que permite tener registro de las ediciones pasadas de un archivo o un grupo de ellos. Con esto, se posee un historial de cambios para cada fichero, lo que entrega la posibilidad de volver a una edición anterior del mismo [1].

2. Control de Versiones Distribuido (DVC): Consiste en un sistema en donde cada cliente descarga una réplica completa del repositorio que contiene el proyecto, a diferencia del sistema de Control de Versiones Centralizado (CVCS), en donde los clientes solo descargan copias instantáneas de archivos específicos. Con lo cual el DVC ofrece mayor seguridad en caso de que el servidor deje de funcionar, ya que cada cliente, al tener una copia del proyecto completo, puede ser recuperado desde el repositorio local de alguno de ellos, lo que en CVCS puede no ser posible [2].

3. Repositorio Remoto y Repositorio Local: Un repositorio remoto es aquel que aloja los datos del proyecto, junto con sus distintas versiones, en un servidor dedicado para ello [3]. Por otro lado, un repositorio local es el que se encuentra en la unidad de almacenamiento del cliente, y posee los datos del proyecto junto con las modificaciones que hace el mismo [4].

4. Copia de Trabajo (Working Copy): Corresponde a la copia del proyecto que se encuentra almacenada en la unidad de almacenamiento del cliente. Es decir, que es el directorio con el cual el cliente trabaja directa y localmente [5].

5. Área de Preparación (Staging Area): Corresponde a un archivo en el cual se registran los cambios que se realizarán en una próxima confirmación (*commit*) [6].

6. Preparar Cambios (Stage Changes): Es el proceso mediante el cual se establecen qué archivos deben ser actualizados en el repositorio del proyecto al momento de generar la nueva versión [7].

7. Confirmar Cambios (Commit Changes): Es el proceso que, generalmente, sigue a Preparar Cambios. Consiste en la creación de un nuevo registro de instantánea del proyecto, según lo indicado en el Área de Preparación. Esta nueva instantánea o versión puede ir acompañada de una descripción y, opcionalmente, de una etiqueta [8].

8. Comando *commit*: Registra los cambios en el repositorio. Almacena el contenido actual del índice (Área de Preparación) en una confirmación nueva junto con un mensaje de registro del cliente donde describe los cambios [9].

9. Comando *clone*: Clona un repositorio en el directorio indicado. En otras palabras, copia todo el contenido del repositorio en la dirección indicada. En caso de que se desee clonar un repositorio que esté alojado en una ubicación remota, se debe descargar el contenido [10].

10. Comando *pull*: Útil para buscar los cambios existentes en el repositorio remoto respecto a la versión que posee el cliente. En el caso de que existan diferencias se actualiza el repositorio local con el contenido que está presente en el remoto [11].

11. Comando *push*: Actualiza el repositorio remoto con los cambios realizados sobre el repositorio local del cliente [12].

12. Comando *fetch*: Permite al cliente buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local de trabajo [13].

13. Comando *merge*: Útil para fusionar los cambios realizados en dos ramas. Se actualiza la rama activa con la especificada en los parámetros de la orden [14].

14. Comando *status*: Lista aquellos ficheros (o rutas) que tienen diferencias entre el que se encuentra en el índice y el que está en la última versión (confirmación), las rutas que tienen diferencias entre el árbol de trabajo y el archivo de índice, y las rutas en el árbol de trabajo que no están siendo rastreados [15].

15. Comando *log*: Muestra una lista de confirmaciones (junto con sus detalles) de una rama respectiva [13].

16. Comando *checkout*: Crear una Rama nueva o cambia a otra para trabajar en ella [13].

17. Rama (Branch): En cada confirmación, Git almacena un punto de control que está compuesto por los archivos del proyecto (con las modificaciones hasta ese momento), los datos del autor y la descripción de la misma. Además se deja registro de las confirmaciones anteriores mediante uno o varios apuntadores a la/las confirmaciones inmediatamente anteriores (confirmaciones padre). Con ello una Rama corresponde a un historial de cambios específico dentro del árbol, que en la práctica es un apuntador que apunta a una confirmación concreta, con lo cual la Rama está compuesta por dicha confirmación más las anteriores y las que son padre de estas hasta llegar a la raíz [16].

18. Etiqueta (Tag): Es una cadena definida por algún cliente que apunta a una confirmación específica. Por ende, un Tag es el nombre que recibe la marca de un punto específico en el historial de cambios [17].

## Bibliografía
[1] *Acerca del Control de Versiones (Primera edición)*. Disponible vía web en: [https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones). Consultado por última vez el 22 de Agosto de 2018.

[2] *Acerca del Control de Versiones (Segunda edición)*. Disponible vía web en: [https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones). Consultado por última vez el 22 de Agosto de 2018.

[3] *Trabajando con Repositorios Remotos (Primera edición)*. Disponible vía web en: [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos). Consultado por última vez el 22 de Agosto de 2018.

[4] *Trabajando con Repositorios Locales*. Disponible vía web en: [https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/). Consultado por última vez el 22 de Agosto de 2018.

[5] *Obteniendo un Repositorio Git*. Disponible vía web en: [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git). Consultado por última vez el 22 de Agosto de 2018.

[6] *Fundamentos de Git*. Disponible vía web en: [https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git). Consultado por última vez el 22 de Agosto de 2018.

[7] *Guardando Cambios en el Repositorio - Preparando Cambios en el Repositorio*. Disponible vía web en: [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Preparando-archivos-modificados](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Preparando-archivos-modificados). Consultado por última vez el 22 de Agosto de 2018.

[8] *Guardando Cambios en el Repositorio - Confirmando Tus Cambios*. Disponible vía web en: [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio#Confirmando-tus-cambios). Consultado por última vez el 22 de Agosto de 2018.

[9] *Git-commit Documentation*. Disponible vía web en: [https://git-scm.com/docs/git-commit](https://git-scm.com/docs/git-commit). Consultado por última vez el 22 de Agosto de 2018.

[10] *Git-clone Documentation*. Disponible vía web en: [https://git-scm.com/docs/git-clone](https://git-scm.com/docs/git-clone). Consultado por última vez el 22 de Agosto de 2018.

[11] *Lista Comandos Git*. Disponible vía web en: [https://github.com/susannalles/MinimalEditions/wiki/Lista-Comandos-Git](https://github.com/susannalles/MinimalEditions/wiki/Lista-Comandos-Git). Consultado por última vez el 22 de Agosto de 2018.

[12] *Git-push Documentation*. Disponible vía web en: [https://git-scm.com/docs/git-push](https://git-scm.com/docs/git-push). Consultado por última vez el 22 de Agosto de 2018.

[13] *Comandos Básicos de Git*. Disponible vía web en: [https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git). Consultado por última vez el 22 de Agosto de 2018.

[14] *Git Comandos Básicos*. Disponible vía web en: [http://www.7sabores.com/blog/git-comandos-basicos](http://www.7sabores.com/blog/git-comandos-basicos). Consultado por última vez el 22 de Agosto de 2018.

[15] *Git-status Documentation*. Disponible vía web en: [https://git-scm.com/docs/git-status](https://git-scm.com/docs/git-status). Consultado por última vez el 22 de Agosto de 2018.

[16] *Ramificaciones de Git - ¿Qué es Una Rama?*. Disponible vía web en: [https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F). Consultado por última vez el 23 de Agosto de 2018.

[17] *Tags Con Git*. Disponible vía web en: [https://www.genbeta.com/desarrollo/tags-con-git](https://www.genbeta.com/desarrollo/tags-con-git). Consultado por última vez el 23 de Agosto de 2018.