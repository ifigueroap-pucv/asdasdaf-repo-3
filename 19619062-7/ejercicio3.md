# Datos del Repositorio

## Ramas (*Branches*)

* Cantidad: 1
* Nombre(s): "master"

## Etiquetas (*Tags*)

* Cantidad: 0
* Nombres(s): N/A

## Últimas 3 Confirmaciones (*Commits*)

* Código Hash: 82b79507f62243adfc6976f063678b53bd577743
* Autor: Máximo Arnao
* Mensaje:

 **Agrega Glosario Para Conceptos Acerca de Git**

        Se crea la carpeta con el RUT "19619062-7" y en ella se incorpora el
        archivo en formato Markdown "glosario.md" que presenta las definicio-
        nes de algunos conceptos acerca de Git.

***

* Código Hash: a8fba2d17ae531ad2a9f689b378b4484608d4947
* Autor: Andres Carcamo
* Mensaje:

 **Corrige error en el usuario**

        bla explicacion blaaaaa

***

* Código Hash: 5e39681816081c5553a5fbd3697c3fdf9c193954
* Autor: Andres Carcamo
* Mensaje:

 **Se agregan los datos pedidos**

        bla bla bla tercera explicacion bla bla bla
