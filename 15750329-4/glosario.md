﻿# a. Control de versiones (VC)
Se refiere a la gestión que se le pueda realizar al producto o alguna modificacion, revisión o edición de un producto, tambien se tiene el sistema de control de versiones siendo un metodo que controla los cambios que se realizan en el tiempo a un archivo, ya que el archivo no siempre es terminado en su primera escritura necesitando procesos correctivos o tambien se pueden producir modificaciones en su contenido.
Fuente: [link](https://es.wikipedia.org/wiki/Control_de_versiones)
Fuente: [link](https://torroja.dmt.upm.es/media/files/cversiones.pdf)	  

# b. Control de versiones distribuido (DVC)
El control de versiones distribuido hace referencia a los desarrolladores que trabajan con su propio repositorio sin estar conectado a un servidor teniendo una copia completa del repositorio en su ordenador y si se hace un cambio se propaga a los demas ordenadores que estan distribuidos empleando esos datos para actualizarlos en sus sistemas, un ejemplo de un sistema DVD es git.
Fuente: [link](http://www.mclibre.org/consultar/informatica/lecciones/git.html)
Fuente: [link](http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/)
Fuente: [link](https://altenwald.org/2009/01/12/sistemas-de-control-de-versiones-centralizados-o-distribuidos/)

# c. Repositorio remoto y Repositorio local
Los repositorios remotos se refieren a las versiones de un proyecto que se encuentran alojadas en internet por ejemplo git, estos repositorios pueden ser tanto de escrituras como de lectura acorde a los permisos que uno tenga, cuando el desarrollador quiera compartir un proyecto debe enviarse a un repositorio remoto, el repositorio local se refiere al repositorio que se encuentra alojado ordenador, se compone de 3 estados inicial(estado en donde se encuentran los archivos), intermedio(estado donde se registran los cambios) y final llamado head siendo el estado donde se guarda lo ultimo.
Fuente: [link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
Fuente: [link](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)	

# d. Copia de trabajo / Working Copy
Se refiere a la copia que se puede realizar del proyecto encontrado en el repositorio, en el caso de querer eliminar la copia se utiliza el comando release ocupando el parametro -d
Fuente [link](https://www.osmosislatina.com/cvs_info/wcopy.htm)
Fuente [link](https://rdlab.cs.upc.edu/docu/html/manual_subversion/ManualSubversion-rdlab_es.html)

# e. Área de Preparación / Staging Area
Corresponde a una de las sesiones principales de un proyecto git que contiene un archivo que almacena informacion acerca de la proxima confirmacion, es decir es parte del proceso del estado del ciclo de vida de un archivo.
Fuente [link](https://www.uco.es/aulasoftwarelibre/curso-de-git/introduccion/)
Fuente [link](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

# f. Preparar Cambios / Stage Changes
Corresponde al proceso que tiene el ciclo del vida del archivo para esto a medida que se vaya editando el archivo se preparan todos los cambios para que posteriormente sean confirmados.
Fuente: [link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

# g. Confirmar cambios / Commit Changes
Consiste en un estado del ciclo de vida del archivo, para esto pasa por distintas etapas y una vez que el archivo es modificado posterior a esto se confirman los cambios.
Fuente: [link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

# h Commit
Se refiere al proceso de ir confirmando cambios, para el control de cambios distribuido el comando git es usado para cambiar el head sin afectar el repositorio remoto.
Fuente: [link](https://es.wikipedia.org/wiki/Commit)
Fuente: [link](https://www.hostinger.es/tutoriales/comandos-de-git)

# i. Clone
Corresponde a obtener una copia sobre el repositorio remoto hallado en el control de versiones distribuido git.
Fuente: [link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

# j. Pull
Corresponde a los cambios que uno haga en una rama para seguir en otra rama remota el comando pull une la rama remota con la actual es decir busca y descarga el contenido del repositorio remoto y lo actualiza en el repositorio local.
Fuente: [link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
Fuente: [link](https://www.atlassian.com/git/tutorials/syncing/git-pull)

# k. Push
Se refiere a los cambios que hace uno en el archivo o proyecto del repositorio local y esto posteriolmente los guarda en el repositorio remoto.
Fuente: [link](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)
Fuente: [link](https://translate.google.cl/translate?hl=es-419&sl=en&u=https://www.atlassian.com/git/tutorials/syncing/git-push&prev=search)

# l. Fetch
Corresponde a descargar informacion nueva desde un repositorio remoto sin integrarlos en los  archivos o proyecto de trabajo esto implica que en ninguna instancia se eliminaran o se arruinara el proyecto o el archivo.
Fuente: [link](https://translate.google.cl/translate?hl=es-419&sl=en&u=https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull&prev=search)

# m. Merge
Se refiere a complementar una rama con otra o varias creando un nuevo commit.
Fuente: [link](https://git-scm.com/book/es/v2/Appendix-B%3A-Comandos-de-Git-Ramificar-y-Fusionar)
Fuente: [link](https://www.solucionex.com/blog/git-merge-o-git-rebase)

# n. Status
Corresponde este comando a determinar en que estado se encuentra el repositorio, el proyecto o archivo por ejemplo si se agrega algun dato al proyecto o archivo se mostrara con el comando status que se añadio.
Fuente: [link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

# o. Log
Con este comando se puede ver que modificaciones se han llevado a cabo en el repositorio por ejemplo si se a clonado un repositorio que ya tenia cambios o si se le han hecho varias confirmaciones es decir commit al lchivo o proyecto con este comando se pueden mirar que modificaciones se han hecho. 
Fuente: [link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

# p. Checkout
Este comando sirve para cambiar de posicion el apuntador head en el cual se encuentre es decir que cambia de rama a otra con este comando, si se retrocede a una rama que esta anterior a la actual se puede ir trabajando en una direccion diferente es decir en otra rama.
Fuente: [link](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

# q. Rama / Branch
Corresponde a una rama contenida en el sistema de control de versiones distribuido git este comando nos indica en que rama esta uno situado actualmente, si se modifica, se borra o se añade un archivo al proyecto este no incluye los cambios a la rama principal que esta por defecto rama master.
Fuente: [link](https://www.arsys.es/blog/programacion/ramas-git/)
Fuente: [link](https://www.genbeta.com/desarrollo/manejo-de-ramas-de-desarrollo-con-git)

# r. Etiqueta / Tag
Corresponde a marcar versiones de lanzamientos, tambien son referencias que apuntan a puntos especificos en el historial de git.
Fuente: [link](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)
Fuente: [link](https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag)





   