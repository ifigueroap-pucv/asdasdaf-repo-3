# Ejercicio3
##### por Enzo Aravena Quevedo.
#
### Ramas:
 - Cantidad: Uno
 - Nombre: "master"

### Tags:

- Cantidad: Cero
- Nombre: N/A

### Últimas 3 confirmaciones: 
#### Primera confirmación:
- Codigo Hash: commit 7a6cc4a8d2e471470dcdefd1d01f4c56a1707e85
- Autor: Enzo Aravena <enzoaravenaq@gmail.com>
- Date:   Sat Aug 25 18:38:54 2018 -0300
- Mensaje: Guardar glosario

#### Segunda confirmación: 

- Codigo Hash: commit 70a476bce0f614fecb25c8b1a6c80087d796b1fa
- Autor: vicenteign <vicente.navarrete.r@gmail.com>
- Date:   Sat Aug 25 16:38:28 2018 -0300
- Mensaje: Adición de archivo datos

#### Tercera Confirmación:

- Codigo Hash: commit 0ba20460384448077068b19e44d6961a6ca8ec1d
- Autor: vicenteign <vicente.navarrete.r@gmail.com>
- Date:   Sat Aug 25 16:29:43 2018 -0300
- Mensaje: Glosarios agregados 'Solo cambia el tipo de vista'



