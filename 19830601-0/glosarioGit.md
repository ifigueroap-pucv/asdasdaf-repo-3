# Glosario
##### por Enzo Aravena Quevedo.
#
A continuación se definiran multiples conceptos necesarios para entender y usar de forma correcta el software Git. 
## Conceptos
1. Control de versiones (VC).
2. Control de versiones distribuido (DVC).
3. Repositorio remoto.
4. Repositorio local.
5. Copia de trabajo (Working Copy).
6. Área de preparación (Staging Area).
7. Preparar cambios (Stage Changes).
8. Confirmar cambios (Commit Changes).
9. Commit.
10. Clone.
11. Pull.
12. Push.
13. Fetch.
14. Merge.
15. Status.
16. Log.
17. Checkout.
18. Rama (Branch).
19. Etiqueta (Tag).

-------------------------
# Definición de Conceptos

### 1. Control de versiones (VC) :

Sistema que registra a lo largo del tiempo los cambios realizados en un archivo o conjunto de archivos (proyecto), es útil para recuperar versiones anteriores en caso de cometer errores en el archivo actual, como también consultar distintas versiones anteriores al archivo actual y visualizar los cambios realizados entre las versiones. Al encontrar un error en una versión es posible ver quien modifico el archivo por última vez, lo que facilita encontrar al posible encargado del error y solucionarlo.

### 2. Control de versiones distribuido (DVC):

En un DVC , al ser descargada un instantánea por los clientes, se crea un replica completa del repositorio, lo que permite conservar los archivos del servidor en caso de que este falle y así poder restaurar los archivos desde un cliente sin perder todo el avance almacenado en el servidor. 

### 3. Repositorio remoto:

Son versiones de tus archivos que se encuentran alojados en internet o en algún punto de la red. Estos archivos pueden encontrarse restringidos como solo lectura o escritura y lectura.

### 4. Repositorio local:

Son directorios que se encuentran en el ordenador donde se trabaja, almacenando los archivos de un proyecto en este. En este repositorio existen tres depósitos.

- Depósito inicial: directorio de trabajo, donde se almacenan los archivos que se editan y modifican.
- Depósito intermedio: depósito llamado Index o Stage, similar a una memoria cache, es donde se registran los cambios de un archivo antes de guardarlo en el depósito final.
- Depósito final: depósito llamado head, donde se guarda el archivo en su último estado de modificación.

### 5. Copia de trabajo (Working Copy):

Es la copia local de los ficheros de un repositorio remoto. Todo el trabajo realizado sobre los ficheros en un repositorio remoto se  debe realizar inicialmente sobre una copia de trabajo en el repositorio local del ordenador.

### 6. Área de preparación (Staging Area):

Es un archivo contenido en el directorio de Git, que almacena información sobre los cambios que se desean guardar en la próxima confirmación.

### 7. Preparar cambios (Stage Changes):

Previo al envió de un archivo al área de preparación, el usuario puede añadir, remover o modificar partes del archivo actual (preparar cambios),  para una próxima confirmación de cambios realizados con el comando commit.  

### 8. Confirmar cambios (Commit Changes):

Es almacenar los datos del archivo actual de manera segura en tu base de datos local, para esto, se confirman los cambios y se toman el o los archivos, sin realizar cambios en ellos, del área de preparación y se guardan en el directorio de Git.

### 9. Commit:

Comando en Git que se utiliza para añadir los archivos que se encuentran en el área de preparación al directorio local de Git en donde se encuentra tu proyecto. 

##### Sintaxis:
#
    git commit -m "mensaje: descripción de la confirmacion"
    

### 10. Clone:

Comando en Git que se utiliza para obtener una copia de un repositorio remoto existente, es útil cuando se desea contribuir en un proyecto que se almacena en repositorio remoto y así poder trabajar sobre una copia de este.

##### Sintaxis:
#
    git clone [url]

### 11. Pull:

Comando en Git que se utiliza para recibir archivos de repositorios remotos. Al usar este comando, se unirá automáticamente los archivos clonados remotos con los archivos actuales, intentando unir las ramas remotas con la rama local maestra.
##### Sintaxis:
#
    git pull


### 12. Push:

Comando en Git que se utiliza para compartir un proyecto ubicado en un repositorio local a un repositorio remoto, y así permitir que otras personas que tengan acceso al repositorio remoto pueden acceder a los cambios en tu proyecto.
##### Sintaxis:
#
    git push [nombre-remoto][nombre-rama]

### 13. Fetch:

Comando en Git que se utiliza para recuperar archivos de un proyecto ubicado en el repositorio remoto, ubicándolos en el en repositorio local, a diferencia del comando pull, este no une automáticamente los archivos a la rama maestra de tu proyecto.
##### Sintaxis:
#
    git fetch [nombre-remoto]

### 14. Merge:

Comando en Git que se utiliza para fusionar una o más ramas dentro de la rama actual que se tiene activa.
##### Sintaxis:
#
    git merge [nombre-rama]

### 15. Status:

Comando en Git que se utiliza para obtener información sobre los diferentes estados de un archivo ubicado en tu directorio de trabajo, por ejemplo, que archivos están modificados y sin seguimiento o cuales, con seguimiento, pero aun no son confirmados.
##### Sintaxis:
#
    git status


### 16. Log:

Comando en Git que se utiliza para obtener un registro de modificaciones que se han realizado en la historia un archivo.
##### Sintaxis:
#
    git log

### 17. Checkout:

Comando en Git que se utiliza para realizar un cambio de rama en la que sea desea trabajar.
##### Sintaxis:
#
    git checkout [nombre-rama]

### 18. Rama (Branch):

Cada vez que se confirma un cambio, Git almacenara un punto de control que posee un apuntador a una copia de los contenidos preparados, y varios apuntadores a las confirmaciones que sean padres directos de esta. Entonces, una rama es un apuntador móvil que apunta a una de sus confirmaciones, la rama por defecto es la rama maestra, la cual se crea con la primera confirmación de cambios que se realice.

### 19. Etiqueta (Tag):

Comando en Git que se utiliza para nombrar puntos específicos del historial de un proyecto, principalmente es usada para marcar versiones de lanzamiento de un proyecto, las etiquetas no cambiaran a menos que el usuario lo indique.

##### Sintaxis:
#
    git tag
#
----------

# Referencias

A continuación se enumeraran respectivamente las referencias utilizadas para desarrollar las definiciones de los conceptos antes enumerados.

1. https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
2. https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
3. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
4. https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/
5. https://es.wikipedia.org/wiki/Control_de_versiones
6. https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
7. https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
8. https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
9. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio
10. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git
11. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
12. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
13. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
14. https://git-scm.com/book/es/v2/Appendix-B%3A-Comandos-de-Git-Ramificar-y-Fusionar 
15. https://git-scm.com/book/es/v2/Appendix-B%3A-Comandos-de-Git-Seguimiento-B%C3%A1sico
16. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones
17. https://www.hostinger.es/tutoriales/comandos-de-git
18. https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F
19. https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado




