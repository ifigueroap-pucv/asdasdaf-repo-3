﻿# Glosario

Control de versiones: Se llama **control de versiones** a la gestión de los diversos cambios que se realizan sobre los elementos de algún producto o una configuración "Gestión de configuración de software") del mismo. Una versión, revisión o edición de un producto, es el estado en el que se encuentra el mismo en un momento dado de su desarrollo o modificación.

Control de versiones distribuido: El control de versiones distribuido toma un enfoque entre iguales ([peer-to-peer](https://es.wikipedia.org/wiki/Peer-to-peer "Peer-to-peer")), opuesto al enfoque de cliente-servidor de los sistemas centralizados. En lugar de un único repositorio central en el cual los clientes se sincronizan, la copia local del código base de cada _peer_ es un repositorio completo. El control de versiones distribuido sincroniza los repositorios intercambiando ajustes (conjuntos de cambios) entre iguales. Esto establece algunas diferencias importantes en comparación a un sistema centralizado:

Repositorio remoto: Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en algún punto de la red.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

Repositorio local: En general, trabajarás con un **[repositorio](https://colaboratorio.net/glosario/repositorio/) local**, que es el que tienes en tu ordenador
https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/

Copia de trabajo: Es una copia de los archivos originales del trabajo en cuestion, un "respaldo".

https://es.wikipedia.org/wiki/Copia_de_seguridad

Area de preparacion: Archivo contenido en el propio repositorio el cual contiene informacion acerca de la proxima confirmacion.

https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git

Preparar cambios: Es una accion que se refiere a agregar contenidos para tu proxima confirmacion, se usa el comando add.

https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio

Confirmar cambios: Una vez que se tengan todos los archivos o cambios preparados, se confirman, en este paso se escribe un mensaje de confirmacion en el cual se escribe los cambios aplicados al o los archivos.

https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio

Commit: Comando para confirmar.

Clone: Comando para copiar un repositorio del repositorio remoto a uno local

Pull: Sincronizar commit del repositorio remoto original.

Push: Enviar los cambios de mi repositorio local al remoto.

Fetch: Este comando actualiza los cambios realizados y los lleva a otra rama.

Merge: Es para mezclar las ramas con el objetivo de actualizar los archivos modificados, git se encarga de identificar los cambios y los aplica.

Status: Comando para mostrar el estado del repositorio, si hay cambios etc.

Log: Comando para mostrar todos los commit realizados hasta la fecha.

Chekout: Comando para revertir todos los cambios realizados hasta el commit anterior de un archivo. O tambien para viajar a una rama anterior en el tiempo.

Rama/branch: Es una linea de desarrollo donde un commit sigue a su commit anterior y asi sucesivamente. Sirve para trabajar en el mismo repositorio varios usuarios al mismo tiempo y branch es el comando para crear otra rama.

Etiqueta/tag: Son alias a commits concretos.

https://www.youtube.com/watch?v=QGKTdL7GG24
