# Ejercicio 3

1. Contiene 1 branch de nombre Master

2. Contiene 0 etiquetas

3. �ltimos 3 commits (desde el m�s reciente)

	| **Author**                          | **Mensaje**                                      | **Codigo hash**  |
	| -------------------------------------- | ------------------------------------------------ | ----------------:|
	| Manuel Figueroa <mifi.marin@gmail.com> | Se agrega archivo datos.md                       | commit be09c5236ff55ce74c1c0c4b4dcc352aff1431ae |
	|                                        | Se a�ade el archivo a carpeta personal           |                  |
	| Manuel Figueroa <mifi.marin@gmail.com> | Se sube archivo ejercicio3.md                    | commit dbc8beef0a9bb50bb7848de1e940cba8d96388fa |
	|                                        | Se sube archivo a carpeta personal con lo pedido.|                |
	| Manuel Figueroa <mifi.marin@gmail.com> | Creaci�n de carpeta 19758188-3                   | commit e60d6075b10a82ef54c083c0b87977e1225b0c8c |
	|                                        | Se crea carpeta propia con mi rut y se copia en glosario.md dentro de la carpeta|   |





