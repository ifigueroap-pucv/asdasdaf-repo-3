# **Taller 0**
## Glosario

1. Control de versiones (VC): Es un sistema que posibilita el registro de cambios que se realicen sobre archivos en el tiempo que estos se puedan modificar. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

2. Control de versiones distribuido (DVC): Es un sistema en el que se replica absolutamente todo el repositorio, de manera que si el servidor con los archivos se cae y este tipo de sistemas est� contribuyendo con el, se puede restaurar a trav�s de los clientes que lo est�n utilizando. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

3. Repositorio remoto y Repositorio local: El repositorio remoto es el que est� situado en Internet, ya sea de GitHub o similar. El repositorio local b�sicamente es el lugar de trabajo que se utiliza en el computador, que contiene un directorio, un almac�n y un head. Ambos contienen los archivos de trabajo. [Fuente 1](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales), [Fuente 2](-https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) 

4. Copia de trabajo/Working Copy: Es el clon que se genera del repositorio que se est� utilizando, de este modo todos los que contribuyan con los archivos generan una copia de trabajo para que se trabaje sobre ella, y no hagan cambios al original. [Fuente](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

5. Area de Preparaci�n / Staging Area: Es un archivo que est� contenido en el directorio de Git y almacena la informaci�n correspondiente a los archvivos que se preparar�n previo a la confirmaci�n de cambios. [Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

6. Preparar Cambios / Stage Changes: Se realiza cuando un cambio inicial se ha llevado a cabo, antes de que este sea permanente en el repositorio. De manera que se documenta el cambio realizado respecto de la versi�n anterior. [Fuente](https://githowto.com/staging_changes)

7. Confirmar cambios / Commit Changes: Se realiza luego de que se tiene una copia de trabajo lista para que se realicen cambios en el repositorio, de esta forma cuando se desee hacer el cambio se tienen que confirmar para que el proyecto tenga un nuevo estado. [Fuente](hhttps://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

8. Commit: Es la confirmaci�n que se realiza al realizar un cambio pero en el repositorio local, el cual adem�s contiene informaci�n respecto del cambio. [Fuente](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git) 

9. Clone: Cuando se desea tener una copia del repositorio, se utiliza el comando *git clone*, de manera que se copian todos los datos contenidos en una carpeta nueva. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git) 

10. Pull: Se utiliza para recuperar informaci�n del servidor que se clon�, uni�ndolo con el c�digo que se est� trabajando. De esta manera, mediante *git pull* se recupera y une la rama remota con la local. [Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git) 

11. Push: Cuando el proyecto que se est� utilizando se modifica o tiene un nuevo estado que se desea compartir, se tiene que enviar al repositorio contenido en git (remoto), mediante el comando *git push*. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) 

12. Fetch: Cuando se desea recuperar datos de repositorios remotos se utiliza el comando *git fetch*, de esta manera se descargan los cambios a una carpeta oculta (origin/master). [Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git) 

13. Merge: Se utiliza para traer los datos contenidos en otro branch(carpeta oculta) a un branch local (rama master), mediante el comando *git merge*. [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar) 

14. Status: Utilizado para comprobar el estado de los archivos. De esta manera, mediante *git status* se obtiene informaci�n respecto a los archivos, si es que est�n en seguimiento, fueron modificados y adem�s de la rama en la que se encuentra. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones) 

15. Log: Para saber qu� modificaciones fueron realizadas en el repositorio, se utiliza el comando *git log*, en el cual se listan una serie de confirmaciones hechas sobre el repositorio. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones) 

16. Checkout: Se utiliza para crear un nuevo branch y saltar a el, mediante el comando *git checkout*
. [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar) 

17. Rama / Branch: En Git, es b�sicamente un apuntador din�mico a una confirmaci�n en particular. Por ejemplo, luego de la primera confirmaci�n de cambios que se realice, se crear� una rama *Master* la cual apuntar� siempre a la �ltima confirmaci�n. Mediante el comando *git branch, se crea una nueva rama para usar libremente de apuntador a futuras confirmaciones. [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

18. Etiqueta / Tag: Es utilizada para marcar zonas espec�ficas del historial, que son importantes para los contribuyentes del proyecto. Dicho en otras palabras, se usa generalmente para indicar a que versi�n pertenece. Para listar las etiquetas se ocupa el comando *git tag*. [Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)
