# Glosario de t�rminos
A continuaci�n, se describir�nlos siguientes conceptos o t�rminos t�cnicos asociados a git.

 1. **Control de versiones (VC):** Administra los diversos cambios realizados sobre una versi�n o estado en el que se encuentran uno o m�s archivos. Esto con el fin de recuperar versiones espec�ficas durante el desarrollo de un proyecto.
>[Fuente 1](https://es.wikipedia.org/wiki/Control_de_versiones)
>[Fuente 2](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)
2. **Control de versiones distribuido (DVC):** Permite que m�ltiples usuarios trabajen en un proyecto com�n sin necesidad de compartir una misma red. Para esto los usuarios no solo descargan la �ltima versi�n de los archivos, sino que clonan completamente el repositorio.
>[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones)
3. **Repositorio remoto y Repositorio local:** El repositorio local es el �rea de en donde se genera el flujo de trabajo en git, en este conjunto de areas se modifican, preparan y confirman los archivos que se almacenaran en la base de datos local. El repositorio remoto es una versi�n del proyecto almacenada en internet, como, por ejemplo, *gitLab* o *GitHub*.
>[Fuente 1](http://rogerdudler.github.io/git-guide/index.es.html)
>[Fuente 2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
4. **Copia de trabajo / *Working Copy*:** El *Working Copy* es una copia de una versi�n del proyecto. Estas copias son extra�das del repositorio local y colocadas en el �rea de trabajo para su posterior modificaci�n.
>[Fuente](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)
5.  **�rea de Preparaci�n / *Staging Area*:** La *Staging Area* es la �rea intermedia entre el directorio de trabajo y el repositorio local. Es en esta zona en donde se preparan los cambios realizados a los archivos.
>[Fuente](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)
6.  **Preparar Cambios / *Stage Changes*:** *Stage Changes* se refiere a la acci�n de marcar un archivo que ha sido modificado en su versi�n actual, estos archivos "marcados" posteriormente se confirmaran y almacenaran en la base de datos del repositorio local.
>[Fuente](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)
7.  **Confirmar cambios / *Commit Changes*:** *Commit Changes* es el paso en el cual se toman los archivos modificados que ya han sido preparados, almacenando una copia o "instant�nea" de estos en el repositorio local.
>[Fuente](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)
8.  **commit:** confirmaci�n, comando git encargado de almacenar el contenido del �rea de preparaci�n en el repositorio local, esta confirmaci�n se realiza junto con un mensaje descriptivo.
>Ejemplo: `git commit �m �Mensaje descriptivo�`
>[Fuente](https://git-scm.com/docs/git-commit)
9. **clone:** clonado, comando git encargado de clonar o copiar un repositorio a un nuevo directorio de trabajo.
>Ejemplo: `git clone https://gitlab.com/usuario/proyecto.git `
>[Fuente](https://git-scm.com/docs/git-clone)
10. **pull:** comando git encargado de a�adir cambios desde un repositorio remoto a la rama actual de trabajo. El comando git pull implica git fetch seguido de git merge FETCH_HEAD.
>Ejemplo: `git pull nombe_repo_remoto nombre_rama`
>[Fuente](https://git-scm.com/docs/git-pull)
11. **fetch:** comando git encargado de obtener y descargar ramas y/o etiquetas desde repositorios remotos.
>Ejemplo: `git fetch nombre_repo_remoto`
>[Fuente](https://git-scm.com/docs/git-fetch)
12. **merge:** comando git encargado de unir dos ramas de trabajo. Permite mezclar el codigo de una implementacion a la rama principal.
>Ejemplo: `git merge nombre_rama`
>[Fuente](https://git-scm.com/docs/git-merge)
13. **status:** comando git encargado de mostrar estado de los archivos durante su flujo de trabajo entre las distintas �reas. Los tres estados principales en los que se pueden encontrar los archivos son los siguientes:
>- Confirmado o *committed*
>- Modificado o *modified*
>- Preparado o *staged*
>
>Ejemplo: `git status -s`
>[Fuente](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)
14. **log:** comando git encargado de mostrar el registro de confirmaciones realizadas.
>Ejemplo: `git log`
>[Fuente](https://git-scm.com/docs/git-log)
15.  **checkout:** comando git encargado de cambiar rama activa.
>Ejemplo: `git checkout master`
>[Fuente](https://git-scm.com/docs/git-checkout)
16.  **Rama / Branch:** Las ramas o *branches* permiten a los desarrolladores implementar funcionalidades fuera de la rama principal. esta rama anexa se integra a la rama principal una vez terminada la implementaci�n de la funcionalidad.
>Ejemplo: `git checkout -b nombre_rama`
>[Fuente](http://rogerdudler.github.io/git-guide/index.es.html)
17.  **Etiqueta / Tag:** Las etiquetas o *tags* permiten crear un alias para identificar de mejor forma un commit importante, como por ejemplo una nueva versi�n del proyecto.
>Ejemplo: `git tag v1.0 acb123de45`
>[Fuente](http://rogerdudler.github.io/git-guide/index.es.html)