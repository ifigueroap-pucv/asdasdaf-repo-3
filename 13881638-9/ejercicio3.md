# Ejercicio 3
## a. Cantidad y nombre de los branches
```
```
| Cantidad      | Nombre|
|:-------------:|:-------------:|
| 1     | master |


## b. Cantidad y nombre de las etiquetas
```
```
| Cantidad       | Nombre           |
| :-------------: |:-------------:|
| 0     | - |


## c. Últimos 3 commits
```
```
| Nombre        |     Mensaje       | Código hash|
| :-------------: |:-------------:| :-----:|
|  Octavio Oyanedel - PC casa      |  Parte 1, punto 4, add y commit a repositorio local | 1e666194536f90949bbd2b556f088c6854d09dc8  |
| Felipe Lara |   datos subidos     |   b2163a441613f48fa35759d1a79d312bc012df93  |
| Felipe Lara | ejercicio3 subido   |    1df5b73bcc7790a1c2729019910cf8db5ff563e2 |
```
