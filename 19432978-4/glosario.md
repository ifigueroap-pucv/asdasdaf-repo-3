# Glosario

**1. Control de versiones (VC):** Es un sistema que registra los cambios hechos sobre uno o m�s archivos a lo largo del tiempo, de ese modo se puede recuperar cualquier versi�n espec�fica que necesites m�s adelante.
<https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones>

**2. Control de versiones distribuido (DVC):** Este sistema funciona con m�s de un cliente, donde estos al descargar la �ltima instant�nea de los archivos replican completamente el repositorio, creando una copia de seguridad completa de todos los datos, de este modo si un servidor muere se recuperan todos los datos del repositorio de cualquiera de los clientes, restaurando el servidor.
<https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones>

**3. Repositorio remoto:** Son versiones de tu proyecto que se encuentran en Internet o en un punto espec�fico de la red, se puede tener m�s de uno y estos poseen permisos, pueden ser s�lo lectura o lectura y escritura.
<https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos>

**4. Repositorio local:** Es el repositorio que se encuentra en tu ordenador.
<https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/>

**5. Copia de trabajo (*Working Copy*):** Es un repositorio por s� mismo, en el cual el programador trabaja realizando las modificaciones necesarias.
<https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms>

**6. �rea de preparaci�n (*Staging Area*):** Es un archivo contenido usualmente en el directorio de Git, que almacena todos los cambios e informaci�n de lo que se enviar� en la siguiente confirmaci�n.
<https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git>

**7. Preparar cambios (*Stage Changes*):** Es el paso anterior antes de que se realice el *commit*.
<https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git>

**8. Confirmar cambios (*Commit Changes*):** Es el paso donde se env�an y guardan los cambios en el repositorio original.
<https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio>

**9. *Commit*:** Almacena el contenido actual y cambios realizados por el usuario en el repositorio junto con un mensaje donde el usuario describe los cambios realizados.
<https://git-scm.com/docs/git-commit>

**10. *clone*:** Tal como se�ala su nombre, este clona un repositorio en un directorio nuevo, creando las ramas de seguimineto remoto para cada rama del repositorio que se clon�.
<https://git-scm.com/docs/git-clone>

**11. *pull*:** Incorpora los cambios realizados del repositorio remoto en el repositorio local.
<https://git-scm.com/docs/git-pull>

**12. *push*:** Actualiza el repositorio remoto con los cambios realizados en el repositorio local.
<https://git-scm.com/docs/git-push>

**13. *fetch*:** Se obtienen las ramas y etiquetas del o los repositorios remotos para ir completando el proyecto.
<https://git-scm.com/docs/git-fetch>

**14. *merge*:** Incorpora los cambios realizados de los *commits* nombrados de todas las ramas, que se bifurcaron de la rama actual, en la rama actual.
<https://git-scm.com/docs/git-merge>

**15. *status*:** Muestra el estado actual del �rbol de trabajo, detallando todas las diferencias que poseen las rutas entre el archivo �ndice y la confirmaci�n del HEAD.
<https://git-scm.com/docs/git-status>

**16. *log*:** Muestra los *commit* realizados.
<https://git-scm.com/docs/git-log>

**17. *checkout*:** Actualiza los archivos en el �rbol de trabajo, haciendo que de esta manera coincida con la versi�n del �rbol especificado. De no proporcionar ninguna ruta, git actualizar� el HEAD estableciendo la ruta especificada como la actual.
<https://git-scm.com/docs/git-checkout>

**18. Rama (Branch):** Las ramas son l�neas de desarrollo del proyecto, un proyecto puede tener una o m�s ramas, la rama por defecto de *git* se llama *master*.
<https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F>

**19. Etiqueta (Tag):** Sirve para marcar puntos espec�ficos en el historial como por ejemplo las versiones del proyecto.
<https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado>
