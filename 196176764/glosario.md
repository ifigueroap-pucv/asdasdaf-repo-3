

# Glosario de T�rminos **_GIT_**

######En el siguiente trabajo, se realiz� una definici�n de t�rminos de **_GIT_**, este es un Software de control de versiones dise�ado por Linus Torvalds. Fue desarrollado pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones.

## 

## T�rminos a definir.
1. Control de versiones (VC).
2.  Control de versiones distribuido (DVC).
3.  Repositorio remoto y Repositorio local.
4.  Copia de trabajo / Working Copy.
5.  �rea de Preparaci�n / Staging Area.
6.  Preparar Cambios / Stage Changes.
7.  Confirmar cambios / Commit Changes.
8.  commit.
9.  clone.
10. pull.
11. push.
12. fetch.
13. merge.
14. status.
15. log.
16. checkout.
17. Rama / Branch.
18. Etiqueta / Tag.

##

##Definiciones


**1. Control de versiones (VC):** Sistema que registra los cambios realizados sobre un archivo o un conjunto de archivos a lo largo del tiempo, permitiendo la recuperaci�n de versiones espec�ficas m�s adelante.

**2. Control de versiones distribuido (DVC):** Sistema que registra los cambios realizados sobre un archivo o un conjunto de archivos a lo largo del tiempo, sin necesidad de un servidor central, cada programador que tenga acceso a los archivos, tendr� una copia completa del repositorio en su equipo, y al hacer modificaciones estos se env�an a todos los nodos. Esto permite una b�squeda mas eficaz y evitar p�rdida de datos en caso de fallar un servidor, se tendr� un respaldo en el resto de sistemas enlazados a �l.

**3. Repositorio remoto y Repositorio local:**

* Repositorio Remoto: Un repositorio remoto, es una versi�n de un proyecto que se encuentra almacenado en Internet o en alg�n punto de la red. Se pueden tener mas de uno, estos pueden ser de s�lo lectura, lectura/escritura, seg�n los permisos que se tengan. Cuando son proyectos de mas de un participante, estos son colaborativos, donde cada uno de los colaboradores puede acceder a un repositorio remoto o todos los repositorios remotos existentes, dependiendo de sus permisos. Estos se pueden gestionar, y se pueden enviar y recibir, (`push` y `pull` respectivamente).

* Repositorio Local: El repositorio local, es una versi�n de un proyecto que se encuentra almacenado en el equipo donde se est� trabajando esa versi�n. Se pueden tener mas de uno, pero a estos solo se tiene acceso desde el equipo donde se crearon.

**4. Copia de trabajo / Working Copy:** Realizar una copia de trabajo, es copiar un repositorio existente mediante el comand `git clone`, este comando permite clonar/copiar un repositorio que ya existe, este copia **TODOS** los archivos existentes en el repositorio. Adem�s permite clonar repositorios almacenados en la web, agregando la url al comando, `git clone [url]`. Este puede ser escrito de distintas formas: `git://`, `http(s)://` o `usuario@servidor:/ruta.git`.

**5. �rea de Preparaci�n / Staging Area:** Es un archivo que almacena la informaci�n nueva y/o modificada y recibe los archivos que deben ser preparados para la pr�xima confirmaci�n (commit). El �rea de preparaci�n se conoce tambi�n como "_Index_". 


**6. Preparar Cambios / Stage Changes:** Este es el proceso previo para enviar los archivos y/o modificaciones al �rea de Preparaci�n, estos son seleccionados por el usuario. Para esto debemos ingresar el comando `git add` al archivo que se desea preparar para ser enviado al �rea de preparaci�n.

**7. Confirmar cambios / Commit Changes:** Es una acci�n que confirma todos los cambios y/o nuevos archivos, preparados para confirmar en el �rea de preparaci�n. Esto se realiza mediante el comando `git commit`.

**8. Commit:** Comando utilizado para confirmar las modificaciones realizadas en los archivos, tambi�n para los archivos creados, este comando se escribe de la siguiente manera `git commit`, este abrir� un cuadro de texto, donde se debe especificar que se realiz�. Otra forma de ingresar este comando, es agregando en la l�nea de comando el mensaje, `git commit -m "S� elimin� la funci�n sumar y restar"`.

**9. clone:** Comando utilizado para obtener una copia de un repositorio Git existente, se ingresa de la siguiente manera `git clone`. Al ejecutar este comando, cada versi�n de cada archivo de la historia del repositorio es descargado.

**10. pull:** Comando utilizado para unir una rama local, con una rama remota, en otras palabras mas simples, pull une todo lo realizado en el repositorio local con el repositorio remoto. El comando se ingresa de la siguiente manera `git pull`.

**11. push:** Comando que sube los cambios realizados en el repositorio local a un repositorio remoto. Se ingresa de la siguiente manera `git push`.

**12. fetch:** Comando que recupera los datos del remoto que no est�n en el �rea local. Se ingresa de la siguiente manera `git fetch [nombre-remoto]`. Posteriormente a ingresar el comando, ha de tener acceso a todas las ramas del repositorio remoto. 

**13. merge:** Comando que une las modificaciones realizadas a la rama master. Se asume que se est� posicionado en la rama de origen, se escribe de la siguiente forma `git merge`.

**14. status:** Comando utilizado para ver el estado de la rama actual, entrega informaci�n sobre si se agregaron, eliminaron, modificaron o movieron archivos. Se ingresa de la siguiente manera `git status`.

**15. log:** Comando utilizado para ver el historial de confirmaciones (Commit). Se ingresa de la siguiente manera `git log`.

**16. checkout:** Comando utilizado para pasar de una rama a otra. Se ingresa de la siguiente manera `git checkout nombre_rama_destino`. Tambi�n se usa para crear una rama y acceder a ella, en este caso el ingreso es un poco distinto, `get checkout -b nombre_rama`

**17. Rama / Branch:** Una confirmaci�n en Git, es realmente un punto de control que contiene las modificaciones realizadas, y este apunta a la confirmaci�n anterior. Entonces las ramas permiten la conexi�n entre estas confirmaciones, al crear la primera confirmaci�n esta se une a la rama "Master" que es la creada por defecto. El comando para crear una rama es el siguiente, `git branch nombre_rama`

**18. Etiqueta / Tag:** Las etiquetas renombrar las confirmaciones, al ingresar `git log --oneline` uno podr� observar las distintas confirmaciones realizadas, con un breve c�digo �nico, est� c�digo puede reemplazarse por un "tag" el cual puede hacer aun m�s descriptivo una confirmaci�n, o, que es el uso que se le da usualmente, es para separar versiones, por ejemplo: v1.0.

## 

## REFERENCIAS 

####Definici�n 1.

* <https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones>

* <http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/>

* <https://es.wikipedia.org/wiki/Git>

####Definici�n 2.

* <https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones>

* <http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/>

####Definici�n 3.

* Repositorio Remoto: <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos>

* Repositorio Local: <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos>

####Definici�n 4.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git>

####Definici�n 5.

* <https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git>

####Definici�n 6.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio>

####Definici�n 7.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio>

####Definici�n 8.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio>

####Definici�n 9.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git>

####Definici�n 10.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos>

####Definici�n 11.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos>

* <https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git>

####Definici�n 12.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos>

####Definici�n 13.

* <https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar>

####Definici�n 14.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio>

####Definici�n 15.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones>

####Definici�n 16.

* <https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar>

####Definici�n 17.

* <https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F>

####Definici�n 18.

* <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas>

##

######Autor: Diego Herrera Torres.
