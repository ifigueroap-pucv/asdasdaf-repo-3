# Ejercicio 3

1. **Cantidad y nombre de los branches**: 1 branch llamado "*master*".

2. **Cantidad y nombre de las etiquetas**: ningun tag/etiqueta existente.

3. **Nombre, mensaje y código hash de los últimos 3 commits**: Utilizando el comando git log, los resultados son:
~~~
commit 1df5b73bcc7790a1c2729019910cf8db5ff563e2
Author: Felipe Lara <anibalmode@gmail.com>
Date:   Mon Aug 27 19:27:22 2018 -0300

    ejercicio3 subido
~~~

~~~
commit b2163a441613f48fa35759d1a79d312bc012df93 (origin/master, origin/HEAD)
Author: Felipe Lara <anibalmode@gmail.com>
Date:   Mon Aug 27 19:33:19 2018 -0300

    datos subidos
~~~

~~~
commit dab0ae9a5a9421500a517c860c335874ca12281c (HEAD -> master)
Author: dvilchesm <d.vilches001@gmail.com>
Date:   Mon Aug 27 19:52:54 2018 -0300

    commit glosario
~~~