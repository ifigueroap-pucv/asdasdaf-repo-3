# Glosario

1. **Control de versiones (VC)**: es un sistema que guarda los cambios realizados a un archivo o a un conjunto de archivos a lo largo del tiempo, con el objetivo de poder recuperar versiones específicas a futuro. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

2. **Control de versiones distribuido (DVC)**: permite a los diferentes clientes crear una copia local de la última versión de los archivos (repositorio), de esta manera pueden trabajar en múltiples flujos de trabajo. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

3. **Repositorio remoto y Repositorio local**: el repositorio remoto son versiones del repositorio alojada en tu ordenador, el repositorio remoto son versiones alojadas en internet o en algún punto de la red. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git)

4. **Copia de trabajo / Working Copy**: copia local del repositorio donde el desarrollador trabaja. [Fuente](https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf)

5. **Área de Preparación / Staging Area**: es la zona donde se añaden los cambios para posteriormente ser confirmados. [Fuente](https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf)

6. **Preparar Cambios / Stage Changes**: proceso que permite asegurar que los cambios realizados estén listos para ser guardados, es decir, para incluirlos en el siguiente commit. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

7. **Confirmar cambios / Commit Changes**: confirmar los cambios realizados a los archivos e incluirlos en el HEAD. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

8. **Commit**: comando utilizado para enviar los cambios al HEAD de la copia local del repositorio. [Fuente 1](http://rogerdudler.github.io/git-guide/index.es.html) [Fuente 2](https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf)

9. **clone**: comando utilizado para crear una copia local de un repositorio existente. [Fuente](http://rogerdudler.github.io/git-guide/index.es.html)

10. **pull**: comando utilizado para actualizar el repositorio local al commit más reciente, de esta manera se bajan y combinan los cambios remotos. [Fuente](http://rogerdudler.github.io/git-guide/index.es.html)

11. **push**: comando utilizado para enviar los cambios en el HEAD de la copia local al repositorio remoto. [Fuente](http://rogerdudler.github.io/git-guide/index.es.html)

12. **fetch**: comando para bajar los cambios del repositorio remoto a la rama origin/master del repositorio local. [Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)

13. **merge**: comando utilizado para fusionar una rama específica con la rama activa. [Fuente](http://rogerdudler.github.io/git-guide/index.es.html)

14. **status**: comando para comprobar el estado de los archivos. [Fuente 1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio) [Fuente 2](https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf)

15. **log**: Visualizar el historial de commits realizados, entrega el commit id. [Fuente 1](http://rogerdudler.github.io/git-guide/index.es.html) [Fuente 2](https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf)

16. **checkout**: actualiza los archivos en el árbol de trabajo para que coincida con la versión de la ruta especificada. Si no se especifica ruta, se actualizará la rama apuntada al HEAD. [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

17. **Rama / Branch**: cada una es utilizada para desarrollar una funcionalidad aislada de las otras, se pueden fusionar a la rama principal. [Fuente](https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf)

18. **Etiqueta / Tag**: son creados para identificar cada una de las versiones del software. Hay de dos tipos:
    - **Ligeras**: es similar a una rama, pero esta no cambia. Es un puntero a un commit específico.
    - **Anotadas**: es un objeto generado en el repositorio. Contiene nombre, email, fecha, mensaje de etiqueta y pueden ser firmadas y verificadas con la herramienta de cifrado y firmas digitales GNU Privacy Guard (GPC). [Fuente 1](https://git-scm.com/book/en/v2/Git-Basics-Tagging) [Fuente 2](https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf) [Fuente 3](http://rogerdudler.github.io/git-guide/index.es.html)
