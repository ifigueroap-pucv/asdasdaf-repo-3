# Glosario

1. **VC**: Sistema que gestiona los diversos cambios que se hacen sobre un proyecto. Los registra y quedan guardadas todas sus fases de desarrollo. [VC](https://hipertextual.com/archivo/2014/04/sistema-control-versiones/)
2. **DVC**: Permite trabajar a mas de un desarrollador sobre un proyecto desde distintos computadores y redes. Cada colaborador mantiene copia completa del repositorio. [DVC](http://www.mclibre.org/consultar/informatica/lecciones/git.html)
3. **Repositorio** 
	- **Remoto**: Versiones del proyecto que est�n alojadas en internet, al cual tienen acceso los colaboradores. Se les pueden asignar determinados permisos (Solo lectura, lectura y escritura, etc). [REMOTO](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
	- **Local**: Proyecto alojado en un directorio local. Es el que uno modifica localmente y va desarrollando. [LOCAL](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)
4. **Copia de Trabajo**: Archivos del proyecto que est�n alojados localmente, la copia del repositorio remoto. Es a los que le realizamos las modificaciones directamente. [Copia de Trabajo](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)
5. **Area de Preparacion**: Es un area temporal en el que se almacenan los cambios realizados en el proyecto (localmente), que ser�n incluidos en la proxima confirmacion. [Area de Preparacion](https://networkfaculty.com/es/video/detail/2604-git---area-de-preparacion-o-indice---standing-area)
6. **Preparar Cambios**: Estado en que se encuentran listos los cambios para ser confirmados. Estos archivos nuevos (con sus respectivas modificaciones) entran al dep�sito HEAD con la orden "git add". [Preparar Cambios](https://networkfaculty.com/es/video/detail/2604-git---area-de-preparacion-o-indice---standing-area)
7. **Confirmar Cambios**: Estado en que se encuentran los archivos listos para ser llevados a un objeto "commit". [Confirmar Cambios](https://www.siteground.es/kb/tutorial-git-comandos/)
8. **Commit**: Confirma los cambios realizados (que se encuentran en el index) y los registra en el repositorio. Generalmente va acompa�ado de un mensaje que explica el por que de los cambios. [Commit](https://www.youtube.com/watch?v=QGKTdL7GG24)
9. **Clone**: Crea copia del repositorio remoto al directorio del repositorio local. [CLONE](https://www.siteground.es/kb/tutorial-git-comandos/)
10. **Pull**: Actualiza repositorio local con posibles cambios que se hayan hecho en el repositorio remoto (por los colaboradores). Pull combina Fetch + Merge, lo cual actualiza y fusiona automaticamente las ramas. [PULL](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)
11. **Push**: Actualiza repositorio remoto con posibles cambios que se hayan hecho en el repositorio local por el colaborador.
12. **Fetch**: Actualiza y lleva los cambios solamente, desde el repositorio remoto al local. Los deja en otro branch y hay que realizar un "merge" para llevarlos a branch local. [Fetch](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)
13. **Merge**: Fusiona una o mas ramas en la rama actual en que se est�. [MERGE](https://www.siteground.es/kb/tutorial-git-comandos/)
14. **Status**: Consulta estado del area de preparacion, si es que se han realizado cambios o si no se han hecho. [Status](https://www.siteground.es/kb/tutorial-git-comandos/)
15. **Log**: Examina el registro de commit que se han realizado en la rama posicionada. Se muestra la de todos los colaboradores. [Log](https://www.siteground.es/kb/tutorial-git-comandos/)
16. **Checkout**: Mueve o nos posiciona en un commit (rama). Es para retroceder a versiones anteriores y ver como estaba el desarrollo en ese momento. [Checkout](https://www.siteground.es/kb/tutorial-git-comandos/)	
17. **Rama**: Union de commits. Se usan para trabajar en caracteristicas independientes y evitar desorden de commits. [Rama](https://www.youtube.com/watch?v=QGKTdL7GG24)
18. **Etiqueta**: Se le asigna a un commit, para acceder facilmente a �l despues, con  un checkout. [Etiqueta](https://www.siteground.es/kb/tutorial-git-comandos/)
 
	
