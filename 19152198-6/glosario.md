# Glosario

1. Control de versiones: Es un sistema que registra diferentes versiones realizadas,debido a las constantes modificaciones a los ficheros y permite el acceso a versione anteriores en caso de algún fallo. [Referencia](https://hipertextual.com/archivo/2014/05/git-sistema-control-versiones/)

2. Control de versiones distribuido (DVC): Consiste en que cada cliente administre la informacion en su totalidad, mediante la obtencion de  una copia del repositorio. De ese modo, pueden actuar no solo como cliente sino como servidor en cualquier momento. [Fuente](https://hipertextual.com/archivo/2014/05/git-sistema-control-versiones/)

3. Repositorio remoto y Repositorio local: El repositorio remoto son contenedores de archivos online desde donde se comparten a otros clientes .El repositorio local es aquel banco de datos que esta ubicado en el ordenador desde donde estoy trabajando, de modo que no se necesita conexión a internet para operar en él. [Fuente](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

4. Copia de trabajo / Working Copy: Es un duplicado de todo el contenido que posee el repositorio local y cualquier cliente que tenga una copia de ese repositorio puede trabajar en él. [Fuente](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

5. Área de Preparación / Staging Area: Es un zona virtual  que almacena los archivos a enviar en la proxima confirmación.Ademas, permite la revisión de cambios de manera ordenada y visualizar los errores al momento de fusionar ramas. [Referencia](https://stackoverflow.com/questions/49228209/whats-the-use-of-the-staging-area-in-git)

6. Preparar Cambios / Stage Changes: son cambios que han sido marcados para ser registrados en el repositorio la proxima ves que se haga un commit. [Referencia](https://howtogit.net/concepts/types-of-changes.html)

7. Confirmar cambios / Commit Changes:Ocurre cuando se cambia la ultima confirmación modificando el mensaje y por consiguiente ocurre un cambio en el hash del commit. [Referencia](https://www.tutorialspoint.com/git/git_commit_changes.htm)

8. Commit: Es un comando utilizado para indicar textualmente las modificaciones que se le hicieron a los archivos y registrarlo en el repositorio local. [Referencia](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)

9. clone: Es un comando utilizado para duplicar un repositorio que este en mi ordenador o en sitios remotos. [Referencia](http://rogerdudler.github.io/git-guide/index.es.html)

10. pull: Es un comando utilizado para buscar y descargar los datos almacenados en un repositorio remoto e instantaneamente   actualizar la información  al  repositorio local. [Referencia](https://www.atlassian.com/git/tutorials/syncing/git-pull)

11. push:  Es un comando utilizado para subir  los elementos  desde el repositorio local al remoto .El elemento puede ser un archivo modificado o una rama.nueva para su uso. [Referencia](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)

12. fetch: Es un comando utilizado para  descargar todos los  elementos contenidos en el repositorio remoto hacia el repostorio local. [Referencia](https://www.atlassian.com/git/tutorials/syncing/git-fetch)

13. merge: Es un comando utilizado para unir los cambios de una rama a otra,lo cual puede suceder de forma correcta o pueden ocurrir conflictos denotados como "sin fusionar". [Referencia](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

14. status: Es un comando que muestra el estado actual del repositorio e indica si hay archivos a confirmar,y muestra el estado del directorio de trabajo. [Referencia](https://githowto.com/checking_status)

15. log: Es un comando que muestra el registro historico de las confirmacion que se han realizado. [Referencia](https://mijingo.com/blog/understanding-git-log)

16. checkout: Es un comando utilziado para el cambio de rama o  restablecer las modificaciones que se hicieron en el directorio de trabajo.Sin embargo, los archivos nuevos y los que se encuentran en staging index permanecen sin cambios. [Referencia](https://git-scm.com/docs/git-checkout)

17. Rama / Branch:Es una linea de trabajo separada de la rama principal o master.El proposito de la rama es realizar modificaciones sin alterar la version original,puesto que,una vez que la modificación se haya realizado de manera completa  y este funcionando sin problemas, se vuelva a unir a la rama principal. [Referencia](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

18. Etiqueta / Tag: Es utilizado para designar un alias a puntos del registro historico, de modo que sea identificable.Gracias a las etiquetas es posible listar las versiones presentes y acceder a alguna de ellas si se desea. [Referencia](https://filisantillan.com/creando-etiquetas-con-git/)














