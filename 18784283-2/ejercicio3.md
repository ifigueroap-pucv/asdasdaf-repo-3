## Ramas
- Cantidad de ramas: 1
- nombre de la rama: master

## Etiquetas
- Cantidad de etiquetas: 0

## Commits

commit ddfc5151d3def9435fe80a55a16582a53deb6a0a (HEAD -> master)
Author: pedro morales martinez <pedro.ignaciom95@gmail.com>
Date:   Sun Aug 26 19:31:47 2018 -0300

    Se agrega el archivo glosario.md

commit 2848c404ba4243e34908cf09cbba9559c57e95d4 (origin/master, origin/HEAD)
Merge: d51de49 77110bc
Author: Enzo Aravena <enzoaravenaq@gmail.com>
Date:   Sun Aug 26 17:23:40 2018 -0300

    Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-3

commit 77110bc0c08ad86c0bd002b6df39425d018a503c
Merge: 5bdda0e c856fc3
Author: Jose Pablo Arancibia <jose.arancibia.l@mail.pucv.cl>
Date:   Sun Aug 26 16:44:49 2018 -0400

    Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-3
