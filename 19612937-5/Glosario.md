# Glosario 

----
## 1. Control de versiones (VC) 
Un sistema de control de versiones, como su nombre lo indica ayuda a la gesti�n de las diversas versiones de un proyecto, archivos, etc. Su funcionalidad es mantener un registro de las diversas versiones y cambios que se realizan sobre lo que se gestiona, permitiendo visualizar los autores de las modificaciones. Adem�s, permite reestablecer las versiones anteriores. [[referencia]](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)


## 2. Control de versiones distribuidos (DVC) 
Es un sistema de control de versiones, los cuales mantienen la informaci�n a trav�s de un servidor y varios dispositivos en paralelo, es decir la informaci�n se replica en cada dispositivo que tiene acceso al proyecto que est� siendo gestionado en el DVC. Esto permite que la informaci�n sea recuperable en caso de fallos. [[referencia]](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) 


## 3. Repositorio remoto y Repositorio local 
El repositorio es donde est�n almacenados los archivos en el sistema de control de versiones. Entre estos se encuentran dos tipos de repositorios.

a. Repositorio remoto: Es un repositorio que se encuentra en un punto de la red, es decir la informaci�n se mantiene almacenada en la internet.
b. Repositorio Local: Es un repositorio que se encuentra en un dispositivo particular, es decir aquel que se encuentra dentro del computador.[[referencia]](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)


## 4. Copia de trabajo / Working Copy 
Una copia de trabajo, es la versi�n del archivo en la cual se est� trabajando o realizando modificaciones.
[[referencia]](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

## 5. �rea de preparaci�n / Staging Area
Es un archivo que contiene los cambios que se realizaran al ejecutar el proximo commit.
[[referencia]](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

## 6.Preparar Cambios / Stage changes
Son los cambios realizados sobre un determinado archivo. Generalmente son las modificaciones que se guardaran el commit.[[referencia]](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

## 7. Confirmar Cambios /Commit changes
Es el resultado que se genera al realizar un commit. Ya sea al eliminar un archivo, agregarlo o editarlo.
[[referencia]](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

## 8. Commit 
Un commit es una confirmaci�n de los cambios que se han realizado sobre uno varios archivos de un determinado repositorio. El commit marca un punto desde el cual se puede volver o comparar con respecto a otras versiones del proyecto.[[referencia]](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

## 9. Clone
Se copia un repositorio en un directorio determinado.[[referencia]](https://git-scm.com/docs/git-clone)

## 10. Pull
Realiza un fetch de los archivos y los integra en una rama o repositorio local. [[referencia]](https://git-scm.com/docs/git-pull)

## 11. Push
Actualiza o sube los objetos determinados al repositorio remoto.Es decir, este comando permite compartir los archivos desde el repositorio local a uno remoto.
[[referencia]](https://git-scm.com/docs/git-push) 

## 12. Fetch
Descarga un archivo y la referencia desde un repositorio determinado. [[referencia]](https://git-scm.com/docs/git-fetch)

## 13. Merge
Fusiona ramas, guardando los cambios y referencias que tienen estas.[[referencia]](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b�sicos-para-ramificar-y-fusionar)

## 14. Status
Muestra el estado de los archivos y sus commit, adem�s de las diferencias que tiene con otras versiones de este mismo.[[referencia]] (https://git-scm.com/docs/git-status)


## 15. Log
Muestra un listado de los commit realizados.[[referencia]](https://git-scm.com/docs/git-log)

## 16. Checkout
Revisa las ramas y las diferentes versiones que tiene un archivo.[[referencia]](https://git-scm.com/docs/git-checkout)

## 17. Rama /Branch
Es un punto en la linea de desarrollo de un proyecto delimitada por un commit.[[referencia]](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-�Qu�-es-una-rama%3F)

## 18. Etiqueta / Tag
Nombre que se la a un punto en el historial de cambios de un archivo. [[referencia]](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)

----