#Glosario

##**Control de Versiones (VC):**
>Sirve para poder almacenar mi codigo online, para no perder informacion en caso de una falla de disco o para poder usarlo dentro de un grupo de trabajo online para asi no tener que estar todos juntos fisicamente.
[Control de Versiones](https://docs.google.com/presentation/d/1FHRL4JMMOyX0uM2CPxQ_WD1NlhIaX4JBi48F5eROz2Q/pub?slide=id.g19402deeea_0_1320)

##**Control de versiones distribuido (DVC):**
>Es cuando los clientes descargan la ultima version del archivo en el que se trabaja, y se tienen varios repositorios por cada cliente que la descarga, asi aunque se caiga el sistema, cualquiera de los clientes puede subir nuevamente el archivo.
[Control de Versiones Distribuido \(DVC)](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

##**Repositorio remoto y Repositorio local **
>El repositorio remoto son versiones de un proyecto en el cual se puede trabajar de forma online, dependiendo de los permisos que se tenga se podra solamente leer o si tienes mayores permisos , escribir y leer.
>El repositorio Local son los archivos del proyecto que estan dentro de tu computador actualmente el cual mediante comandos podra ser subido a internet.
[Repositorio remoto y Repositorio local](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

##**Copia de trabajo / Working Copy**
>Para poder copiar un repositorio es necesario usar el comando `git clone [url]` dejando la url del archivo que quieras clonar.
[Copia de trabajo](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

##**Area de Preparacion /Staging Area**
>El area de preparacion es un archivo contenido en tu directorio de git que guarda la informacion que ira en tu siguiente `git commit`
[Area de Preparacion](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

##**Preparar Cambios / Stage Changes**
>Cada vez que quieras guardar o subir tus cambios del repositorio, sera necesario dejar el archivo preparado, para esto se puede usar el comando `git add` , en ese entonces estara preparado el archivo, y para poder verificar su estado se puede usar el comando `git status`
[Preparar Cambios](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

##**Confirmar cambios / Commit Changes**
>Despues de hacer el paso de Preparar Cambios, estos se confirman con el comando `git commit` para poder subirlos al repositorio.
[Confirmar cambios](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

##**Commit**
>Comando para poder confirmar datos y enviar al repositorio
[Commit](https://git-scm.com/docs/git-commit)

##**Clone**
>Comando que sirve para clonar un repositorio local o remoto
[Clone](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

##**Pull**
>Comando que sirve para cargar y unir una rama
[Pull](https://git-scm.com/docs/git-pull)

##**Push**
>Comando que sirve para subir un contenido de un repositorio local a uno remoto
[Push](https://git-scm.com/docs/git-push)

##**Fetch**
>Comando que sirve para descargar desde un repositorio remoto al local, el cual tambien sirve para ver en que estan trabajando los demas usuarios.
[Fetch](https://git-scm.com/docs/git-fetch)

##**Merge**
>Comando que sirve para agregar cambios a una rama, pudiendo unirlas.
[Merge](https://git-scm.com/docs/git-merge)

##**Status**
>Comando que sirve para ver el estado del repositorio
[Status](https://git-scm.com/docs/git-status)

##**Log**
>Comando que sirve para ver el historial de comandos commits usados en el repositorio
[Log](https://git-scm.com/docs/git-log)

##**Checkout**
>Sirve para crear una rama nueva, o cambiar por otra rama que exista.
[Checkout](https://git-scm.com/docs/git-checkout)

##**Rama / Branch**
>Sirven para trabajar en lineas de desarrollo dentro del proyecto de forma paralela.
[Rama / Branch](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

##**Etiqueta / Tag**
>sirve para marcar puntos en la version o historia.
[Etiqueta / Tag](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)

