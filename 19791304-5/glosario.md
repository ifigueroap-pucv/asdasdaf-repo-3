#Taller 00 - Git y Bitbucket
####Katherine Ar�valo Parker
____

#Glosario

#####**a. Control de versiones (VC)**
Es el sistema que registra todos los cambios que se le han hecho al archivo a lo largo del tiempo.
[Ver fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

#####**b. Control de versiones distribuido (DVC)**
En este sistema de control de versiones no solo se descarga la ultima versi�n, sino que se replica completamente el repositorio, haciendo as� una copia de seguridad completa de todos los datos.
[Ver fuente]( https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

#####**c. Repositorio remoto y Repositorio local**
Los repositorios remotos son versiones del proyecto que se encuentran alojados en Internet o en alg�n punto de la red. El repositorio local es la versi�n del proyecto que se encuentra en tu propia computadora.
[Ver fuente 1](http://michelletorres.mx/como-conectar-un-repositorio-local-con-un-repositorio-remoto-de-github/),
[Ver fuente 2](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

#####**d. Copia de trabajo / Working Copy**
Cada usuario tiene una copia de trabajo del proyecto entonces hay varias copias del repositorio las cuales guardan los cambios hechos por el usuario en el repositorio local y estos cambios se devuelven al repositorio remoto para ser vistos por todos los usuarios.
[Ver fuente](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

#####**e. �rea de Preparaci�n / Staging Area**
Es un area que almacena informaci�n sobre lo que se incluir� en su pr�ximo "commit".
[Ver fuente](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

#####**f. Preparar Cambios / Stage Changes**
Luego de editar los archivos en tu proyecto viene la fase de preparaci�n de cambios la cual se realiza con git add el cual selecciona los cambios que se har�n con git commit.
[Ver fuente](https://www.atlassian.com/git/tutorials/saving-changes)

#####**g. Confirmar cambios / Commit Changes**
La confirmaci�n de cambio viene luego de la etapa de preparaci�n, se procede a guardar los cambios por etapas, esto queda en el historial.
[Ver fuente](https://www.atlassian.com/git/tutorials/saving-changes)

#####**h. Commit**
Comando que act�a sobre una colecci�n de archivos y directorios realizando los cambios.
[Ver fuente](https://www.atlassian.com/git/tutorials/saving-changes/git-commit)

#####**i. clone**
Comando que permite descargar un repositorio remoto a mi repositorio local.
[Ver fuente](https://www.git-tower.com/learn/git/commands/git-clone)

#####**j. pull**
Comando que descarga el contenido desde un repositorio remoto y actualiza inmediatamente el repositorio local para que coincida con ese contenido.
[Ver fuente](https://git-scm.com/docs/git-pull)

#####**k. push**
Comando que carga contenido de repositorio local a un repositorio remoto.
[Ver fuente](https://www.atlassian.com/git/tutorials/syncing/git-push)

#####**l. fetch**
Comando que descarga el contenido de un repositorio remoto a uno local, es como el pull pero con la diferencia de que no le obliga a fusionar los cambios en su repositorio.
[Ver fuente](https://www.atlassian.com/git/tutorials/syncing/git-fetch)

#####**m. merge**
Comando que permite tomar las l�neas independientes de desarrollo creadas por git branch e integrarlas en una sola rama.
[Ver fuente](https://www.atlassian.com/git/tutorials/using-branches/git-merge)

#####**n. status**
Comando que muestra el estado del directorio de trabajo y el �rea de preparaci�n.
[Ver fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)

#####**o. log**
Comando que muestra el historial de commit, a diferencia del status, log solo opera en el historial.
[Ver fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)

#####**p. checkout**
Comando que permite navegar en las ramas del repositorio, con el se pueden ver las confirmaciones antiguas.
[Ver fuente](https://git-scm.com/docs/git-checkout)

#####**q. Rama / Branch**
Es un apuntador el cual apunta a cada una de las confirmaciones realizadas, as� pueden haber distintas ramas de trabajo al mismo tiempo.
[Ver fuente](https://www.atlassian.com/git/tutorials/using-branches)

#####**r. Etiqueta / Tag**
Son referencias que apuntan a puntos espec�ficos en el historial de Git, esto sirve para ubicarse mejor.
[Ver fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)