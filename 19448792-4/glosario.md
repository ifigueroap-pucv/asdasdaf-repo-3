# GLOSARIO

En el presente documento se detallará una lista itemizada, con los términos y
sus definiciones.

 ### 1.- Control de versiones (VC)
    Gestión de los cambios que se pueden realizar en la configuración o estructura de un producto informático(software), tambien se utiliza en el control de las versiones y ediciones de este. Por último, permite mantener un registro de los cambios que se van realizando en el producto.
<https://es.wikipedia.org/wiki/Control_de_versiones>

 ### 2.- Control de versiones distribuido (DVC)
    Similar al VC, sin embargo la diferencia es que ademas DVC permite a los desarrolladores de un software trabajar en un proyecto en común sin la necesidad de compartir una misma red.
    

 <https://es.wikipedia.org/wiki/Control_de_versiones_distribuido>

    
 ### 3.- Repositorio Local y Repositorio Remoto
    Los Repositorios Locales(RL) son versiones de un proyecto que se almacenan en tu propio dispositivo, se utilizan cuando no es necesario conectarse a  internet para manejar las versiones de un proyecto. Los Repositorios Remotos(RR) a diferencia de los RL, se encuentran almacenados en Internet o en algún punto de la red. Se pueden tener varios de los cuales algunos pueden ser de sólo lectura, o de lectura/escritura, según los permisos que se hayan configurado. Gestionar repositorios locales y remotos significa mandar y recibir datos de ellos cuando necesites compartir cosas, ademas de añadir repositorios nuevos, eliminar aquellos que ya no son válidos, gestionar ramas remotas e indicar si están bajo seguimiento o no, etc.
    
    
<http://michelletorres.mx/como-conectar-un-repositorio-local-con-un-repositorio-remoto-de-github>
    
    
  ### 4.- Copia de trabajo / Working Copy
    Una vez creado un proyecto en un Repositorio se puede crear una Copia de Trabajo o Working Copy que basicamente es el Directorio o Carpeta donde se encuentra la copia de nuestro proyecto que debería estar sincronizada con la copia del repositorio.
    
<https://rdlab.cs.upc.edu/docu/html/manual_subversion/ManualSubversion-rdlab_es.html>

### 5.- Área de Preparación 
    Corresponde a un archivo o area que normalmente se ubica en el directorio del respositorio local y se utiliza para almacenar información sobre las modificaciones o cambios que irán en la próxima confirmación de la nueva versión.
    
<https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git>

### 6.- Preparar Cambios o Stage Changed
    Es una acción que se realiza al momento modificar el proyecto, sin embargo esta modificación que ha sido llevada a cabo se encuentra en la Stage Changed ya que no es permanente o no esta confirmada definitamente en el repositorio.
    
<https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git> , <https://githowto.com/staging_changes>


### 7.- Confirmar cambios o Commit Changes
    Es una acción que se realiza al momento de haber subido o corfirmado los datos o  la nueva versión del proyecto en el repositorio, esto implica que los datos han sido almacenado en la base de datos de forma exitosa.
    
<https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git>

### 8.- Commit
    Es un Comando que identifica los cambios hechos en el proyecto y manda estos cambios al repositorio local o Head. 
    
<https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git>

### 9.- Clone
    Clone es un comando que se realiza para obtener una copia de un Repositorio existente, en caso de querer contribuir o conocer otros proyectos. Al realizar el comando clone se copiara cada archivo de la historia de un proyecto.
    
<https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git>

### 10.- Pull
    Es un comando que actualiza el Repositorio Local hasta el ultimo commit realizado. Solo se utiliza cuando se esta seguro y no dudoso de los cambios que pueda traer el Repositorio Remoto. El comando pull es una fusión del comando fetch mas el comando merge.
    
<http://rogerdudler.github.io/git-guide/index.es.html>

### 11.- Push
    Es un Comando que identifica los cambios realizados en un proyecto ubicado en el Head o copia local para luego mandarlos al Repositorio Remoto y de esta manera se veran reflejados los cambios en el Repositorio Remoto.
    
<https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git>

### 12.- Fetch
    Comando que se utiliza para bajar los cambios desde el Repositorio Remoto a la rama origin/master la cual es una rama oculta que siempre existe(En esta rama usted puede visualizar los cambios en dicha rama), luego con el comando merge, puedes bajar esos cambios a la rama local. 
    
<https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git>

### 13.- Merge
    Comando que se utiliza para bajar los cambios de una rama determinada (por ejemplo origin/master) a la rama local.
    
<https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git>

### 14.- Status
    Es un comando que se utiliza para determinar el estado de los archivos, normalmente se utiliza para detectar que los commit han sido agregados exitosamente o tambien para encontrados datos que no han sido agregados.
    
<https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio>

### 15.- Log
    Es un comando que se utiliza para conocer el histórico de confirmaciones o modificaciones realizadas anteriormente en un proyecto. Al ejecutar log se mostrará por pantalla un listado ordenado de manera cronologicamente con las modificaciones, detallando la suma de comprobacion, el nombre y direccion de correo del autor, fecha y el mensaje de confirmación.
    
<https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones>


### 16.- Checkout 

    Es un comando que se utiliza para crear una nueva rama y saltar a ella. Estas ramas pueden ser utilizadas para seguir trabajando o modificando un proyecto de manera paralela sin afectar el proyecto original en funcionamiento.
    
<https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar>

### 17.- Rama o Branch
    Es un termino que hace referencia a un apuntador el cual por defecto siempre apunta a la ultima confirmación realizada en el proyecto, en caso de querer utilizar otra rama, se crea con el comando "branch" <nombreRama> y de esta manera se crea un nuevo apuntador libre con el nombre "nombreRama" que apunta a la última confirmación del proyecto y en donde se podrá trabajar libremente sin afectar el proyecto original.
    
<https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F>

### 18.- Etiqueta o Tag
    Es un comando que se utiliza con el objetivo de etiquetar puntos especificos en la historia de proyecto como importantes.
    
<https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas>

