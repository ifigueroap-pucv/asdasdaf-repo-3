#Parte 1 Taller 0
## Glosario

1. Control de Versiones (VC): Herramienta util que soluciona problemas de administracion en un proyecto facilitando este mismo.[Control de Versiones](https://es.wikipedia.org/wiki/Control_de_versiones)

2. Control de versiones distribuido (DVC): Herramienta que permite que los clientes repliquen completamente el repositorio evitando la perdida de estos por la muerte o caida de un servidor.[DVC](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
 
3. Repositorio remoto y Repositorio local: El repositorio remoto es una version del repositorio global alojado en una direccion o en internet en una red distinta. Un repositorio remoto no tiene porque ser igual a un repositorio local/global.
Un repositorio local es equivalente a decir repositorio global, el cual es el primer repositorio creado para algun proyecto.[Repositorio remoto y Repositorio Local](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

4. Copia de trabajo / *Working Copy*: La copia de trabajo es una copia de un repositorio, ya sea local/global o remoto, lo que permite realizar cambios en alguno de estos
y evitar que el original cambie y ocurra algun error inesperado.[Working Copy](http://rogerdudler.github.io/git-guide/)

5. Aréa de preparacion / *Staging Area*: Staging area es un area temporal donde se añaden archivos cuyos cambios estan a punto de enviarse en forma de commit. Esta area realiza las siguientes funciones:
Modificar archivos dentro del directorio de trabajo, Seleccionar aquellos archivos cuyos cambios queremos enviar y agregarlos al staging area y confirmar esos cambios para que se almacenen en el repositorio
(commit). <a href="http://www.youtube.com/watch?feature=player_embedded&v=QGKTdL7GG24" target="_blank"><img src="http://img.youtube.com/vi/QGKTdL7GG24/0.jpg" alt="TE ENSEÑO GIT EN 30 MINUTOS" width="100" height="40" border="10" /></a>

6. Preparar cambios / *Stage Changes*: Es el proceso de creacion del commit. Lo que se realiza en este paso es "documentar" un cambio hecho en el proyecto, ya sea en alguna linea de codigo o agregar una funcion.[Stage Changes](https://githowto.com/staging_changes)

7. Confirmar cambios / *Commit Changes*: Es el proceso en el cual se guardan los cambios y se envian al repositorio para que sean almacenados.[Commit Changes](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

8. *Commit*: Es la confirmacion de un conjunto de cambios realizados en el proyecto y quedan almacenados en el repositorio. Los commit se pueden eliminar o deshacer, pero no se pueden modificar. Una vez creado
el commit, quedará almacenado en el repositorio. [Commit](https://es.wikipedia.org/wiki/Commit)

9. *Clone*: Comando de la libreria GIT que premite clonar un repositorio en un nuevo directorio. Si el repositorio clonado contiene ramas, estas tambien son copiadas.[Clone](https://git-scm.com/docs/git-clone)

10. *Pull*: Comando de la libreria GIT que permite sincronizar cambios procedentes de un repositorio origen. Se usa para poder mantener un orden en el proyecto y que no existen confusion entre ramas y repositorios. <a href="http://www.youtube.com/watch?feature=player_embedded&v=QGKTdL7GG24" target="_blank"><img src="http://img.youtube.com/vi/QGKTdL7GG24/0.jpg" alt="TE ENSEÑO GIT EN 30 MINUTOS" width="100" height="40" border="10" /></a>

11. *Push*: Comando de la libreria GIT que permite enviar cambios entre repositorios. Se utiliza para que exista conocimiento de todos los cambios realizados para todas las personas que trabajan en el proyecto. <a href="http://www.youtube.com/watch?feature=player_embedded&v=QGKTdL7GG24" target="_blank"><img src="http://img.youtube.com/vi/QGKTdL7GG24/0.jpg" alt="TE ENSEÑO GIT EN 30 MINUTOS" width="100" height="40" border="10" /></a>

12. *Fetch*: Comando de la libreria GIT que permite descargar objetos y referencias desde otro repositorio.[Fetch](https://git-scm.com/docs/git-fetch)

13. *Merge*: Comando de la libreria GIT que permite fusionar ramas. Se utiliza para añadir algun cambio en otra rama. Si se realiza un *Merge* de una rama en otra sin haber tocado los mismos archivos los cambios se realizan bien.
Si se toca el mismo archivo en ambas ramas, pero se editan partes separadas, GIT detecta que no se ha tocado la misma linea o region (codigo) y se SUELE integrar bien. Por otro lado, si se ha editado el mismo archivo en ambas
ramas y se modifica la misma linea o region de codigo, GIT genera conflicto porque no sabe con que modificacion quedarse.<a href="http://www.youtube.com/watch?feature=player_embedded&v=QGKTdL7GG24" target="_blank"><img src="http://img.youtube.com/vi/QGKTdL7GG24/0.jpg" alt="TE ENSEÑO GIT EN 30 MINUTOS" width="100" height="40" border="0" /></a>

14. *Status*: Comando de la libreria GIT que permite mostrar el estado de todo lo que se esta trabajando (Repositorios, archivos, ramas, etc). Se utiliza principalmente para mantener un control del proyecto.[Status](https://git-scm.com/docs/git-status)

15. *Log*: Comando de la libreria GIT que permite mostrar todos los commit realizados. Este comando muestra los commit con el nombre del creador, correo, cuerpo del mensaje y una id unica del commit. <a href="http://www.youtube.com/watch?feature=player_embedded&v=QGKTdL7GG24" target="_blank"><img src="http://img.youtube.com/vi/QGKTdL7GG24/0.jpg" alt="TE ENSEÑO GIT EN 30 MINUTOS" width="100" height="40" border="10" /></a>

16. *Checkout*: Comando de la libreria GIT que tiene 2 funciones, la primera es que permite cambiar de rama para trabajar en ella y la segunda es que permite examinar commit realizados previamente.[CheckOut](https://git-scm.com/docs/git-checkout)

17. Rama/Branch: Las ramas son apuntadores que especifican al usuario donde esta situado. Por ejemplo, por defecto se tiene la rama *master*, el usuario puede crear otras ramas para separar trabajos o funciones especificas con el fin
de mantener un orden en el proyecto.[Branch](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

18. Etiqueta/Tag: Las etiquetas son alias que se le asignan a commit concretos para poder acceder de manera mas facil y tener un control de versiones de los cambios del proyecto.[Tag](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)