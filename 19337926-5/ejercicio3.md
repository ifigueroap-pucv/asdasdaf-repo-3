# Ejercicio 3

1. La carpeta contiene <1> branch de nombre: Master

2. Contiene <3> etiquetas:
	tag: 1.0.0: Carga de glosario.
	tag: 1.0.1: Carga de datos.
	tag 1.0.3: confirmacion ejercicio.

3. Últimos commits (desde el más antiguo)

  | **Autor**                       			  | **Mensaje**                                         | **Codigo hash**                                       |
  | ----------------------------------------------| ----------------------------------------------------| ----------------------------------------------------- |
  | Benjamin Tapia <benjamin.tapia.rom@gmail.com> | Se agrega archivo glosario.md (glosario actualzado.)| 57a3d05b5bfa9664200b37f62bd1d23fbdab1abc  (tag: 1.0.0)|
  |                                               | Se añade el archivo a carpeta personal              |                 									    |
  | Benjamin Tapia <benjamin.tapia.rom@gmail.com> | Se sube archivo datos.md                            | b9abbd274a9d3cb647c0b1107b22bcb5dd63d466 (tag: 1.0.1) |
  |               	                              | Se sube archivo a carpeta personal con lo pedido.   |                                                       |
  | Benjamin Tapia <benjamin.tapia.rom@gmail.com> | se agrega y confirma el archivo ejercicio3.md       | 4ec2e2d21d7b93dbf22098b2c097be9df233202f  (tag: 1.0.3)|
  |                                               |                                                     |                                                       |


	
commit 4ec2e2d21d7b93dbf22098b2c097be9df233202f (tag 1.0.2)
Author: Benjamin Tapia <benjamin.tapia.rom@gmail.com>
Date:   Tue Aug 28 17:05:26 2018 -0300

    ultima actualizacion , commit variable.


commit b9abbd274a9d3cb647c0b1107b22bcb5dd63d466 (tag: 1.0.1)
Author: Benjamin Tapia <benjamin.tapia.rom@gmail.com>
Date:   Tue Aug 28 14:36:54 2018 -0300

    datos modificados.

commit 57a3d05b5bfa9664200b37f62bd1d23fbdab1abc (tag: 1.0.0)
Author: Benjamin Tapia <benjamin.tapia.rom@gmail.com>
Date:   Tue Aug 28 14:28:45 2018 -0300

    glosario actualizado.

