# Glosario de Conceptos

a. **Control de versiones (VC)**              : Se denomina Control de versiones, a la gestión de diversos cambios realizados sobre archivos y códigos fuente en
						función del tiempo, en la cual es posible coordinar y manejar de mejor forma las versiones anteriores con la
						especificación de los cambios realizados al código.
						(https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

b. **Control de versiones distribuido (DVC)** : En el control de versiones distribuido es posible interactuar con otros clientes, ya que su comportamiento
						se distribuye en diversas partes, que en su totalidad componen un todo. Esto es útil cuando se cae un sistema, 
						ya que el servidor permite seguir la interacción y la colaboración entre clientes. Esta forma de gestión soluciona
						un problema de  los VC centralizados, duplicando el repositorio de cada cliente, haciendo una copia de seguridad
						en los mismos. (https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

c. **Repositorio Remoto y Repositorio Local** : Los repositorios remotos son versiones del proyecto en el cual estas trabajando y que se encuentran en internet
						o guardados en algun servidor, estos servidores trabajan en función de puntos de redes, los cuales pueden establecer
						procesos de lectura (read) y lectura-escritura(read/write). Esto implica que la colaboración de proyectos está en función
						de trabajo en repositorios locales,los cuales luego se suben a repositorios remotos en donde se envía y recibe información.
						(https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) 

d. **Copia de Trabajo** / *working copy*      :	Es una copia de el repositorio principal, en el cual es posible la edición y testeo de de caracteristicas sin afectar el proyecto principal.
						(https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

e. **Area de Preparacion** / *Staging Area*   : Es un archivo, generalmente ubicado en el directorio Git que almacena información acerca de lo que irá en la próxima versión, 
					       en el flujo de trabajo este se encuentra entre el directorio de trabajo y el repositorio.
					       (https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

f. **Preparar Cambios** / *Stay Change*       : Actúa como área intermedia, en la cual antes de confirmar, se preparan los cambios de un archivo antes de realizarlos.
						(https://git-scm.com/about/staging-area)

g. **Confirmar Cambios** / *Commit Changes*   : Es la confirmación de cambios realizados anteriormente, para que sean enviados al repositorio.
						(https://git-scm.com/docs/git-commit)

h. **Commit** 				  			      :	Confirma y sube los cambios realizados anteriormente, guardandolos en el repositorio del proyecto principal.
						(https://git-scm.com/docs/git-commit)

i. **clone** 				      			  : Genera una copia de un repositorio en un nuevo directorio.
						(https://git-scm.com/docs/git-clone)

j. **pull** 				      			  :	Esta acción en git obtiene archivos desde un repositorio remoto y los traslada al repositorio actual para actualizarlos.
						(https://git-scm.com/docs/git-pull)

k. **push** 				      			  :	Actualiza referencias remotas con respecto a las del repositorio local.
						(https://git-scm.com/docs/git-push)

l. **fetch** 				      			  :	Descargar archivos desde un repositorio.
						(https://git-scm.com/docs/git-fetch)

m. **merge** 				      			  :	Corresponde a La unión de dos o más versiones de un archivo.
						(https://git-scm.com/docs/git-merge)

n. **status** 				      			  :	Muestra el estado actual de un eventual directorio.
					        (https://git-scm.com/docs/git-status)

o. **log** 				      				  :	Muestra el registro de cambios realizados anteriormente.
						(https://git-scm.com/docs/git-log)

p. **checkout**			      				  :	Permite cambiar de rama o volver al estado anterior (restaurar) una versión del repositorio.
					        (https://git-scm.com/docs/git-checkout)

q. **Rama / Branch** 						  : Corresponden a directorios o sub-directorios del "working area" con referencia a los commits.
						(https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)

r. **etiqueta / Tag** 		      			  : Actúan como IDs, es decir Identificación  de las versiones en git.
						(https://git-scm.com/docs/git-tag)
