a. Control de versiones (VC) : El control de versiones es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos
a lo largo del tiempo, de modo que puedas recuperar versiones especificas mas adelante. Cualquier tipo de archivos que encuentres en un ordenador
puede ponerse bajo control de versiones

se llama control de versiones a la gestion de los diversos cambios que se realizan sobre los elementos de algun producto o una configuracion del 
mismo.Una version, revision o edicion de un producto, es el estado en el que se encuentra el mismo en un momento dado de su desarrollo o 
modificacion

Debe proporcionar 

.Mecanismo de almacenamiento de los elementos que deba gestionar
.Posibilidad de realizar cambios sobre los elementos almacenados
.Registro historico de las acciones realizadas con cada elemento o conjunto de elementos


b. Control de versiones distribuido (DVC) : Ofrece soluciones para los problemas que tiene un sistema de control de versiones centralizado 
(como por ejemplo si el servidor se cae durante n tiempo entonces durante ese n tiempo nadie puede colaborar o guardar cambios versionados
de aquello en que estan trabajando). Los clientes no solo descargan la ultima copia instantanea de los archivos, sino que se replica 
completamente el repositorio. De esta manera, si un servidor deja de funcionar y estos sistemas estaban colaborando a traves de el, cualquiera
de los repositorios disponibles en los clientes puede ser copiado al servidor con el fin de restaurarlo.

Carateristicas principales

.Necesita menos veces estar conectado a la red para hacer operaciones
.Aunque se caiga el repositorio remoto la gente puede seguir trabajando
.Existe menos necesidad de Backups, sin embargo, los backups siguen siendo necesarios  
.El servidor remoto requiere menos recursos que los que necesitarian un servidor centralizado ya que gran parte del trabajo lo realizan los repositorios locales


c. Repositorio remoto y Repositorio local :

	-Repositorio remoto = Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en alg�n punto 
	de la red.Puedes tener varios, cada uno de los cuales puede ser de s�lo lectura, o de lectura/escritura, seg�n los permisos que tengas.

	-Repositorio local = Los repositorios locales son parecidos a los remotos pero con la diferencia de que estos actuan de forma local en
	tu dispositivo 	

d. Copia de trabajo / Working Copy : Al utilizar el comando git clone tenemeos una copia de un repositorio existente, el cual crea un directorio
, luego inicializada como git en su interior descarga toda la informacion de ese repositorio y saca una copia de trabajo de la ultima version, con
ello tendremos listos los archivos del proyecto para poder utilizarlos 

e. �rea de Preparaci�n / Staging Area : El area de preparacion es un archivo, generlmente contenido en el direcotrio Git, que guarda informacion acerca
de lo que va a ir en tu proxima confirmacion . Si la version concreta de un archivo ha sufirdo cambios desde que se obtuvo del repositorio, pero ha
sido a�adida al area de preparacion, esta esta preparada (en estado staged)

f. Preparar Cambios / Stage Changes : Tambien llamado como "Standing" indica a git que los conflictos han sido resueltos, con ello podemos utilizar
el comando commit para terminar de guardar los cambios. si no se prepara el cambio todo quedara como archivos modificados en el disco

g. Confirmar cambios / Commit Changes : Luego de tener todo en estado preparado, tenemos que usar git commit, lo que nos lleva a confirmar los
cambios realizados.Con esta accion se abrira el editor de preferencia y mostrara un mensaje de confirmacion

h. Commit : Comando de git que se utiliza para identificar los cambios hechos en el ambiente de trabajo

i. clone : Comando de git que se utiliza para clonar repositorios ya existentes 

j. pull : Comando de git que se utiliza para sincronizar la copia del repositorio remoto a la copia local

k. push : Comando de git que se utiliza para subir los cambios hechos en el ambiente de trabajo a una rama de trabajo de un equipo remoto

l. fetch : Comando de git que se utiliza para recuperar datos de tu repositorio remoto,con ello obtener todas las referencias a todas las ramas der
repositorio remoto

m. merge : Se utiliza para fusionar uno o mas ramas dentro de la rama que tienes activa

n. status : Comando que se usa para saber que archivos estan en que estado 

o. log : Comando que lista las confirmaciones hechas sobre el repositorio en orden cronologico inverso, las confirmaciones mas recientes
se muestran al principio

p. checkout : Comando que se usa para saltar entre ramas

q. Rama / Branch : Representa una linea de desarrollo independiente.Las ramas sirven como una abstraccion para el proceso de edicion.El comando
branch te permite crear, listar y cambiar de nombre y eliminar ramas

r. Etiqueta / Tag : Comando que se usa para etiquetar puntos especificos en la historia como importantes.Generalmente se usa para marcar puntos
importantes como la creacion de una version 

-----------Fuentes-------------

a:
https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
https://es.wikipedia.org/wiki/Control_de_versiones#Ventajas_de_sistemas_distribuidos

b:
https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones
https://es.wikipedia.org/wiki/Control_de_versiones#Ventajas_de_sistemas_distribuidos

c:
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/ 

d:
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

e:
https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git

f:
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

g:
https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio

h:
https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git

i:
https://git-scm.com/book/es-ni/v1/Git-Basics-Obteniendo-un-Repositorio-Git

j:
https://github.com/susannalles/MinimalEditions/wiki/Lista-Comandos-Git

k:
https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git

l:
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

m:
https://git-scm.com/book/es/v2/Appendix-B%3A-Comandos-de-Git-Ramificar-y-Fusionar

n:
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

o:
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones

p:
https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F

q:
https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F

r:
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas
