﻿# Glosario

------

| Término o Concepto | Descripción | Fuente |
| ------ | ------ | ------ |
| a) Control de Versiones (VC) | Es un sistema que registra las modificaciones realizadas sobre un archivo o un conjunto de estos durante el tiempo. Permite recuperar versiones anteriores de dicho archivo. | [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) |
| b) Control de Versiones Distribuido (DVC) | En estos sistemas de control los clientes tienen acceso a la última instantánea de los archivos, replicando completamente el repositorio. | [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) |
| c) Repositorio local/remoto | Un repositorio es un espacio centralizado donde se almacena, organiza, mantiene y difunde información digital, habitualmente archivos informáticos. Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en algún punto de la red, mientras que los repositorios locales son versiones de tu proyecto que se encuentran en tu ordenador. | [Fuente](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/) [Fuente](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-remotos/)|
| d) Copia de Trabajo / Working Copy | Es una copia local de los archivos del proyecto o repositorio. | [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git) |
| e) Área de preparacion / Staging Area | Es un archivo, generalmente contenido en el directorio de Git, almacena informacion sobre lo que pasará en el próximo commit. | [Fuente](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics) |
| f) Preparar Cambios / Stage Changes | Es el paso cuando un conjunto de cambios puede ser modificado antes de un commit. | [Fuente](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git) |
| g) Confirmar Cambios / Commit Changes | Paso en donde el Area de preparacion se confirma. | [Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio#r_committing_changes) |
| h) Commit | Comando que permite confirmar los cambios realizados en el repertorio local. | [Fuente](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git) |
| i) Clone | Cuando se ejecuta este comando se obtiene cada versión de cada archivo de la historia del proyecto, clona un repositorio. | [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git) |
| j) Pull | Comando que permite recibir datos de un repositorio remoto. | [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) |
| k) Push | Comando que permite subir datos a un repositorio remoto. | [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) |
| l) Fetch | Comando que permite bajar, recibir o descargar de un repositorio remoto. | [Fuente](https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull) |
| m) Merge | Fusiona una rama con la rama activa. | [Fuente](https://git-scm.com/docs/git) |
| n) Status | Comprueba el estado de los archivos. | [Fuente](https://git-scm.com/docs/git) |
| o) Log | Permite ver el historial de commits. | [Fuente](https://desarrolloweb.com/articulos/git-log.html) |
| p) Checkout | Actualiza los archivos en el árbol de trabajo para que coincida con la versión de la ruta especificada. Si no se especifica ruta, se actualizará la rama apuntada al HEAD. | [Fuente](ttps://git-scm.com/docs/git-checkout) |
| q) Rama / Branch | Se utiliza para realizar tareas aisladas de las demas, pueden ser unidas a la rama principal. | [Fuente](https://git-scm.com/docs/git-branch) |
| r) Etiqueta / Tag | Comando que permite dar etiquetas. | [Fuente](https://git-scm.com/book/en/v2/Git-Basics-Tagging) |



