# Glosario

1. Control de versiones (VC): Con el control de versiones que posee git, uno puede regresar
o revisar versiones anteriores del archivo, saber cu�ndo y donde se origin� el problema,
quien lo introdujo, cuando fue la �ltima modificaci�n, etc. B�sicamente si pierdes archivos
es un m�todo para identificar problemas y restaurar el archivo.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

2. Control de versiones distribuido (DVC): Consiste en replicar los repositorios, logrando
hacer que todos los que est�n trabajando con �l tengan una copia del repositorio original, ya
sea en el caso de p�rdida del servidor original, un cliente solo tendr� que subir su repositorio
actual para su restauraci�n.
[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

3. Repositorio remoto y Repositorio local:

	* Repositorio remoto: Es la versi�n del git actual con el que trabajas hospedada en alg�n
	servidor de internet.
	[Fuente](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

	* Repositorio local: Es el repositorio hospedado en tu ordenador.
	[Fuente](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

4. Copia de trabajo / *Working Copy*: B�sicamente es una forma de respaldar el trabajo actual, ya que si se llega
a perder el repositorio actual, no habr�a forma de recuperarlo, a menos de que se haga una copia para
restaurarlo.
[Fuente](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

5. Preparar Cambios / *Stage Changes*: Esta es la primera etapa de dos para confirmar los cambios. Esta consiste
en la puesta en escena de ellos, y prepara los cambios para que luego se apliquen al original, en este
estado se pueden modificar esos cambios hasta que se est� conforme y luego se apliquen con la segunda confirmaci�n.
[Fuente](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)

6. Confirmar Cambios / *Commit Changes*: Esta es la segunda de dos confirmaciones para aplicar los cambios, y
consiste en enviar los cambios al repositorio original (en el cual se est� trabajando) y los guarda.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

7. *Commit*: Consiste en registrar los cambios en el repositorio, posee una corta descripci�n, seguido de
una m�s detallada y est� vinculado al cambio efectuado en el repositorio. Con esto, los dem�s usuarios/clientes
podr�n ver los cambios hechos, en que consiste y el
porque de ello.
[Fuente](https://git-scm.com/docs/git-commit)

8. *Clone*: Como dice su nombre, clona, en este caso, clona el repositorio actual en un directorio nuevo, esto,
seguido de nuevas ramas para cada rama en el repositorio clonado, junto con una rama inicial la cual se divide
de la actual activa del clonado.
[Fuente](https://git-scm.com/docs/git-clone)

9. *pull*: Actualiza la rama actual con los cambios de un repositorio remoto, es similar a un fetch, solo que est�
junto con un merge para luego fusionar las ramas recuperadas en la rama actual.
[Fuente](https://git-scm.com/docs/git-pull)

10. *push*: Consiste en actualizar los repositorios remotos con los cambios efectuados en el repositorio local en el
cual se trabaja.
[Fuente](https://git-scm.com/docs/git-push)

11. *fetch*: Se obtienen las ramas de el/los repositorios para ir completando la actual, en pocas palabras,
si se trabaja con un repositorio remoto, y este sufre cambios, para poder adquirir esos cambios en el repositorio
local, es necesario realizar un fetch.
[Fuente](https://git-scm.com/docs/git-fetch)

12. *merge*: Consiste en incorporar los commits nombrados desde que se bifurcaron las ramas en la rama actual,
esto significa que es capaz de fusionar todas las ramas en una.
[Fuente](https://git-scm.com/docs/git-merge)

13. *status*: Compara las rutas del archivo �ndice y del HEAD para luego mostrarlos si es que tienen diferencias,
incluyendo las diferencias de las rutas de trabajo que no sean detectadas por Git.
[Fuente](https://git-scm.com/docs/git-status)

14. *log*: Muestra los commit.
[Fuente](https://git-scm.com/docs/git-log)

15. *checkout*: Se encarga de actualizar el �rbol de trabajo con el �ndice, para as� lograr que coincida con el
especificado. Ejemplo: si estamos en la rama �master�, usamos el comando "git checkout taller0" para movernos a
esa rama en espec�fico.
[Fuente](https://git-scm.com/docs/git-checkout)

16. Rama / Branch: Una rama, es una l�nea de desarrollo paralela a la principal. Es un apuntador a la rama local,
y en el caso de aplicar el comando git branch, este solo crea una nueva rama.
[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

17. Etiqueta / Tag: Sirve para etiquetar puntos en el historial.
[Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)
	
