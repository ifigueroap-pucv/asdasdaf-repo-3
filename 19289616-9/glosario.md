# Glosario

1. *Control de versiones (VC)*: Sistema para mantener un registro de los cambios de ciertos archivos, permitiendo volver al archivo antes de los cambios o viseversa. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "Git")

2. *Control de versiones distribuido (DVC)*: Sistema para mantener un registro de los cambios de ciertos archivos dentro de una plataforma en linea, permitiendo tener respaldo de estos y trabajar de manera grupal. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "Git") 

3. *Repositorio remoto y local*: Son espacios donde se almacena, mantiene y organiza informacion digital, donde el repositorio remoto local esta ubicado en el ordenador del usuario y el remoto en una plataforma en linea. [Fuente](https://es.wikipedia.org/wiki/Repositorio "Wikipedia")

4. *Copia de trabajo / _Working Copy_*: Como su nombre lo indica, es la copia de lo que uno esta trabajando en la que se remplazara cada cambio. [Fuente](https://www.osmosislatina.com/cvs_info/wcopy.htm)

5. *Area de preparacion / _Staging Area_*: Dentro de control de versiones, es un area por el cual pasan los archivos modificados para determinar si estos estan listos para remplazar a los originales. [Fuente](https://es.wikipedia.org/wiki/%C3%81rea_de_stage_(datos) "Wikipedia")

6. *Preparar cambios / _Stage Changes_*: Dentro de control de versiones, se le llama preparar cambios cuando se indica que cierto archivo tendra alguna modificacion que puede o no ser definitiva.  [Fuente](https://githowto.com/staging_changes "GitHowTo")

7. *Confirmar cambios / _Commit changes_*: Dentro del control de versiones se le llama confirmar cambios a indicar que cierto archivo, el cual fue modificado y llevado al area de preparacion, esta listo para remplazar al original. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio "Git") 

8. *Commit*: Es el comando utilizado en control de versiones para confirmar los cambios que se han hecho a un archivo y empezar a trabajar en base a este. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio "git")

9. *Clone*: Es el comando utilizado en control de versiones para clonar un repositorio incluyendo los archivos dentro de este. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git "Git")

10. *Pull*: Es el comando utilizado en control de versiones para recibir datos desde un repositorio remoto. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos "Git")

11. *Push*: Es el comando utilizado en control de versiones para enviar datos a un repositorio remoto. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos "Git")

12. *Fetch*: Es el comando utilizado en control de versiones para recibir todos los datos que no tengas de un repositorio remoto. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos "Git")

13. *Merge*: Es el comando utilizado en control de versiones para fusionar dos ramas de un repositorio y resolver posibles conflictos [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar "Git")

14. *Status*: Es el comando utilizado en control de versiones para verificar el estado del repositorio, es decir, ver ver cuales son los archivos modificados, los que no se han agregado y los que han sido confirmados. [Fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)

15. *Log*: Es el comando utilizado en control de versiones para ver el registro del proyecto, filtrar y buscar cambios especificos pero solo de los archivos confirmados. [Fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)

16. *Checkout*: Es el comando utilizado en control de versiones para cambiar a una rama distinta dentro del repositorio actual. [Fuente](https://www.youtube.com/watch?v=QGKTdL7GG24 "Youtube")

17. *Rama / _Branch_*: Es un termino utilizado en control de versiones para referirse a lineas de desarrollo, lo que permite que se pueda modificar en paralelo distintas partes de un mismo archivo sin interferir en el principal o en el trabajo del otro. [Fuente](https://es.wikipedia.org/wiki/Control_de_versiones#Ramas_puntuales "Wikipedia")

18. *Etiqueta / _Tag_*: Es un termino utilizado en control de versiones que, como su nombre lo indica, son etiquetas que el usuario le da a cierto archivo para organizar o facilitar la busqueda de estos. [Fuente](https://es.wikipedia.org/wiki/Control_de_versiones#Ramas_puntuales "Wikipedia")