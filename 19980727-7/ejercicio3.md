#Ejercicio 3

# 1.Cantidad y nombre de los branches
Cantidad de branches = 1 
Nombre de branch = master

# 2.Cantidad y nombre de las etiquetas
Cantidad de etiquetas = 0
nombre de tag = null

3. **Nombre, mensaje y c�digo hash de los �ltimos 3 commits**: Utilizando el comando git log, los resultados son:

~~~
commit 272cb1bf38ed57b446a7d3bf0307207555214cea (HEAD -> 19980727-7) *codigo*
Author: catalinaO <scottpapeleria@gmail.com> *nombre*
Date:   Sat Sep 1 02:31:46 2018 -0300
 *mensaje*
    Agregue el Glosario
    No esta completo, falta escribir definiciones
~~~

~~~
commit b1fd44c2c8cc8cc373d3067fc92f6bafa8a004d2 (origin/master, origin/HEAD, origin/19980727-7, master) *codigo*
Author: Constanza Zamora <conynicoleza@icloud.com> *nombre*
Date:   Wed Aug 29 12:29:57 2018 -0300
 *mensaje*
    no message
~~~

~~~
commit 43236994ef1ce8cc78c2dc48410738ac6847d0c3 *codigo*
Merge: eb03806 200f841
Author: Connie Zamora <conynicoleza@gmail.com> *nombre*
Date:   Wed Aug 29 13:16:16 2018 +0000
 *mensaje*
    Merged in Connie-za/198989568-creado-va-web-con-bitbucket-1535512084331 (pull request #2)
    otra vez metiendo mano jojojjo
    19898956-8 creado v�a web con Bitbucket
:
~~~
