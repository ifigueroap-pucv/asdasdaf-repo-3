#Glosario

1. Control de versiones:El control de versiones es un sistema que registra los cambios realizados a un archivo a lo largo del tiempo, por lo que se puede recuperar una versión anterior revirtiendo los cambios que fueron realizados.
Existen distintas formas de control de versiones y dependiendo de la manera en se trabajará puede ser locales, centralizados y distribuidos. 
https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

2. Control de Versiones distribuido: Los clientes no sólo descargan la última instantánea de los archivos si no que,  
replican completamente el repositorio. Además esto permite un gran rendimiento en grandes proyectos, ya que las busquedas son más eficaces y rapidas.
https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

3. Repositorio remoto y Repositorio local: El repositorio local es el que uno tiene en su ordenador
y el repositorio remoto puede estar en GitHub, GitLab, entre otros.
https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/ 

4. Working copy: Es una copia local donde se trabaja, 
la cual puede ser trabajada sin internet para posteriormente subirla a un repositorio remoto.  
https://magmax.org/blog/usando-git/

5. Staging Area: Es un archivo que generalmente se encuentra en el directorio de Git, este almacena información acerca del proximo trabajo a realizar,
generalmente se conoce como "index".
https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git

6. Stage Changes: Se refiere a preparar y organizar los cambios que se realizarán en los archivos del repositorio.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio  

7. Commit Changes: Para preparar los cambios se utiliza el comando add, esto agrega los cambios realizados al repositorio local.
https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio

8. Commit: Sirve para confirmar y comentar los cambios realizados.
https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio

9. Clone: Copia un repositorio existente donde todos sus archivos estarán bajo seguimiento y sin modificaciones.  
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

10. Pull: Recibes datos del repositorio.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

11. Push: Comando que manda los cambios que realizaste al repositorio remoto.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos 

12. Fetch: Permite recuperar todos los datos del proyecto remoto que no tengas todavía. 
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos 

13. Merge:Comando que sirve para incorporar los cambios a la rama master y los fusiona.
https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-básicos-para-ramificar-y-fusionar 

14. Status: Comando que sirve para ver el estado de los archivos, te avisa cuando algo no ha sido agregado, o si no hay cambios que hacer, entre otros.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

15. Log: Comando que muestra el historial de los cambios realizados al repositorio.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones

16. Check out: Comando que permite cambiarse de rama para poder trabajar en ella. 
https://git-scm.com/docs/git-checkout

17. Rama: Áreas de un proyecto/repositorio, que no necesariamente tienen relación entre si.
Sirve para trabajar en distintos sectores simultaneamente sin afectar a la rama principal (master)
https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F

18. Tag: Se utiliza para nombrar las versiones y/o hitos que son importantes durante el transcurso del proyecto.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas