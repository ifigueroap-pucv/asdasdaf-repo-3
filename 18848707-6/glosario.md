#Glosario

1. __control de versiones (VC):__
    + sistema para registrar los cambios realizados sobre uno o varios archivos en una linea de tiempo que permite recuperar cambios en un futuro.

[fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "control de versiones")

2. __control de versiones distribuido (DVC):__
    + los clientes trabajan con clones de los repositorios y no con copias trabajando como servidoes y clientes al mismo tiempo.

[fuente](https://www.adictosaltrabajo.com/tutoriales/mercurial/ "control de versiones distribuido")

3. __repositorio remoto y repositorio local:__
    + Los repositorios remotos son versiones del proyecto que permite colaboracion para mandar y recivir datos cuando se nesecite compartir
    + Los repositorios locales son los archivos almacenados en el equipo de trabajo y que poseen 2 estados, __Inicial__ o __Intermedio__.

[fuente1](https://www.adictosaltrabajo.com/tutoriales/mercurial/ "repositorio remoto")
[fuente2](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/  "repositorio local")

4. __Copia de trabajo/Working Copy:__
    + Es una copia del repositorio cuando el proyecto alcanza un estado para conservar.

[fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio "Working copy")

5. __Area de preparacion / Staging Changes:__
    + es un area temporal donde estan los cambios que seran guardados en el repositorio con el siguiente _commit_.

[fuente](https://styde.net/flujos-y-zonas-por-las-que-pasa-un-fichero-en-un-repositorio-git/ "area de preparacion")

6. __preparar cambios / Stage Changes:__
    + cambiar el estado del fichero pasandolo alarea de preparacion

[fuente](https://styde.net/flujos-y-zonas-por-las-que-pasa-un-fichero-en-un-repositorio-git/ "preparar cambios")

7. __Confirmar cambios / Commit Changes:__
    + es l confirmacion de los cambios realizando una copia y un registro al repositorio

[fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio "confirmar cambios")

8. __Commit:__
    + comando git para confirmar cambios y pasar a la cabecera.

[fuente][comandosBasicosGIT]

9. __Clone:__
    + comando git para clonar un repositorio.

[fuente][comandosBasicosGIT]

10. __pull:__
    + comando git para fucionar los cambios realizados en el repositorio local.

[fuente][comandosBasicosGIT]

11. __push:__
    + comando git para enviar los cambios a la rama principal de los repositorios remotos.

[fuente][comandosBasicosGIT]

12. __fetch:__
    + comando git que permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local en el que est� trabajando.

[fuente][comandosBasicosGIT]

13. __merge:__
    + comando git para fucionar una rama con otra rama activa.

[fuente][comandosBasicosGIT]

14. __status:__
    + comando git que muestra una lista con el estado de los archivos cambiados o comprometidos.

[fuente][comandosBasicosGIT]

15. __log:__
    + comando git que muestra una lista de commits en una rama junto con todos los detalles.

[fuente][comandosBasicosGIT]
[comandosBasicosGIT]: https://www.hostinger.es/tutoriales/comandos-de-git/

16. __checkout:__
    + comando git que crea ramas o cambia entre ellas.

[fuente][comandosBasicosGIT]

17. __rama / branch:__
    + forma de almacen para cada confirmacion de cambios.

[fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F "rama")

18. __Etiqueta / tag:__ 
    + marca de puntos espcificos en el historial usado tipicamente para marcar versiones.

[fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado "Etiquetas")