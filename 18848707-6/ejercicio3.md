#ejercicio 3

1. __cantidad y nombre de los branches:__
    + existe un branch con nombre "master".

2. __cantidad y nombre de las etiquetas:__
    + no existen etiquetas.

3. __nombre, mensaje y codigo de los ultimos 3 commit:__



    + commit 9299557defe029631d2ebd2f21b4bb7832bc5f17 (HEAD -> master)
      Author: Byron Ortiz <graninty@gmail.com>
      Date:   Tue Aug 28 12:19:26 2018 -0400

      creacion de los archivo glosario.md y ejercicio3

      Creo los archivos glosario.md y lo completo con su informacion y creo
      ejercicio3.md en blanco

      glosario.md se encuentra en formato markdown y el archivo ejercico3 esta
      en blanco para rellenar con su informacion posterior a esto.
      

    + commit 8cbd321eb8d09c76bf71ca01e21fb0af3682b7bf (origin/master, origin/HEAD)
      Author: Benjamin Tapia <benjamin.tapia.rom@gmail.com>
      Date:   Tue Aug 28 15:11:37 2018 +0000

      Glosarito Agregado.

    + commit a612cb2f11f6b526fc45679ef163713f4f7ef0f9
      Merge: 5cb229d c776996
      Author: Benjamin Tapia <benjamin.tapia.rom@gmail.com>
      Date:   Tue Aug 28 12:03:01 2018 -0300

      Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-3


