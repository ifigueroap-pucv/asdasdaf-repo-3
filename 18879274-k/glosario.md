a. **Control de versiones:** El control de versiones es un sistema en el cual se almacenan los cambios realizados en un archivo, esto permite que uno pueda revisar versiones anteriores o recuperar codigo (o archivos) que ya no se encuentre disponible.
b. **Control de versiones distribuido:** Es un sistema que viene a mejorar CVCSs, este permite a todos los usuarios guardad ademas de una copia del archivo, si no que ademas todo el repositorio. Por lo cual en caso de que un usuario pierda el proyecto, existiran mas usuarios que contengan copias de seguridad de esta.
c. **Repositorio remoto y local:** Es un sistema que almacena en una "base de datos simple" el registro de todos los cambios que se han producido en el codigo o archivo. Esto es util, ya que permite al usuario volver a una versión determinada cuando lo necesite.
d. **Copia de trabajo:** Es la copia que se genera al momento de crear un repositorio de un sistema, en su ultima versión de esta. En este sistema se ve almacenado toda la información del sistema existente hasta ese momento con sus respectivos archivos.
e. **Área de preparación:** Es un archivo que contiene una referencia a todos los cambios realizados en el sistema, esta está contenida en la próxima confirmación que hará el usuario al momento de guardar la versión en la cual trabaja.
f. **Preparar cambios:** Es el estado que tiene el archivo que fue ya subido al repositorio, y fue puesta en el área de preparación.
g. **Confirmar cambios:** Es la confirmación para que la versión que se esta trabajando, se suba en el directorio de git (Subida).
h. **Commit:** Este comando lo que hace es guardar la ultima versión en un repositorio local.
i. **Clone:** Este comando lo que hace es clonar toda la información del repositorio que se desea respaldar, asi en caso de que se corrompa el disco donde uno trabaja, el usuario puede trabajar con el clon del servidor desde los clientes para volver al estado previo a la corrupción del disco.
j. **Pull:** Este comando lo que permite es unir la "rama actual" con la "rama remota". Esto significa que generalmente, cuando se ejecuta esta linea de codígo se clona la información del servidor y se intenta de unir al sistema del usuario.
k. **Push:** Este comando envía el proyecto a un repositorio remoto. Este comando es usadado para compartir la información al resto de usuario (en el servidor donde el ususario tenga permisos de edición). Tiene la restricción que para funcionar previamente debe ser clonado el servidor donde el usuario tiene permisos de escritura y nadie ha enviado información previo al envío.
l. **Fetch:** Este comando permite la recuperación de todos los datos del proyecto con los cuales aun no cuenta el usuario. Esta información recuperada solo es inserta en el repositorio local (No se une automaticamente con el trabajo del usuario ni modifica el actual trabajo de este).
m. **Merge:** Este comando permite la fusión de ramas, estas se utilizan comunmente para solucionar errores previos a un punto exacto, se crean diversas ramas si existen diversos errores y se solucionan 1 a la vez, luego esta rama es fusionada con la "master" y se prosigue al siguiente error.
n. **Status:** Este comando lo que hace es mostrar todos aquellos conflictos que ocurren al fusionar 2 ramas y que ademas no se han podido resolver, son marcadas como "unmerged".
o. **Log:** Este comando es muy util ya que nos lista todas las confirmaciones hechas en el proyecto en orden del mas nuevo al mas antiguo.
p. **Checkout:** Este comando nos permite saltar de una rama a otra. 
q. **Branch:** Este comando crea una nueva rama y no salta a esta.
r. **Tag:** 	Este comando nos permite listar etiquetas, comunmente se utiliado para etiquetar en versiones tipo "1.2.1, 1.2.2, 1.2.3, 1.3.0" y esto ayuda al usuario si desea ingrear a una versión determinada.

[a, b y c.](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
[d.](https://git-scm.com/book/es-ni/v1/Git-Basics-Obteniendo-un-Repositorio-Git)
[e, f y g.](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)
[h.](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
[i.](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
[j, k y l](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
[m y n](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)
[o.](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)
[p](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)
[r.](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
