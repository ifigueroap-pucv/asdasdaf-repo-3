#Glosario



##a. Control de versiones (VC) 
##b. Control de versiones distribudo (DVC)
##c. Repesitorio remoto y Repositorio local 
##d. Copia de trabajo / Working Copy 
##e. Area de Preparacion / Staging Area 
##f. Preparar Cambios / Stage Changes
##g. Confirmar cambios / Commit Changes 
##h. Commit
##i. clone 
##j. pull 
##k. push 
##l. fetch 
##m. merge
##n. status 
##o. log
##p. checkout 
##q. Rama / Branch 
##r. Etiqueta / Tag


###a. Control de versiones (VC)

	En un proyecto en Informatica, una cosa muy importante para administrar durante el desarrollo del proyecto, es el *control de versiones*. En efecto, cualquier proyecto de computadora implica un mantón de cambios en el código fuente. De hecho, para administrar estos cambios, elimportante es de tener un histórico de los códigos, o en otras palabras, versiones de código fuente. Es en esta perspectiva que ha llegado software de control de versiones permitiendo de :

	*Almacenar el código fuente en múltiple ubicación --> Evitar la pérdida del código

	*Matener un histórico de los cambios realizados --> Pasar de una version a una otra 

	*Trabajar de manera ordenada sin necesidad de estar todos juntos --> Mejorar la efectidad del desarollo 

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)

###b. Control de versiones distribudo (DVC)*

	Después haber definido un software de control de versiones, podemos hablar de los tipos de software de control de versiones. En efecto, hay 2 tipo de software en el control de versiones :

		* *Centralizado*

		* *Distribudo*

La principal diferencia entre un sistema *distribuido*, usando *DVC*, y un sistema centralizado es que >cada programador tiene una copia completa del repositorio en su equipo y al hacer un cambio se propaga a todos los nodos>. Ademàs, son sistemas utilizando el *"peer-to-peer"*, entonces, en principio no necesita de un servidor. Pero, en realidad, hay un servidor que va a hacer la conexión, y transmitir los datos, archivos, a los otros desarolladores.

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)

###c. Repositorio remoto y Repositorio local* 

	Primero, un repositorio ilustra una copia de un proyecto. Esta copia puede encontrarse en el computador del desarrollador, hablamos entonces de un *repositorio local*. En el caso donde el repositorio se encuentra en un servidor, o Internet de manera general, podemos hablar de un *repositorio remoto*.

	*Repositorio remoto* :

Existe diferentes comandas para administrar los repositorios remoto :

	*"git remote" : Para ver los repositorios remotos que existen. Este comando va a sacar una lista de todos los repositorios remotos del proyecto.

	*"git remote add [nombre_del_proyecto][url] : Para añadir un repositorio en el servidor, y hacer de eso un repositorio remoto. Este comanda necessita el [nombre_de_proyecto] y el [url] que representa la dirección donde será el repositorio.

	*"git pull" : Este comando va a recuperar y fusionar automáticamente una rama remota en su rama local. Este comando tiene por función de recuperar los cambios hecho en una rama remota, en la rama que el desarrollador tiene en su computador.

	*Repositorio local* :

	Los comandos, para administrar un repositorio local, son los mismos que para administrar archivos, o carpetas en su computador. Entonces, esa implica que los cambios realizados en esta carpeta solamente actúan por el desarrollador, y su computador. Luego puede pasar su repositorio local en un repositorio remoto por el comando "git remote add" descrito arriba.
 
link : [Repositorio_remoto_local](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remoto)

###d. Copia de trabajo / Working Copy* : 

	Para obtener una "copia de trabajo", que es en realidad la copia de un repositorio existente, la necesidad es de tocar la comando "git clone" más el link donde se encuentra el repositorio. En efecto, esta comanda va a tomar todo el repositorio remoto y hacer una copia local en su computador.

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)


###e. Area de Preparacion / Staging Area* 

	El software GIT trabaja con tres " áreas" diferentes para administrar los versiones de archivos :

	*área de trabajo

	*área de preparación

	*área de la carpeta GIT

	El *área de preparación* es un archivo que se encuentra en la carpeta GIT, donde tiene las informaciones sobre los archivos donde haría modificaciones. Entonces, cuando hay una modificación en el área de trabajo que ilustra el área donde el desarrollador administra su código fuente, un archivo está creando en el área de preparación, esperando una validación del desarrollador. Una validación, ilustrado por el comando "git commit", implica que el nuevo archivo, o en otras palabras, la nueva versión del archivo se va de la *área de preparación* para ir en *el área de la carpeta GIT* que es una basa de datos donde hay todas las versiones validadas de diferentes archivos del proyecto.

link : [Area_de_Preparacion](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

###g. Confirmar cambios / Commit Changes* 

	Para confirmar cambios de un código, archivo, repositorio, necesita hacer el comando " git commit" más el nombre del archivo que quiere confirmar código. Como descrito arriba, este comando implica que el software va a poner esa versión en su repositorio, al fin de la basa de datos donde hay todas las versiones desde el inicio del proyecto.

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)

###h. Commit  

	Como descrito arriba, "Commit", y el comando "git commit", sirve a confirmar los cambios realizado en un archivo, o código. Un commit no va a mandar el archivo en el servidor : un simple commit es local.
	![Comando git commit](/home/assaf/Téléchargements/git_commit.PNG)

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)

###i. clone 

	El comando *"git local"* sirve para hacer una copia de un repositorio que se encuentra en un servidor, y ponerlo en local.

	![Comando git clone](/home/assaf/Téléchargements/Clone_repositorio.PNG)

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)

###j. pull

	El comando *"git pull"* sirve para descargar noticias del proyecto desde el servidor. De esta manera, podría tener los modificaciones realizados por los otros desarolladores.


link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)
 
###k. push 

	El comando *"git push"* sirve para mandar los archivos "commited" en el servidor. Lo que importante es que necesita tener los derechos de escribir en el repositorio remoto. Otra cosa, para evitar conflicto al momento de hacer el comando "push", el mejor es estar al día del proyecto remoto.

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)	

###l. fetch 
	
	El comando sirve a *descargar* los nuevos archivos "comitted" que se encuentran en el servidor. 

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)

###m. merge
	
	"git merge" sirve a fusionar los archivos descargado que viene de la rama del servidor en la rama de su computador. 

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)
	
###n. status 

	*"git status"* permite tener un progreso del proyecto : eso pasa por los archivos que han sido el objeto de un "commit", las modificaciones realizadas, y una comparación entre las ramas locales y las ramas remota.

	![Comando git status](/home/assaf/Téléchargements/git_add_git_status.PNG)
	![Comando git status](/home/assaf/Téléchargements/git_status.PNG)

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)

###o. log

	*"git log"* permite de obtener un historico de los commit realizados : 
	
	![Comando git log](/home/assaf/Téléchargements/git_log.PNG)

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)

###p. checkout 

	Un *checkout* puede ser muy util. En efecto, el comando checkout tiene dos functionalidad : 
		
		* Restaurar un archivo modificado a su ultimo commit 	 
		* Cambiar de rama 

link : [Open_classroom_GIT](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)

	
###q. Rama / Branch 

	Las ramas son una de las más importantes característica del software GIT. Ellas permiten trabajar en paralelo sobre una cosa, cuando esta cosa puede tomar tiempo, necesitando diferentes commit, y si la cosa no está segura de funcionar al fin. Entonces, en GIT, el proyecto o repositorio, se encuentra en la rama principal, llamada "master". Es posible de crear una rama, a partir de la master : eso va a hacer una copia de la rama master que va a llamarse con el nombre que quiere. Para administrar las ramas hay diferentes comandos :

		* Para crear una rama : *"git branch [nombre_de_la_rama]".*

		* Para ver las ramas creadas : *"git branch"*

		* Para pasar de una rama a otra : *" git checkout [nombre_de_la_rama]"*

		* Para fusionar dos ramas : *"git merge"*

link : [Rama_Definicion](https://git-scm.com/book/es/v2/Ramificaciones-en-Git-¿Qué-es-una-rama%3F)

###r. Etiqueta / Tag

	GIT permite poner etiqueta en el archivo que administra el histórico de commit. Esta etiqueta puede servir a encontrarse rápidamente una versión, o a indicar la importancia de una versión. Hay 2 tipos de etiquetas : las etiquetas *anotados*, y las etiquetas *ligueras*.

En primer lugar, las etiquetas *anotados* contiene *informaciones adicionales* para indicar, justificar la etiqueta. De esta manera, un desarrollador puede ver quien ha creado esta etiqueta, y por qué. Este tipo de etiqueta caga bien para hacer una versión mayor durante el proyecto.

*Para crear una etiqueta anotada : "git tag -a [nombre_de_la_etiqueta]"

Por otra parte, existen las etiquetas *ligueras*. Este tipo de etiqueta no tiene *informaciones adicionales*, entonces sirve principalmente para encontrarse una versión rápidamente.

*Para crear una etiqueta liguera : "git tag [nombre_de_la_etiqueta]"

En todos casos, para ver las etiquetas creadas : *"git tag"*

link : [Etiquetado](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)


	
