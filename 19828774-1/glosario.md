# Glosario

Definición de términos técnicos asociados a git.

1. [Control de versiones (VC) ](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) - Sistema que se encarga de la gestión y registro de cambios realizados sobre un archivo, para así poder recuperar distintas versiones de este.

2. [Control de versiones distribuido (DVC) ](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) - lo mismo que la definición anterior, solo que al ser distribuido no es necesario un servidor central, todos los participantes tienen una copia del repositorio y cuando este se modifica, se propaga a todos los nodos.
3. [Repositorio remoto](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) - Versión de un proyecto que lo encontramos alojado en la red. Estos repositorios implican gestionarlos, donde podemos mandar y recibir datos de otras personas que trabajan en conjunto.
 [Repositorio local](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) - Versión de un proyecto, pero este lo tenemos en nuestro PC, por lo que solo lo ves tú y nadie puede tener acceso a él, a menos que nosotros lo permitamos.
4. [Copia de trabajo / Working copy](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git) - Es una copia de un proyecto, la cual podemos clonar de algún repositorio remoto. A esta copia podremos hacerle modificaciones, sin afectar al proyecto original.
5. [Área de preparación / Staging area](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git) - Archivo contenido en el directorio git, éste almacena información de tu próxima confirmación.
6. [Preparar cambios / Stage changes](https://githowto.com/staging_changes) - Etapa en la cual git sabe sobre los cambios que se realizarán, pero estos no son permanentes, el siguiente "commit" los incluirá. Si no se realizan, se puede usar el comando git reset para dejar de grabar estos cambios.
7. [Confirmar cambios / Commit changes](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository) - Proceso donde se guardan los cambios y se envían al repositorio.
8. [Commit](https://platzi.com/git-github/tutoriales/comandos-git-del-dia-a-dia/) - Comando que confirma los cambios realizados y los registra en el repositorio.
9. [Clone](https://platzi.com/git-github/tutoriales/comandos-git-del-dia-a-dia/) - Comando para clonar un repositorio existente.
10. [Pull](https://platzi.com/git-github/tutoriales/comandos-git-del-dia-a-dia/) - Comando para bajar los cambios del repositorio remoto.
11. [Push](https://platzi.com/git-github/tutoriales/comandos-git-del-dia-a-dia/) - Comando para subir los cambios que nosotros hemos hecho.
12. [Fetch](https://www.atlassian.com/git/tutorials/syncing/git-fetch) - Comando que descarga commits, archivos, referencias, etc. Desde un repositorio remoto, al nuestro.
13. [Merge](https://platzi.com/git-github/tutoriales/comandos-git-del-dia-a-dia/) - Comando para unir dos ramas.
14. [Status](https://platzi.com/git-github/tutoriales/comandos-git-del-dia-a-dia/) - Comando para ver el estado de los archivos.
15. [Log](https://platzi.com/git-github/tutoriales/comandos-git-del-dia-a-dia/) - Comando para ver el historial de los commits.
16. [Checkout](https://platzi.com/git-github/tutoriales/comandos-git-del-dia-a-dia/) - Comando para deshacer los cambios de un archivp que esta en el working directory.
17. [Rama / Branch](https://www.arsys.es/blog/programacion/ramas-git/) - Es una bifurcación del estado del código, sirven para trabajar en una rama distinta a la principal, para que ésta no se vea afectada al realizar cambios. Permiten un mejor control del código.
18. [Etiqueta / Tag](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado) - Se utilizan para nombrar (marcar) puntos específicos de un proyecto, por ej. versiones de lanzamiento del proyecto.
