##### Cantidad y nombre de los branches.
- Cantidad: 1.
- Nombre: master.

##### Cantidad y nombre de las etiquetas.
- Cantidad: 0.
- Nombre: no hay eitquetas.

##### Nombre, mensaje y código hash de los últimos 3 commits.
- ###### Primer Commit.

    Nombre: Matías Pinto.
    Mensaje: Agregando glosario.
    Código: 4d18f4b881786510474c923ec437a6c4e22c9f76.
- ###### Segundo Commit.
    Nombre: assaf_ibrahim.
    Mensaje: Merge branch master.
    Código: 19d307137a46e85a47ffd1d7148f2add9a5d2b43.
- ###### Tercer Commit.
    Nombre: assaf_ibrahim.
    Mensaje: Última modificación.
    Código:  875181264d8c7e0eda84c75411f0dd2611305fcf.