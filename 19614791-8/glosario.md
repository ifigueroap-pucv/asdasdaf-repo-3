Control de Versiones (VC):
	Sistema que permite acceder a distintas versiones de un archivo o un conjunto de archivos, 
	registrando los cambios realizados en el/los archivos durante el tiempo.

	Fuente:	https://git-scm.com/book/en/v1/Getting-Started-About-Version-Control

Control de Versiones Distribuido (DVC):
	Un tipo de VC, el registro de cambios (o versiones), se guardan en un server aparte y 
	tambien el los clientes.

	Fuente: https://git-scm.com/book/en/v1/Getting-Started-About-Version-Control

Repositorio Remoto:
	Versiones de un proyecto que estan almacenados fuera del cliente en uso, 
	puede ser internet o cualquier otra red.
	
	Fuente:	https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes

Repositorio Local:
	Versiones de un proyecto que estan almacenados dentro del cliente en uso.

	Fuente: https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/
	
Copia de Trabajo:
	Copia de una version del archivo con el que se esta trabajando en el repositorio.

	Fuente:	https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

Area de Preparacion:
	Es el Index, un archivo que guarda informacion sobre lo que ira en el siguiente commit, 
	para ser guardado el el repositorio.

	Fuente:	https://git-scm.com/book/en/v2/Getting-Started-Git-Basics

Confirmar cambios:
	Guardar los cambios a un archivo en el repositorio local, creando versiones del archivo.

	Fuente:	https://www.git-tower.com/learn/git/commands/git-commit
	
Commit:
	Version de un archivo en un repositorio.

	Fuente:	https://www.git-tower.com/learn/git/commands/git-commit

Clone:
	Clonar un repositorio en un nuevo directorio.

	Fuente: https://git-scm.com/docs/git-clone

Pull:
	Incopora cambios desde un repositorio remoto a la rama actual.
	
	Fuente:	https://git-scm.com/docs/git-pull

Push:
	Actualiza referencias remotas jutno con sus objetos asociados usando referencias locales.

	Fuente:	https://git-scm.com/docs/git-push

Fetch:
	Descarga obejtos y referencias de un o mas repositorios.

	Fuente:	https://git-scm.com/docs/git-fetch

Merge:
	Une 2 o mas ramas.

	Fuente:	https://git-scm.com/docs/git-merge

Status:
	Mostrar la version actual de una entidad ((Ramas,archivos y commit).

	Fuente:	https://git-scm.com/docs/git-status

Log:
	Muestra todos los cambios realizados (commits).

	Fuente:	https://git-scm.com/docs/git-log

Checkout:
	Apuntar a diferentes versiones de una entidad (Ramas,archivos y commit).

	Fuente:	https://www.atlassian.com/git/tutorials/using-branches/git-checkout

Rama:
	Un hilo de versiones en una linea de desarrollo de un archivo.

	Fuente:	https://git-scm.com/book/en/v1/Git-Branching-What-a-Branch-Is

Etiqueta:
	Nombre simbolico a una version especifica de una rama.

	Fuente:	https://stackoverflow.com/questions/1457103/how-is-a-tag-different-from-a-branch-in-git-which-should-i-use-here