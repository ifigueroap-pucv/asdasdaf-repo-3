# Glosario
**1. Control de versiones ***(VC)***:** El control de versiones es un sistema que registra cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que ouedas recuperar versiones específicas mas adelante. 
    <https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones>

**2. Control de versiones distribuido ***(DVC)***:** Los sistemas de control de versiones distribuidos, los usuarios no solo pueden descargar la última intancia de los archivos.   Así, si un servidor muere, y estos sistemas estaban colaborando a traves de él, cualquiera de los repositorios de los clientes puede copiarse en el servidor para restaurarlo.
    <https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones>

**3. Repositorio remoto y Repositorio local:** Un repositorio local es donde se almacena todos los archivos de un proyecto, el cual se guarda en tu ordenador, de la misma forma el repositorio remoto almacena estos datos pero en la nube, la cual se podra tener acceso desde cualquier lugar, en cambio el local, solo tiene acceso que creador de este. 	
    <https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones>

**4. Copia de trabajo / ***Working Copy***:** Se le denomina "copia de trabajo" al lugar donde se posee una copia de un repositorio, en la cual va a ser trajado o modificado.
    <https://www.youtube.com/watch?v=QGKTdL7GG24>
	
**5. Área de Preparación / Staging Area:** es un area temporal en el que agregamos archivos cuyos cambios estamos apunto de ser enviados a git en forma de commit.
    <https://www.youtube.com/watch?v=QGKTdL7GG24>
    
**6. Preparar Cambios / ***Stage Changes***:** Es donde se almacenan los archivos que estan sujetos a cambios (que estas agregando o modificando).
	<https://www.youtube.com/watch?v=QGKTdL7GG24>
	
**6. Confirmar cambios / ***Commit Changes***:** Es cuando guardas los archivos que estan almacenados en el staging area, para que que queden de manera permanente en el directorio en estas trabajando.	
	<https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf>
	
**7.Commit:** Un "commit" o hacer uno, quiere decir que, estas confirmando el agregar un archivo, el cual sera almacenado en el repositorio local de forma permanente.
    <https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf>

**8. clone:** clone se utiliza para realizar una copia de un repositorio ya sea hacia un repositorio local o remoto.
    <https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf>

**9. pull:** Es ultilizado para cargar y unir al mismo tiempo, una rama remota con la rama local actual
    <https://informatica.ucm.es/data/cont/media/www/pag-66886/TallerGitGitHub.pdf>

**10. push:** Es utilizado para enviar los archivos en el repositorio local al remoto.
    <https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos>

**11. fetch:** Es ultilizado para cargar, una rama remota a tu repositorio local.
<https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos>

**12. merge:** Se utiliza para argregar cambios a una rama, uniendo o fusionando estas en el proceso.
    <https://diego.com.es/ramas-y-uniones-en-git>
    
**13. status:** Ayuda a determinar a saber en qué estados se encuentran archivos o directorios que se están utilizando.
    <https://www.siteground.es/kb/tutorial-git-comandos/>    

**14. log:** Es un comando que muestra el registro historico de los commit realizados.
    <https://www.siteground.es/kb/tutorial-git-comandos/>

**15. checkout:** Permite crear una rama nueva o cambiar entre las ramas existentes.
    <https://www.siteground.es/kb/tutorial-git-comandos/>

**16. Rama / ***Branch***:** las ramas nos permiten trabajar de forma paralela acorde se van realizando cambios en el proyecto (de forma independiente una de la otra)	
    <https://diego.com.es/ramas-y-uniones-en-git>

**17. Etiqueta / ***Tag***:** Es el nombre o apodo que se hace referencia al identificador de un commit. Suele	usarse para poseer un control mas estricto de las versiones, agregado de esta forma nombres como v1.0;v1.1; etc.
    <https://filisantillan.com/creando-etiquetas-con-git/>
