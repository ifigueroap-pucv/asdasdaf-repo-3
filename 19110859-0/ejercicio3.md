# Datos del Repositorio

## Ramas (*Branches*)

* Cantidad: 1
* Nombre(s): "master"

## Etiquetas (*Tags*)

* Cantidad: 0
* Nombres(s): no existen

## Últimas 3 Confirmaciones (*Commits*)

* Código Hash: 3422c2b28ced6590059f009665fd2e0d5a0bfd05
* Autor: Eric Felipe Riveros Novoa
* Mensaje:

    **Agrega archivo glosario. md**

        Se almacena archivo que contiene un glosario de definiciones 
        relacionados a los conceptos basicos de git y sus comandos en la 
        carpeta "19110859-0".

***

* Código Hash: 2733aa8a0304eac5b0e26c94ead9db118a217b78
* Autor: xubylele
* Mensaje:

    **Perdon pero otra corrección**

***

* Código Hash: e60c61e1ab8e7173536c050639e9741a9d4f5733
* Autor: xubylele
* Mensaje:

    **Archivo agregado Glosario.md en carpeta 19774094-9**
