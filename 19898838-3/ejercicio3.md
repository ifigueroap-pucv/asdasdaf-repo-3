# Datos
**Cantidad y nombre de los branches**

N° de Branches: 1

Nombre: "master"


**Cantidad y nombre de las etiquetas**

N° de Etiquetas: 0

Nombre: NULL

**Últimos 3 commit**

||commit 1|
|--|--|
| **commit** | f56b2d78bbab4d110369df9731dd7b21bf1a4aaa |
| **Autor** | Javier Fernández Barrientos |
| **Mensaje** | Merge branch 'master' of [https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-3](https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-3) |


||commit 2|
|--|--|
| **commit** | 647bd27d6daab3054c945b5aa2fa5f99168e3085 |
| **Autor** | Javier Fernández Barrientos |
| **Mensaje** | Se agrega el archivo datos.md Se agrega un archivo con los datos de persona. |


||commit 3|
|--|--|
| **commit** | 13f2fef4c6365f361da6b158fb2a06875fda4acc|
| **Autor** | Javier Fernández Barrientos |
| **Mensaje** | Se Ha agregado el archivo ejercicio3.md Se agrego el archivo con toda la informacion de el repositorio en el momento. |
