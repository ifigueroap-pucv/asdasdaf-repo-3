# Glosario
**1. Control de versiones (VC):** es un sistema que administra un conjunto de ficheros manteniendo un historial de cambios realizados, con el objetivo de poder recuperar versiones específicas a futuro. [*FUENTE*](https://www.ecured.cu/Sistemas_de_control_de_versiones)

**2. Control de versiones distribuido (DVC):** es un sistema en el cual todos están conectados al servidor, permite que cada desarrollador mantenga una copia completa de todo el repositorio. [*FUENTE*](https://altenwald.org/2009/01/12/sistemas-de-control-de-versiones-centralizados-o-distribuidos/)

**3. Repositorio remoto y Repositorio local:** un repositorio remoto son versiones alojadas en internet gracias a alguna plataforma, mientras que el repositorio local son versiones del repositorio alojada en tu ordenador, [*FUENTE*](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

**4. Copia de trabajo / Working Copy:** corresponde a una copia local de algún repositorio realizado con el comando *git clone*. [*FUENTE*](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

**5. Área de Preparación / Staging Area:** es el área donde se almacenan los cambios de forma local para posteriormente ser añadidos en la próxima confirmación. [*FUENTE*](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

**6. Preparar Cambios / Stage Changes:** significa que se ha marcado el archivo modificado en su versión actual para que vaya en tu próxima confirmación. [*FUENTE*](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)

**7. Confirmar cambios / Commit Changes:** significa que los datos ya están almacenados de manera segura en tu base de datos local. [*FUENTE*](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)

**8. commit:** confirma los archivos que se hayan agregado con git add y también confirma los archivos que se hayan cambiado. [*FUENTE*](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

**9. clone:** crea una copia de un repositorio ya existente. [*FUENTE*](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

**10. pull:** busca y fusiona los cambios en el servidor remoto a su directorio de trabajo. [*FUENTE*](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

**11. push:** envía los cambios de la copia local al repositorio remoto. [*FUENTE*](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

**12. fetch:** baja los cambios del repositorio remoto a la rama origin/master del repositorio local. [*FUENTE*](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

**13. merge:** fusiona una rama específica con la rama activa. [*FUENTE*](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

**14. status:** comprueba el estado de los archivos, los que han cambiado y los que aún necesitan ser agregados o confirmados. [*FUENTE*](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

**15. log:** obtiene el commit id. [*FUENTE*](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

**16. checkout:** actualiza los archivos en el árbol de trabajo para que coincida con la versión de la ruta especificada. [*FUENTE*](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

**17. Rama / Branch:** es un puntero que apunta a cada uno de los diversos commit en un repositorio. [*FUENTE*](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama?)

**18. Etiqueta / Tag:** sirve como una rama firmada que se mantiene inalterable. [*FUENTE*](https://www.genbeta.com/desarrollo/tags-con-git)