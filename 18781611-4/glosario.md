# Definiciones Basicas  

1. Control de versiones: El control de versiones es un sistema que registra los cambios realizados  
sobre un archivo o conjunto de archivos a lo largo del tiempo,  
de modo que puedas recuperar versiones espec�ficas m�s adelante.  
[Tutoriales Bitbucket](https://www.atlassian.com/git/tutorials/what-is-version-control)  

2. Control de Versiones distribuido: Los clientes no s�lo descargan la �ltima instant�nea de los archivos si no que,  
replican completamente el repositorio.  
[Acerca del control de versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)  

3. Repositorio remoto y Repositorio local: Repositorio local es el que tienes en tu ordenador,  
y un repositorio remoto, que puede estar en GitHub, GitLab o donde sea.  
[Repositorios](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)  

4. Working copy: Es una copia local de un repositorio,  
la cual puede ser trabajada sin internet para posteriormente subirla a un repositorio remoto.  
[Terminos basicos git](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)  

5. Staging Area: Es un archivo generalmente contenido en el directorio del Git,  
que almacena informacion acerca del proximo trabajo a realizar,  
generalmente se le conoce como "index".  
[Foro de preguntas](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)  

6. Stage Changes: Se refiere a mantener un seguimiento en los archivos que vayas agregando,  
un archivo no preparado no sera guardado al confirmarlo.  
[Foro de preguntas](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)  

7. Commit Changes: Guardar los cambios preradados en la ultima version.  
[Fundamentos de git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)  

8. Commit: Ejecuta tu editor de texto con un mensaje de confirmacion predeterminado.  
[Fundamentos de git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)  

9. Clone: Copia un repositorio existente todos tus archivos estar�n bajo seguimiento y sin modificaciones.  
[Comando clone](https://git-scm.com/docs/git-clone)  

10. Pull: Recibes datos de un repositorio remoto.  
[Comando pull](https://git-scm.com/docs/git-pull)  

11. Push: Mandas datos a un repositorio remoto.  
[Comando push](https://git-scm.com/docs/git-push)  

12. Fetch: Permite recuperar todos los datos del proyecto remoto que no tengas todav�a,  
no la une autom�ticamente con tu trabajo ni modifica aquello en lo que est�s trabajando.  
[Comando fetch](https://git-scm.com/docs/git-fetch)  

13. Merge: fusionar rama de trabajo a otra.  
[Ramificaciones en git](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)  

14. Status: Permite ver el estado del directorio de trabajo, cuales cambios estan preparados,  
cuales no, y cuales archivos no estan siendo seguidos.  
[Comando status](https://www.atlassian.com/git/tutorials/inspecting-a-repository)  

15. Log: Muestra el registro de confirmaciones.  
[Comando log](https://git-scm.com/docs/git-log)  

16. Check out: actualiza los archivos en el arbol de trabajo para que coincida con el index de la verison  
o el arbol especificado, si no se especifica nada se actualizara el *HEAD* y se establecera la rama especificada como la rama actual.  
[Comando check out](https://git-scm.com/docs/git-checkout)  

17. Rama: Apuntador de confirmaciones, es una forma de conseguir nuevos directorios de trabajo, osea es para trabajar  
de forma independiente algo para posteriormente unirlo.    
[Uso de branch](https://www.atlassian.com/git/tutorials/using-branches)  

18. Tag: es el metodo tradicional para identificar versiones liberadas del software.  
[Uso del tag](https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag)  