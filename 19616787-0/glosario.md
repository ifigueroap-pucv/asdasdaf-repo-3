# Glosario GIT

##### Alumno: Matias Rodriguez

## Conceptos

1. Control de versiones(VC)
2. Control de versiones distribuido(DVC)
3. Repositorio local
4. Repositorio remoto
5. Copia de trabajo
6. Área de preparación
7. Preparar Cambios
8. Confirmar Cambios
9. Commit
10. Rama
11. Etiqueta

## Definición de conceptos

### 1. Control de versiones

Es un sistema que permite registrar cambios realizados en un archivo o un conjunto de estos a lo largo del tiempo, por lo que este sistema permitirá regresar a versiones anteriores de los archivos, comparar cambios, observar quién modificó el archivo, entre otras funcionalidades. En otras palabras significa que si arruinas el archivo o lo pierdes puedas tener la opción de recuperarlo.

#### Referencias:
  1. [Documentación de Git] *1.1 Inicio - Sobre el Control de Versiones - Acerca del Control de Versiones*, [https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)

### 2. Control de versiones distribuido(DVC)

Este sistema de control es un poco más completo que las versiones locales y centralizadas ya que los clientes no solo pueden descargar la última copia instantánea de los archivos sino que tambien es capaz de replicar el repositorio por completo, estos sistemas además permite que diferentes grupos de persona trabajen de forma simultánea, lo que no es posible en los sistemas centralizados.

#### Referencias:
  1. [Documentación de Git] *1.1 Inicio - Sobre el Control de Versiones - Acerca del Control de Versiones*, [https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)

### 3. Repositorio local

Es la versión del proyecto que como su nombre lo dice se almacena de forma local en el ordenador lo que te permitirá trabajar en los repositorios sin un enlace a internet, si vienen también se le puede realizar push y pull cuando haya enlace a internet. 

#### Referencias:
1. [Colaborativo] *Git y GitHub. Trabajando con repositorios locales*, [https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

### 4. Repositorio remoto

Son versiones del proyecto que se encuentran almacenados en internet, los cuales tienen diferentes tipos de permisos como soló lectura o lectura/escritura según los permisos que posea el usuario, esto permite tener una colaboración con los otros miembros del proyecto, a su vez se deberán realizar push para mandar datos y pula para poder recibir datos.

#### Referencias:
1. [Documentación de Git] *5 Fundamentos de Git - Trabajando con repositorios remotos*, [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

### 5. Copia de trabajo 

Cuando se desea contribuir en un repositorio se hace una copia de este a través del comando **clone**, esté generará una copia  de casi todos los datos que se encuentran en almacenados en el servidor.

#### Referencias:
1. [Documentación de Git] *1 Fundamentos de Git - Obteniendo un repositorio Git*, [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

### 6. Área de preparación 

El área de preparación es un archivo sencillo que contiene la información acerca de lo que irá en la próxima confirmación a realizarse, generalmente esté esta contenido en el directorio de Git.

#### Referencias:
1. [Documentación de Git] *3 Empezando - Fundamentos de Git
*, [https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

### 7. Preparar Cambios 

Cuando se detectan cambios en el repositorio, es el usuario el encargado de confirmar esos cambios para que posteriormente estos sean publicados, para ver si los archivos están preparados para ser confirmados se puede realizar un git status.

#### Referencias:
1. [Documentación de Git] *2 Fundamentos de Git - Guardando cambios en el repositorio*, [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

### 8. Confirmar Cambios 

Una vez que los cambios se encuentren preparados esto se puede comprobar con el git status, el usuario deberá confirmar los cambios realizados estos se grabarán con el comando **commit** y para ser publicados se debe usar el comando **push**.

#### Referencias:
 1. [Documentación de Git] *2 Fundamentos de Git - Guardando cambios en el repositorio*, [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

### 9. Commit 

El commit es el comando para grabar los cambios realizados dentro del repositorio, es como la confirmación antes de hacer el push si es que se estuviera trabajando con un repositorio remoto.

#### Referencias:
1. [Youtube] *Creando un commit*, [https://www.youtube.com/watch?v=UJGKWMHX038](https://www.youtube.com/watch?v=UJGKWMHX038)

### 10. Rama 

Para entender lo que son las ramas primero se debe entender como almacena Git  ya que no almacena de forma incremental sino que como una serie de instancias, dicho esto la ramas no son más que apuntadores móviles que señalan a las confirmaciones(commit), dentro de Git la rama por defecto es la master la cual se crea con la primera confirmación de cambios, por cada modificación que se realice las ramas irán avanzando automáticamente y la master siempre apuntará hacia la ultima modificación.

#### Referencias:
1. [Documentación de Git] *1 Ramificaciones en Git - ¿Qué es una rama?*, [https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

### 11. Etiqueta

Son utilizadas para marcar puntos específicos del repositorio, generalmente son utilizadas cuando se finaliza alguna parte del proyecto o se lanza alguna versión de este.

#### Referencias:
1. [Documentación de Git] *6 Fundamentos de Git - Creando etiquetas*, [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)

------

## Comandos

1. Pull
2. Push
3. Fetch
4. Merge
5. Status
6. Log
7. Checkout
8. Clone
9. Commit


## Definición de comandos

### 1. Pull

Se utiliza para bajar los cambios del repositorio remoto al repositorio local.

#### Sintaxis:
 `$ git pull origin master`

#### Referencias:
 1. [Git Documentation] *Git pull*, [https://git-scm.com/docs/git-pull](https://git-scm.com/docs/git-pull)

### 2. Push

Se utiliza cuando se desea subir los cambios al repositorio remoto.

#### Sintaxis: 
 `$ git push origin master`
 
#### Referencias:
 1. [Git Documentation] *Git push*, [https://git-scm.com/docs/git-push](https://git-scm.com/docs/git-push)
 
### 3. Fetch

Se utiliza para buscar todos los objetos de un repositorio remoto que actualmente no estén en el repositorio local.

#### Sintaxis:
 `$ git fetch origin`
 
#### Referencias:
 1. [Git Documentation] *Git fetch*, [https://git-scm.com/docs/git-fetch](https://git-scm.com/docs/git-fetch)

### 4. Merge

Se utiliza para fusionar una rama con otra rama activa.

#### Sintaxis:
 `$ git checkout master`
 `$ git merge otra_rama`
 
#### Referencias:
 1. [Git Documentation] *Git merge*, [https://git-scm.com/docs/git-merge](https://git-scm.com/docs/git-merge)
 
### 5. Status

Se utiliza para mostrar la lista de archivos que han sido modificados, además de los que están por ser añadidos.

#### Sintaxis:
 `$ git status`
 
#### Referencias:
 1. [Git Documentation] *Git status*, [https://git-scm.com/docs/git-status](https://git-scm.com/docs/git-status)
 
### 6. Log



#### Sintaxis:
 `$ git log`
 
#### Referencias:
 1. [Git Documentation] *Git log*, [https://git-scm.com/docs/git-log](https://git-scm.com/docs/git-log)

### 7. Checkout

Se utiliza para bajar el contenido almacenado en una rama.

#### Sintaxis:
 `$ git checkout nombre_rama`
 
#### Referencias:
 1. [Git Documentation] *Git checkout*, [https://git-scm.com/docs/git-checkout](https://git-scm.com/docs/git-checkout)

### 8. Clone

Se utiliza para clonar un repositorio de git.

#### Sintaxis:
 `$ git clone https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-3`
 
#### Referencias:
 1. [Git Documentation] *Git clone*, [https://git-scm.com/docs/git-clone](https://git-scm.com/docs/git-clone)


### 9. Commit

Se utiliza para confirmar los cambios en el repositorio local, con el *-m* se le puede agregar una descripción al cambio.

#### Sintaxis:
 `$ git commit -m "mensaje: descripción del commit"`
 
#### Referencias:
 1. [Git Documentation] *Git commit*, [https://git-scm.com/docs/git-commit](https://git-scm.com/docs/git-commit)

------

## Bibliografia

1. [Git Documentation] *Git --fast-version-control*, [https://git-scm.com/docs](https://git-scm.com/docs)

2. [Hostinger] *Comandos básicos de GIT*, [https://www.hostinger.es/tutoriales/comandos-de-git](https://www.hostinger.es/tutoriales/comandos-de-git)

2. [Youtube] *Git & Github - Mitocode*, [https://www.youtube.com/playlist?list=PLvimn1Ins-43-1sXQmGZPWLjNjPyGNi0R](https://www.youtube.com/playlist?list=PLvimn1Ins-43-1sXQmGZPWLjNjPyGNi0R)
 
 3. [Youtube] *Tutorial de Git - Makigas*, [https://www.youtube.com/playlist?list=PLTd5ehIj0goMCnj6V5NdzSIHBgrIXckGU](https://www.youtube.com/playlist?list=PLTd5ehIj0goMCnj6V5NdzSIHBgrIXckGU)

