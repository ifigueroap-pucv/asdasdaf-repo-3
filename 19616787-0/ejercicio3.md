# Ejercicio 3

### Cantidad y nombre de los branches

|Numero|Nombre del branch|
|---|---|
|1|master|

### Cantidad y nombre de los tags

|Numero|Nombre del tag|
|---|---|
|0|No hay tags|

### Nombre, mensaje y código de los últimos 3 commits

|Nombre|Mensaje|Codigo Hash|
|---|:---:|---:|
|Patricio Ignacio Torres Rojas|"decía versiones y era version"|a667bcfd8172306c3b5c9b7f835444ebcdf1d9e|
|Eric Felipe Riveros Novoa|"Agrega link faltante"|bd0af29f2afb56f0e9fa271bc49f9b31d3e7cffa|
|MGRC|"Se agrego glosario.md a la carpeta19616787-0"|6e097886b09549d1e0300c9ce5de0a332d55ac9b |