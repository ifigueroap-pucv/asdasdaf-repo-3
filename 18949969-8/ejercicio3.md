# Resumen del repositorio
##### por Jesús Barrientos G.

 Resumen con la información acerca de los ultimos *commit*, *tag* y *branch* del repositorio.
 
## 1. Branches

 Tabla con información de las ramas existentes.

| No | Branch |
|----|--------|
| 1  | master |

------------

## 2. Tags

 Tabla con información de las etiquetas existentes.
 
| No | Tag    |
|----|--------|
| 1  | v1.0.1 |

------------

## 3. Commits

 Tabla con los últimos tres *commit*.
 
| No | Tag | Mensaje | Código Hash |
|----|-----|---------|-------------|
| 1 | v1.0.1 | Carpeta nueva perteneciente al alumno Jesus Barrientos | 6e2c7499731ccff0506d206e20006541f993c99d |
| 2 | | archivo borrado | 524773b6793b8b0c4a949764d3e45e7883687253 |
| 3 | | Adición de archivo de ejericio3 | eb08933a28f60c7f0a6b294e9002c70787ee532e|