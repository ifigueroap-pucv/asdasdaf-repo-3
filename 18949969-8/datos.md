# Datos del Alumno
##### por Jesús Barrientos G.

 Datos del alumno para permitir la comunicación y gestión del código en el curso INF3240-1 (Ingeniería Web).
 
| RUT | Nombre | Usuario Bitbucket | Correo Bitbucket |
|-----|--------|-------------------|------------------|
| 18.949.969-8 | Jesús Alberto Barrientos González | jesbarrien | jesbarrien1995@gmail.com |