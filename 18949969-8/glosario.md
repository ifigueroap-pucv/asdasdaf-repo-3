# Glosario Git
##### por Jesús Barrientos G.

El siguiente documento presenta conceptos y términos acerca de *git* y sus respectivas definiciones.

A continuación, se presenta la lista de contenidos.

## Conceptos

1. [Control de versiones (VC)](#1-Control-de-versiones-(VC))
2. [Control de versiones distribuido (DVC)](#2-control-de-versiones-distribuido-(DVC))
3. [Repositorio local](#3-repositorio-local)
4. [Repositorio remoto](#4-repositorio-remoto)
5. [Working Copy](#5-working-copy)
6. [Staging Area](#6-staging-area)
7. [Stage Changes](#7-stage-changes)
8. [Commit Changes](#8-commit-changes)
9. [Branch](#9-branch)
10. [Tag](#10-tag)

## Comandos

1. [*commit*](#1-commit)
2. [*clone*](#2-clone)
3. [*pull*](#3-pull)
4. [*push*](#4-push)
5. [*fetch*](#5-fetch)
6. [*merge*](#6-merge)
7. [*status*](#7-status)
8. [*log*](#8-log)
9. [*checkout*](#9-checkout)

------------

# Definición de Concepto

## 1. Control de versiones (VC)

 Sistema que permite llevar un registro de un archivo o de un conjunto de estos, contribuyendo a la organización a través de versiones generadas en tiempos variados. Es decir, cualquier acción sobre estos archivos podría ser versionada con el fin de llevar el registro producido en estos y así en el tiempo saber que se ha hecho. Además, no solo lleva un historial, sino que también se puede recuperar a un punto de la línea de tiempo con el propósito de recuperar algo sea cuál sea la situación.

#### Referencias:
  1. [Git Documentation] *1 Empezando - Acerca del control de versiones*, [https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

## 2. Control de versiones distribuido (DVC)

 Este tipo de sistema se considera mucho más seguro que los de tipo [*local y centralizados*](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones), ya que los clientes descargan el repositorio completo y no sólo la última instancia de los ficheros. Así, si se llegase a estropear el servidor donde se almacena el proyecto, cualquier cliente podría subir nuevamente todo sin problemas.

#### Referencias:
 1. [Git Documentation] *1 Empezando - Acerca del control de versiones*, [https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

## 3. Repositorio local

 Los repositorios es prácticamente lo mismo que la carpeta raíz del proyecto, pero con la diferencia que estos poseen funciones o funcionalidades generadas por los servidores de alojamiento como [*GitHub*](https://github.com/) o [*Bitbucket*](https://bitbucket.org).

 Entonces, los repositorios locales serán los proyectos almacenados como su palabra lo dice "localmente", con la finalidad de poder trabajar incluso sin conexión a internet. Por lo tanto, cada cliente podrá hacer sus cambios y luego subirlos al servidor donde se comparte con los demás.

 ![alt text](https://i.stack.imgur.com/UvZ0M.png "Repositories")

#### Referencias:
 1. [Stackoverflow] *Git's local repository and remote repository — confusing concepts*, [https://stackoverflow.com/questions/13072111/gits-local-repository-and-remote-repository-confusing-concepts](https://stackoverflow.com/questions/13072111/gits-local-repository-and-remote-repository-confusing-concepts)

## 4. Repositorio remoto

 Como se mencionó anteriormente, a diferencia de los repositorios locales, los remotos se encuentran alojados en los *servidores de alojamiento*, permitiendo a los clientes actualizar o descargar de estos.

## 5. Working Copy

 La copia de trabajo hace referencia a los repositorios remotos, donde como su nombre lo indica, hace una copia del proyecto y te crea un clon en tu ordenador (repositorio local).

 Esto se puede hacer gracias al comando *clone* que se verá más adelante. Básicamente, este comando crea un directorio ".git" en la raíz de la carpeta donde se almacenará el repositorio.

#### Referencias:
 1. [Git Documentation] *1 Fundamentos de Git - Obteniendo un repositorio Git*, [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

## 6. Staging Area

 El área de preparación es un archivo que almacena la información correspondiente a lo que se hará en la próxima confirmación o más conocido como *commit*.

  ![alt text](https://git-scm.com/figures/18333fig0106-tn.png "Áreas de estados")

#### Referencias:
 1. [Git Documentation] *3 Empezando - Fundamentos de Git*, [https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

## 7. Stage Changes

 Momento donde el usuario añade o remueve de la versión a generar los archivos que desee introducir o no. Es decir, cuando se identifican cambios en ficheros, es el usuario quien decide que archivos aceptará tales cambios que posteriormente serán publicados con el comando *commit*. Para ver el estado del proyecto (cambios, nuevos ficheros, archivos quitados, etc.), se utiliza el comando *status*.

#### Referencias:
 1. [Git Documentation] *2 Fundamentos de Git - Guardando cambios en el repositorio*, [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

## 8. Commit Changes

 Una vez que los cambios son preparados, queda aceptarlos y versionar estos. Para esto se puede añadir una descripción e incluso una etiqueta. El comando utilizado es el ya mencionado *commit*.

#### Referencias:
 1. [Git Documentation] *2 Fundamentos de Git - Guardando cambios en el repositorio*, [https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

## 9. Branch

 Cada vez que git almacena un cambio, en realidad lo que hace es almacenar un punto de control, que básicamente posee el cambio como tal y apuntadores a sus confirmaciones anteriores (en caso de merge, múltiples apuntadores). Entonces, las ramas son un apuntador que conecta con estas confirmaciones que ocurren en el tiempo. La rama principal es la "master" y se pueden crear múltiples ramas con el fin de dividir los procesos en el desarrollo o cambio del proyecto.

#### Referencias:
 1. [Git Documentation] *1 Ramificaciones en Git - ¿Qué es una rama?*, [https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

## 10. Tag

 Las etiquetas son referencias que apuntan a un cambio en específico con el fin de establecer una versión (v1.0.1). Esta etiqueta jamás cambiará a no ser que el usuario lo desee.

#### Referencias:
 1. [Atlassian] *Git tag - Tagging*, [https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag](https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag)

------------

# Definición de Comandos

## 1. *commit*

 Almacena los cambios efectuados en el proyecto.

#### Sintaxis:
 `$ git commit -m "mensaje: descripción del commit"`

#### Referencias:
 1. [Git Documentation] *Git commit*, [https://git-scm.com/docs/git-commit](https://git-scm.com/docs/git-commit)

## 2. *clone*

 Clona un repositorio en un directorio especificado.

#### Sintaxis:
 `$ git clone git://git.kernel.org/pub/scm/.../linux.git my-linux`

#### Referencias:
 1. [Git Documentation] *Git clone*, [https://git-scm.com/docs/git-clone](https://git-scm.com/docs/git-clone)

## 3. *pull*

 Obtiene del repositorio remoto los cambios nuevos y los actualiza en el remoto clonado.

#### Sintaxis:
 En la siguiente sintaxis, se obtiene los datos actualizados del remoto "origin" y los actualiza en la rama "master".

 `$ git pull origin master`

#### Referencias:
 1. [Git Documentation] *Git pull*, [https://git-scm.com/docs/git-pull](https://git-scm.com/docs/git-pull)

## 4. *push*

 Actualiza el repositorio remoto con los cambios efectuados en el repositorio local.

#### Sintaxis:
 En la siguiente sintaxis, se actualiza el remoto "origin" en la rama "master". De no existir esta rama, se crea automáticamente.

 `$ git push origin master`

#### Referencias:
 1. [Git Documentation] *Git push*, [https://git-scm.com/docs/git-push](https://git-scm.com/docs/git-push)

## 5. *fetch*

 Obtiene del remoto los cambios nuevos. A diferencia del comando [*pull*](#3-pull), este no actualiza la rama, para eso es necesario el comando *merge*.

#### Sintaxis:
 En la siguiente sintaxis, se obtienen las actualizaciones del remoto "origin".

 `$ git fetch origin`

#### Referencias:
 1. [Git Documentation] *Git fetch*, [https://git-scm.com/docs/git-fetch](https://git-scm.com/docs/git-fetch)

## 6. *merge*

 Une los cambios realizados en una rama con otra.

#### Sintaxis:
 En los comandos siguientes, se especifica que se debe estar posicionado en la rama a la que se desea actualizar y luego hacer la unión.

 `$ git checkout master`

 `$ git merge otra_rama`

#### Referencias:
 1. [Git Documentation] *Git merge*, [https://git-scm.com/docs/git-merge](https://git-scm.com/docs/git-merge)

## 7. *status*

 Entrega el resumen de la rama, es decir, si se agregó, modificó o eliminó un archivo/s.

#### Sintaxis:
 `$ git status`

#### Referencias:
 1. [Git Documentation] *Git status*, [https://git-scm.com/docs/git-status](https://git-scm.com/docs/git-status)

## 8. *log*

 Muestra el historial de *commit*.

#### Sintaxis:
 `$ git log`

#### Referencias:
 1. [Git Documentation] *Git log*, [https://git-scm.com/docs/git-log](https://git-scm.com/docs/git-log)

## 9. *checkout*

 Permite seleccionar la rama con que se trabajará. Por defecto la rama "master", es la principal.

#### Sintaxis:
 `$ git checkout nombre_rama`

#### Referencias:
 1. [Git Documentation] *Git checkout*, [https://git-scm.com/docs/git-checkout](https://git-scm.com/docs/git-checkout)

------------

# Bibliografía

 1. [Git Documentation] *Git --fast-version-control*, [https://git-scm.com/docs](https://git-scm.com/docs)

 2. [Youtube] *Git & Github - Mitocode*, [https://www.youtube.com/playlist?list=PLvimn1Ins-43-1sXQmGZPWLjNjPyGNi0R](https://www.youtube.com/playlist?list=PLvimn1Ins-43-1sXQmGZPWLjNjPyGNi0R)

 3. [Youtube] *Tutorial de Git - Makigas*, [https://www.youtube.com/playlist?list=PLTd5ehIj0goMCnj6V5NdzSIHBgrIXckGU](https://www.youtube.com/playlist?list=PLTd5ehIj0goMCnj6V5NdzSIHBgrIXckGU)
