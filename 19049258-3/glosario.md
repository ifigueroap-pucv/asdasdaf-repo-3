1. Control de versiones (VC): Gestión de todos los cambios realizados hasta el momento sobre los elementos o componentes propios de algún proyecto o sistema. Esto implica mantener un registro de cada acción realizada (como editar, añadir o eliminar), los responsables, las razones de esto y las consecuencias que estas conllevan. Gracias al control de versiones, se puede volver a una versión anterior estable del proyecto, de modo que los errores son fácilmente solucionables. 
[Fuente 1](https://es.wikipedia.org/wiki/Control_de_versiones)
[Fuente 2](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

2. Control de versiones distribuido (DVC): En este tipo de sistemas, los clientes o computadores clonan todo el repositorio que se encuentra en el servidor o computador central, de modo que si este muere, el repositorio se puede recuperar gracias a las copias de los clientes. En otras palabras, se trata de una red de equipos que mantienen simultánea e independientemente el repositorio principal.
[Fuente 1](https://es.wikipedia.org/wiki/Control_de_versiones)
[Fuente 2](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

3. Repositorio remoto y Repositorio local: Un repositorio es básicamente el lungar donde se almacena todo el código y elementos a utilizar para un proyecto de, en esta instancia al menos, carácter informático. Dicho esto, existen dos tipos de repositorios: remotos y locales. Los remotos, son versiones del proyecto que se encuentran alojados en algún equipo o servidor externo, al cual se accede vía internet. El local, en cambio, es aquel que se encuentra en el equipo propio.
[Fuente](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

4. Copia de trabajo / Working Copy: Se refiere a los clones que terceros realizan del repositorio, por lo que pueden existir muchas copias al mismo tiempo, y cada quién puede trabajar sobre ellas. Todos los cambios o modificaciones realizadas, pueden ser "envíados" de regreso al repositorio, de modo que se mantiene el registro de qué cambios se realizaron y por quién.
[Fuente](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

5. Área de preparación / Staging Area: En esta área se preparan los archivos que serán actualizados finalmente en el repositorio, en otras palabras, es un tipo de fase intermedia antes de realizar el "push".
[Fuente](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)

6. Preparar cambios / Stage Changes: Se refiere a preparar y organizar los cambios que se realizarán en los archivos del repositorio. Esto puede incluir añadir o incluso eliminar otros.
[Fuente](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)

7. Confirmar cambios / Commit Changes: Se refiere a la acción de, valga la redundancia, confirmar los cambios una vez que el área de preparación se encuentra lista o finalizada. Se ejecuta realizando un "git commit", donde se explicará o comentarán cuáles fueron los cambios realizados, sus razones y consecuencias. Vale decir que la estructura de este commit está bien definida.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

8. Commit: Comando y acción de confirmar los cambios realizados en el área de preparación, lo que implicará registrar, comentar y explicarlos. 
[Fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

9. Clone: Comando y acción de clonar un repositorio ya existente desde otro equipo o sistema. Esto significa que se descargará toda la información asociada a dicho repositorio, es decir, los archivos de código o los commits realizados, por ejemplo.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

10. Pull: Comando y acción de recuperar o recibir, y a la vez fusionar automáticamente el código o los cambios que se encuentran clonados en otro equipo o sistema. Vale hacer notar que la diferencia fundamental con "fetch" es la fusión automática de los archivos, ya que este último no lo realiza. 
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

11. Push: Comando y acción de enviar los cambios realizados al repositorio original o "master", el cuál se clonó previamente. 
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

12. Fetch: Comando y acción recuperar o recibir los cambios realizados en las copias del repositorio, sin embargo, la fusión no es automática como en el caso de "pull".
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

13. Merge: Comando y acción de fusionar ramas específicas de un proyecto.
[Fuente 1](https://git-scm.com/book/es/v2/Appendix-B%3A-Comandos-de-Git-Ramificar-y-Fusionar)
[Fuente 2](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

14. Status: El comando "git status" mostrará el estado de los archivos a modificar (área de preparación). Listará los archivos que se agregaron, que se eliminaron o que se editaron. 
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

15. Log: Comando que mostrará un historial de todos los cambios realizados al repositorio.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

16. Checkout: Comando que permite cambiarse hacia una rama en particular para trabajar en ella. 
[Fuente 1](https://git-scm.com/docs/git-checkout)
[Fuente 2](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

17. Rama / Branch: Se pueden entender las ramas como áreas o partes específicas de un proyecto, que pueden tener o no relación entre sí. De este modo, se puede trabajar en distintos sectores simultáneamente sin afectar la rama principal (master). 
[Fuente 1](https://www.adictosaltrabajo.com/tutoriales/git-branch-bash/#3.%20Branch%20en%20Git)
[Fuente 2](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

18. Etiqueta / Tag: Las etiquetas sirven en cierto modo para "nombrar" ciertas versiones, hitos o "releases" importantes durante el transcurso del proyecto. El ejemplo más icónico es precisamente "v 1.0", haciendo alusión a la versión 1 del software. 
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)