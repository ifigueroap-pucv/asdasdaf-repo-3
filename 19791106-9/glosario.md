# Glosario Ingenieria Web

#### a. Control de versiones (VC)

El control de versiones es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones específicas más adelante.
https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
### b. Control de versiones distribuido (DVC)

En un DVCS (como Git, Mercurial, Bazaar o Darcs), los clientes no sólo descargan la última instantánea de los archivos: replican completamente el repositorio. Así, si un servidor muere, y estos sistemas estaban colaborando a través de él, cualquiera de los repositorios de los clientes puede copiarse en el servidor para restaurarlo.
https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
### c. Repositorio remoto y Repositorio local

Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en algún punto de la red. Puedes tener varios, cada uno de los cuales puede ser de sólo lectura, o de lectura/escritura, según los permisos que tengas.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
### d. Copia de trabajo / Working Copy

Todos los archivos, incluidas sus versiones anteriores, se encuentran en un repositorio o repositorio. De este modo, todos los cambios que se han transferido al repositorio están siempre disponibles, y la pregunta de: "¿Quién hizo qué cambia cuando?" puede ser preguntado.
https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms

### e. Área de Preparación / Staging Area

El área de preparación es un sencillo archivo, generalmente contenido en tu directorio de Git, que almacena información acerca de lo que va a ir en tu próxima confirmación.
https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git

### f. Preparar Cambios / Stage Changes

Es un paso previo al proceso de confirmación en git. Es decir, una confirmación en git se realiza en dos pasos: puesta en escena y confirmación real. Siempre que un conjunto de cambios esté en el área de preparación, git le permite editarlo a su gusto (reemplace los archivos por etapas con otras versiones de archivos en etapas, elimine los cambios de la etapa, etc.)
https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git

#### g. Confirmar cambios / Commit Changes

La forma canónica de referirse a una confirmación de cambios es indicando su código-resumen criptográfico SHA-1. Pero también existen otras maneras más sencillas.
https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Selecci%C3%B3n-de-confirmaciones-de-cambios-concretas
### h. Commit

A medida que editas archivos, Git los ve como modificados, porque los has cambiado desde tu última confirmación. Preparas estos archivos modificados y luego confirmas todos los cambios que hayas preparado, y el ciclo se repite.
![alt text](https://git-scm.com/figures/18333fig0201-tn.png)
https://git-scm.com/figures/18333fig0201-tn.png
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

### i. clone

Copia un repositorio remoto en un repositorio local existente en tu computador o crea un repositorio local con los datos del repositorio remoto clonado.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

### j. pull

Recupera y une automáticamente la rama remota con tu rama actual. Ésto puede resultarte un flujo de trabajo más sencillo y más cómodo.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

### k. push

Sube tu parte del proyecto el cual has modificado y lo modifica en el repositorio remoto donde tienes tu projecto original.
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

### l. fetch

Descarga obejetos y referencias desde otro repositorio.

### m. merge

Fusiona la rama actual con la rama master en el repositorio remoto.

### n. status

Muestra el estado actual de la rama.

### o. log

Muestra el historial de modificaciones que se han echo en el repositorio y muestra los commit echos con su codigo.

### p. checkout

Crea una rama nueva y salta hacia ella, esto sirve para poder modificar el programa sin modificar el original.

### q. Rama / Branch

Las ramas son utilizadas para desarrollar funcionalidades aisladas unas de otras. La rama master es la rama "por defecto" cuando creas un repositorio. Crea nuevas ramas durante el desarrollo y fusiónalas a la rama principal cuando termines.
http://rogerdudler.github.io/git-guide/index.es.html

### r. Etiqueta / Tag

Los tag son cambios de "nombre" a los commit realizados para asi poder identificar de manera mas adecuada el commit que uno quiera.
