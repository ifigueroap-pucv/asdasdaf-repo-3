1. Cantidad y nombre de los branches: 1 Branch único llamado "master".
	
2. Cantidad y nombre de las etiquetas: 0 tags.

3. Nombre, mensaje y código hash de los últimos 3 commits:

	commit c101e94b4b4773332741bcea78dd43a94801114d (HEAD -> master)
	Author: Macarena Gárate M <31232682+Macagarate@users.noreply.github.com>
	Date:   Mon Aug 27 20:38:39 2018 -0300

	    Se agrego glosario

	commit 9b3e4ca85b31e6075457208f76c6dcef394268df (origin/master, origin/HEAD)
	Merge: 04c5858 83d2270
	Author: Octavio Oyanedel - PC casa <octavio.oyanedel@gmail.com>
	Date:   Mon Aug 27 23:03:16 2018 -0300

	    Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo-3

	    Actualizar repositorio

	commit 04c58584e76fd766fc92a95dd044d99dde838972
	Author: Octavio Oyanedel - PC casa <octavio.oyanedel@gmail.com>
	Date:   Mon Aug 27 23:02:58 2018 -0300

	    Parte 1, punto 8 add y commit a repositorio local, push a repositorio bitbucket
