# Glosario

__1. Control de versiones (VC):__ Basicamente es un sistema que va registrando los cambios que se realizan sobre uno o varios archivos a través
del tiempo con el fin de poder llamar a una version anterior en caso de ser necesario.

_Source: [Control de Versiones - Git-SCM](https://git-scm.com/book/es-ni/v1/Iniciando-Acerca-del-Control-de-Versiones)_

__2. Control de versiones distribuido (DVC):__ Surge como solución al diagrama de control de versiones centralizado, ya que si se caía
el servidor no se podian colaborar o guardar cambios. En este tipo de control de
versiones los usuarios no descargan únicamente la última copia de los archivos, sino que descargan completamente el repositorio. Entonces, en
caso de caerse el servidor, el cliente puede subir la copia que tenia más los cambios efectuados al servidor.

_Source: [Control de Versiones Distribuido - Git-SCM](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)_

__3. Repositorio remoto y Repositorio local:__ El repositorio local se refiere a la copia local que no esta sincronizada con el repositorio remoto. 
En cambio, el repositorio remoto se refiere a la copia de los archivos que está alojada en algún servidor web y que es compartido por varios clientes.

_Source: [Repositorio Remoto y Local - Git-SCM](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)_

__4. Working Copy:__ Es un directorio completo con un subdirectorio (.git) y es igual al repositorio local.

_Source: [Working Copy mean - Stackoverflow](https://stackoverflow.com/questions/43333940/xcode-what-does-working-copy-mean-anyway)_
	
__5. Staging Area:__ Es un archivo, generalmente almacenado en el directorio del git. Este almacena la información que 
irá en el próximo commit. 

_Source: [Staging Area - Git-SCM](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)_

__6. Stage Changes:__ Son los cambios que almacena en memoria el proceso de Staging Area. En resumen, todos los cambios
realizados que se encuentran almacenados en el archivo de Staging. 

_Source: [Stage Changes - Git-SCM](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)_
				
__7. Commit changes:__ Son los cambios que van a ser actualizados en el repositorio.

_Source: [Commit changes - Git-SCM](https://git-scm.com/docs/git-commit)_

__8. Commit:__ Basicamente es actualizar, solicitar y guardar cambios en el repositorio.

_Source: [Commit - Quora](https://www.quora.com/What-is-the-meaning-of-commit-and-stage-in-git)_
				
__9. clone:__ Clona un repositorio en un directorio, crea branches remotos para cada branch clonado desde el repositorio. 
Además crea y hace un check out sobre la branch principal obtenida de la branch del repositorio clonado.

_Source: [git-clone - Git-SCM](https://git-scm.com/docs/git-clone)_
				
__10. pull:__ Incorpora cambios desde un repositorio local en el branch actual.

_Source: [git-pull - Git-SCM](https://git-scm.com/docs/git-pull)_
				
__11. push:__ Actualiza referencias remotas (repositorio) con las referencias locales (repositorio local).

_Source: [git-push - Git-SCM](https://git-scm.com/docs/git-push)_
				
__12. fetch:__ Descargar objetos y referencias desde otro repositorio.
Pueden ser uno o más repositorios y cuando se realiza la operacion las branches remotas se actualizan automaticamente.

_Source: [git-fetch - Git-SCM](https://git-scm.com/docs/git-fetch)_
				
__13. merge:__ Agrega cambios de los commits en la branch actual. Basicamente une dos o más commits.

_Source: [git-merge - Git-SCM](https://git-scm.com/docs/git-merge)_
				
__14. status:__ Muestra el estado actual del arbol de trabajo. Muestra las rutas distintas entre el index file y el HEAD actual, 
				las rutas que tienen diferencais entre el arbol de trabajado y el index file no son administradas y rastreadas 
				por git.

_Source: [git-status - Git-SCM](https://git-scm.com/docs/git-status)_
				
__15. log:__ Muestra el registro de commits y la clave que pertenece a cada commit.

_Source: [git-log - Git-SCM](ttps://git-scm.com/docs/git-log)_

__16. checkout:__ Cambia entre las branches disponibles y/o restaura los ficheros de un arbol de trabajo.

_Source: [git-checkout - Git-SCM](https://git-scm.com/docs/git-checkout)_
			
__17. Branch(Rama):__ Muestra, crea y/o elimina branches. Basicamente una branch es una linea de desarrollo y se utiliza con el fin
				de no dañar y/o modificar el branch original.

_Source: [git-branch - Git-SCM](https://git-scm.com/docs/git-branch)_
		
__18. Tag(Etiqueta):__ Crea, muestra, elimina o verifica la etiqueta de un objeto. Util para cuando se quiera separar versiones
				estables en el desarrollo. 
				
_Source: [git-tag - Git-SCM](https://git-scm.com/docs/git-tag)_