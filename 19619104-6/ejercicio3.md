# Ejercicio 3

__1. Branches:__

- Cantidad: _1_
- Nombre: _master_

__2. Tags:__

- Cantidad : 0

__3. Commit:__

a. __commit:__ _7bbaa734c99b353cac8f28251d25d67d68d05f5_
	* __Author:__ Francisco Riquelme Cavagnola <francisco.riquelme@outlook.com>
	* __Date:__ Sun Aug 26 21:22:41 2018 -0300
	* __Message:__ Agrega glosario a la carpeta 19619104-6.
	
b. __commit:__ _ae29d2bbe27e5303c36023c4716a7522765b01e4_
	* __Author:__ Katherine Arévalo <arevaloparker@gmail.com>
	* __Date:__ Sun Aug 26 20:15:06 2018 -0300
	* __Message:__ 1 SE AGREGARON DATOS DE KATHERINE

c. __commit:__ _86a130a72fd7e990c137e5645b4ca01380f3ef94_
	* __Author:__ Katherine Arévalo <arevaloparker@gmail.com>
	* __Date:__ Sun Aug 26 20:03:06 2018 -0300
	* __Message:__ 1 Se agrega ejercicio de katherine
