# Glosario

## [Control de versiones (VC)]

Es un programa que sirve para llevar un registro de todos los cambios realizados a un conjunto de archivos y de esta forma se va guardando versiones historicas de los archivos.
Permite poder revertir cambios, ver quien realizo modificaciones y cuando, etc.

## [Control de versiones distribuido (DVC)]

A diferencia del VC, aquí todos los clientes que estén utilizando el repositorio copian completamente el repositorio, de esta forma se facilita la cooperacion en trabajos grupales.

## Repositorio remoto y Repositorio local

Un repositorio es un lugar donde se almacenan los archivos de un proyecto. Este puede ser almacenado de forma local o en un servidor remoto.
Normalmente cuando se trabaja, uno copia el repositorio remoto a su equipo de trabajo para trabajar localmente y solo cuando la modificaion está terminada y sin fallos, en teoria, se sube al repositorio remoto.

## [Copia de trabajo / Working Copy]

Se le llama a la operación que clona el repositorio remoto al equipo local.

## [Área de Preparación / Staging Area]

Es un lugar donde se listan los cambios a realizar entre un commit y otro.

## [Preparar Cambios / Stage Changes]

Los cambios se preparan, agregando archivos al Stagin Area.
Para realizar esto se utiliza los comandos add

## Confirmar cambios / Commit Changes

Para confirmar los cambios que tenemos en el Stagin Area, se utiza Commit, que confirma los cambios.

## [Commit]

Es el comando que realiza la confimacion de los cambios. Se debe comentar los cambios realizados del commit.

## [clone]

Es el comando que realiza una clonacion de un repositorio a una carpeta. Pudiendo indicarle la rama a clonar.

## [pull]

Pull sirve para actualizar el repositorio local y fusionar los cambios remotos.

## [push]

Push se utiliza para enviar los realizados del repositorio local al remoto.

## [fetch]

Fetch se utiliza para descartar los cambios locales y bajar la ultima version existente en el repositorio remoto.

## [merge]

Sirve para fusionar diferentes ramas en una sola.

## [status]

Muestra un listado de de los archivos que han sido modificados entre el Staging Area y el Repositorio

## [log]

Muestra el listado de commit del repositorio con sus detalles.

## [checkout]

Sirve para cambiar la rama, crear rama o listar las ramas, entre otras cosas.

## Rama / [Branch]

Es un historial de commit provenientes de alguna version en especifico.

## Etiqueta / [Tag]

Normalmente se utiliza para darle un nombre a cierto commit, como para indicar alguna version significativa.

[Control de versiones (VC)]: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
[Control de versiones distribuido (DVC)]: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones#Sistemas-de-control-de-versiones-distribuidos
[Copia de trabajo / Working Copy]: https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone
[Área de Preparación / Staging Area]: https://git-scm.com/about/staging-area
[Preparar Cambios / Stage Changes]: https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html
[Commit]: https://git-scm.com/docs/git-commit
[clone]: https://git-scm.com/docs/git-clone
[pull]: https://git-scm.com/docs/git-pull
[push]: https://git-scm.com/docs/git-push
[fetch]: https://git-scm.com/docs/git-fetch
[merge]: https://git-scm.com/docs/git-merge
[status]: https://git-scm.com/docs/git-status
[log]: https://git-scm.com/docs/git-log
[checkout]: https://git-scm.com/docs/git-checkout
[Branch]: https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F
[Tag]: https://git-scm.com/docs/git-tag