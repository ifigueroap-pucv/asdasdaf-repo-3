# Glosario

**1.Control de versiones ***(CV)***:** El control de versiones es un sistema que registra los cambios realizados a un archivo o archivos a lo largo del tiempo, siendo un registro una versión de los archivos en un tiempo específico, por lo que se puede recuperar una versión anterior revirtiendo los cambios que fueron realizados. Existen distintas formas de control de versiones y dependiendo de la manera en se trabajará puede ser locales, centralizados y distribuidos. 

**2.Control de versiones distribuido ***(DVC)***:** En el control de versiones distribuido cada vez que se descarga los datos del repositorio no solo se tiene la última versión registrada, si no que se tiene una copia completa de todos lo que se encuentran en el repositorio, esto quiere decir que se pueden ver todos los cambios que han realizados directamente sin consultar al repositorio compartido. Además en esta copia local se pueden realizar cambios, y si se desea pueden ser enviados al repositorio compartido.

**3.Repositorio remoto y Repositorio local:** Un repositorio remoto es el repositorio que se tiene alojado en Internet o en algún lugar en la Red, cuando se comparte este es necesario utilizar el push para subir los cambios y pull para obtener los datos del repositorio, de esa forma se comparte los cambios que se van realizando. Mientras que un repositorio local es el repositorio que se encuentra en el computador, este está constituido por tres etapas o estado, el primero es directorio de trabajo, donde se encuentran los archivos, el segundo es el stage, en este se guardan todos los cambios de forma momentánea hasta que se realice un commit y por último está el head el cual apunta al ultimo commit que se hizo.

**4.Copia de trabajo/***Working copy***:** Es una copia local donde se trabaja, en otras palabras donde se encuentran los archivos que se han modificado.

**5.Área de preparación/***Staging area***:** El área de preparación es un archivo, en el que se almacena la información sobre el que será el siguiente commit.

**6.Preparar cambios/***Stage changes***:** Para preparar los cambios se utiliza el comando add, de esta forma quedan en el área de preparación y pasan al estado de preparados(staged).

**7.Confirmar cambios/***Commit changes***:** Cuando se considera que el área de preparación esta bien se confirman los cambios realizados utilizando el comando commit, al realizar esto se abrirá un editor de texto donde deberá agregar el comentario del commit. 

**8.commit:** El comando commit es una confirmación de los cambios realizados, los que pasan a estar en el repositorio local. Cuando se utiliza se modifica el head.

**9.clone:** El comando clone es para hacer una copia de un repositorio.

**10.pull:** El comando pull es utilizado para descarga los archivos del repositorio remoto y de esta forma actualizar el repositorio local.

**11.push:** Comando utilizado para enviar los cambios realizado en el repositorio local ubicados ya en el head al repositorio remoto.

**12.fetch:** El comando fetch permite descargar del repositorio remoto los cambios de una rama determinada y crear una rama copia que estará escondida en el repositorio local, en esta se pueden ver los cambios que se han realizados, si luego se utiliza el merge cumple la misma función que realizar un pull.

**13.merge:** Este comando es utilizado para fusionar dos ramas, si al intentar fusionar dos ramas ocurren algún conflicto este debe ser solucionado manualmente.

**14.log:** Utilizado para ver una lista de los commit realizados en una rama, en la cual se muestran algunos datos del commit como el número de identificación, su comentario, el nombre del autor y la fecha en la fue realizado.

**15.checkout:** Este comando puede ser utilizado para crear una rama y para cambiar a de rama. Además este comando puede ser usado para revertir errores en el repositorio local.

**16.Rama/***branch***:** Las ramas  funcionan como bifurcaciones de un mismo proyecto, lo cual sirve en el caso de que se estén realizando cambios en modo de prueba y no se desean integrar aun a la rama principal, por defecto existe una rama principal (master).

**17.Etiqueta/***tag***:** Son utilizados para simplificar el identificador de un commit, aunque como requisito para asignar una etiqueta a un commit es el que este sea único.


### Fuente online 

| Palabra | Link |
| ------ | ------ |
| Control de versiones | [hipertextual.com/archivo/2014/05/git-sistema-control-versiones/][Cv] |
| Control de versiones distribuidos | [hipertextual.com/archivo/2014/05/git-sistema-control-versiones/][Cdv] |
| Repositorio remoto y Repositorio local | [colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/][R] |
| Copia de trabajo/Working copy| [magmax.org/blog/usando-git/][Wc] |
| Área de preparación/Staging area | [www.makigas.es/series/tutorial-de-git/que-es-el-staging-area/][Sa] |
| Preparar cambios/Stage changes | [gist.github.com/juanghurtado/7a819d4f07619e944b56][Sc] |
| Confirmar cambios/Commit changes | [git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git][Cc] |
| commit | [es.stackoverflow.com/questions/28344/cuál-es-la-diferencia-entre-commit-y-push-en-git][Com] |
| clone | [www.hostinger.es/tutoriales/comandos-de-git][Clo] |
| pull | [www.hostinger.es/tutoriales/comandos-de-git][Pl] |
| push | [es.stackoverflow.com/questions/28344/cuál-es-la-diferencia-entre-commit-y-push-en-git][Ps] |
| fetch | [es.stackoverflow.com/questions/245/cuál-es-la-diferencia-entre-pull-y-fetch-en-git][F] |
| merge | [www.hostinger.es/tutoriales/comandos-de-git][M]  |
| log | [www.hostinger.es/tutoriales/comandos-de-git][L] |
| checkout | [www.hostinger.es/tutoriales/comandos-de-git][Ch] |
| Rama/branch | [www.adictosaltrabajo.com/tutoriales/git-branch-bash/][B]  [www.arsys.es/blog/programacion/ramas-git/][Ss]|
| Etiqueta/tag | [www.hostinger.es/tutoriales/comandos-de-git][E] |

 
   [Cv]: <https://hipertextual.com/archivo/2014/05/git-sistema-control-versiones/>
   [Cdv]: <https://hipertextual.com/archivo/2014/05/git-sistema-control-versiones/>
   [R]: <https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/>
   [Wc]: <https://magmax.org/blog/usando-git/>
   [Sa]: <https://www.makigas.es/series/tutorial-de-git/que-es-el-staging-area>
   [Sc]: <https://gist.github.com/juanghurtado/7a819d4f07619e944b56>
   [Cc]: <https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git>
   [Com]: <https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git>
   [Clo]: <https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2nBFQu%C3%A9-es-una-rama%3F>
   [Pl]: <https://www.hostinger.es/tutoriales/comandos-de-git>
   [Ps]: <https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git>
   [F]: <https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git>
   [M]: <https://www.hostinger.es/tutoriales/comandos-de-git>
   [L]: <https://www.hostinger.es/tutoriales/comandos-de-git>
   [Ch]: <https://www.hostinger.es/tutoriales/comandos-de-git>
   [B]: <https://www.adictosaltrabajo.com/tutoriales/git-branch-bash/>
   [Ss]: <https://www.arsys.es/blog/programacion/ramas-git/>
   [E]: <https://www.hostinger.es/tutoriales/comandos-de-git>