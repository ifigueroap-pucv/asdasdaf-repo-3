**1. Cantidad y nombre de las branches**

| N° | Branch |
|----|--------|
| 1  | master |

**2. Cantidad y nombre de las etiquetas**
No existen etiquetas

**3.Nombre, mensaje y código hash de los últimos 3 commits**

|Autor| Mensaje | Código Hash |
|------|--------|-------------|
|Karina Bernal| Agrega glosario | 98d528d0674359cdfd671ec58641a6042db8dd53 |
|Fernando Carvajal|Se agrega archivo datos.md a carpeta 18236041-4 |9d80ed29045417a06aed8684a704f144a7136c97 |
|Fernando Carvajal|Se agrega archivo ejercicio3.md a carpeta 18236041-4| 634afb8475627a1c72c517b83d42551c44223b04
