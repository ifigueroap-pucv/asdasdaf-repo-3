# Ejercicio 3

**a. Cantidad y nombre de branches:**
    - Cantidad: 1
    - Nombre: master
    
**b. Cantidad y nombre de las etiquetas**
    - 0 Etiquetas

**c. Nombre, mensaje y código hash de los últimos 3 commits**

commit 191beb5b24e02cd1271181e18542f9026697f055 (HEAD -> master, origin/master, origin/HEAD)
Author: Pablo Castillo <pablo.castillo.p@mail.pucv.cl>
Date:   Fri Aug 24 11:00:29 2018 -0300

    aqui van los datos

****

commit 6c380bcebb2392465a97c0d06bac81123b5cb113
Author: Pablo Castillo <pablo.castillo.p@mail.pucv.cl>
Date:   Fri Aug 24 10:58:54 2018 -0300

    Aqui va el ejercicio3


****

commit f132532bcdb6de0856ca2b7b3a7a5d2342b88c5d
Author: Pablo Castillo <pablo.castillo.p@mail.pucv.cl>
Date:   Fri Aug 24 10:57:14 2018 -0300

    Aqui va el glosario con las definciones

 