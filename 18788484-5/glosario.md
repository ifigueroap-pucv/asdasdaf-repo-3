# Taller 0 - Definiciones
**a. Control de Versiones (VC):**
Son registros de los cambios realizados en un archivo dentro de un repositorio local o remoto, los cuales pueden volver a estados de versiones anteriores, del tal modo que se pueda recuperar archivos de versiones específicas.
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

**b. Control de versiones distribuido (DVC)**
Los usuarios pueden replicar completamente el repositorio,  además de restaurar a la versión anterior de un archivo, por ende si una sistema falla, este puede ser restaurado por algún usuario, ofreciendo soluciones para los problemas de colaboración entre desarrolladores, etc.
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

**c. Repositorio remoto y Repositorio local**
Un repositorio remoto es donde se almacena las versiones anteriores de un proyecto, en este caso se almacena en la red. Cada repositorio debe manejar prioridades (leer, escribir, etc). 
Un repositorio local, es donde se almacenan las versiones anteriores de un proyecto dentro del ordenador que estemos trabajando.
*Fuente*: [Enlace](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)

**d. Copia de trabajo / Working Copy**
Es la copia de una versión del proyecto, la cual se almacena en el disco para que se pueda usar o modificar por el usuario, los cuales aun no han sido añadidos para preparar el commit.
*Fuente*: [Enlace](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

**e. Área de Preparación / Staging Area**
Es un archivo generalmente almacenado en el directorio de git, el cual almacena la información que contendrá la siguiente confirmación (commit)
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

**f. Preparar Cambios / Stage Changes**
Prepara los archivos que fueron modificados o cambiados, dejándolos en el área de preparación.
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

**g. Confirmar cambios / Commit Changes**
Confirma los cambios realizados en un archivo tal y como están en el área de preparación y luego instancias efectuadas de forma permanente en el directorio
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

**h. Commit**
Indica que guardará los cambios realizados en los archivos almacenados en el repositorio local.
*Fuente*: [Enlace](https://www.git-tower.com/learn/git/commands/git-commit)

**i. Clone** 
Copia un repositorio git ya existente, ya sea remoto o local, el cual almacena todos los archivos de cada historia del proyecto.
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

**j. Pull** 
Descarga los cambios realizados en un repositorio remoto uniendola con el branch que se esté utilizando.
*Fuente*: [Enlace](https://git-scm.com/docs/git-pull)

**k. Push** 
Envía los archivos (proyecto) que se han modificado en el repositorio local hacia el repositorio remoto, en el cual se actualizarán los datos.
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

**l. Fetch** 
Recupera la información que se ha enviado al repositorio remoto desde la última vez que fue clonado o utilizado el comando fetch, además la diferencia con pull es que los datos bajados se almacenan en el repositorio local, pero no se agregan al branch actual (fusión entre ramas recuperadas y actual).
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

**m. Merge**
Fusiona el encabezados de la ramas recuperadas con el branch actual, se ocupa cuando se descargan los cambios de un repositorio y se desea trabajar en ellos.
*Fuente*: [Enlace](https://git-scm.com/docs/git-pull)

**n. Status**
Muestra el estado en el que se encuentra el árbol de trabajando, en el cual se pueden ver los archivos que están listos para hacer añadidos en el siguiente commit o posiblemente archivos que aún no hemos añadido.
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

**o. Log**
Historial o registro de los commits que se han realizado en un repositorio específico y el contenido de sus mensajes.
*Fuente*: [Enlace](https://git-scm.com/docs/git-log)

**p. Checkout**
Sirve para crear nuevas ramas o ir alternando entre ellas, además sirve para restaurar archivos que han sido modificados de un árbol de trabajo.
*Fuente*: [Enlace](https://guide.freecodecamp.org/git/git-checkout/)

**q. Rama / Branch**
Una rama es una línea de desarrollo donde un commit apunta al commit anterior (padre) y así sucesivamente, además los lleva los metadatos del autor y el contenido preparado. Se ocupa para que los commit de distintos usuarios que estén trabajando no se topen el uno con el otro.
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-¿Qué-es-una-rama%3F)

**r. Etiqueta / Tag**
Sirven para identificar y marcar las distintas versiones de un proyecto que se ha creado o modificado, el cual ayudará a buscar las versiones a través de un tag y no por su código.
*Fuente*: [Enlace](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)




