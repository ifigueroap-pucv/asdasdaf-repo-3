# Glosario
## A. Control de versiones (VC)
El control de versiones es básicamente un sistema que va registrando todos los cambios que se vayan haciendo en ya sea un archivo o conjunto de archivos. Esto nos permite poder revertir los archivos a un estado anterior, por ejemplo si alguien trabaja con un archivo en el cual nosotros también trabajábamos y no lo entendemos después de que haya sido modificado por la otra persona, podremos revertir a una versión donde nosotros hayamos llegado. Por otro lado también sirve para ver las modificaciones y quien las hace, incluso si se pierden o dañan archivos se podrán recuperar gracias al control de versiones.
Fuente: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
## B. Control de versiones distribuido (DVC)
El mejor ejemplo para definir este es git, donde consta de un servidor, entonces los clientes que estén usando el control de versiones replican todo el repositorio, con esto se logra de que si hay alguna caída del sistema-servidor , los usuarios podrán seguir trabajando, tener un backup y además poder copiar el repositorio del cliente en el servidor base para poder así restaurarlo.
Fuente: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
## C. Repositorio remoto y Repositorio local
En simples palabras el repositorio remoto son los cambios que vamos subiendo a internet o algún punto de la red y el repositorio local es en nuestro computador.
Para entrar en más detalles en el repositorio remoto, podemos tener varios en la red , ya sea de solo lectura o lectura/escritura, entonces cuando vayamos trabajando en nuestros proyectos/tareas y se quieran añadir los cambios al repositorio remeto se deberá actualizar a través de ya sean push o pull. Por otro lado el repositorio local, como bien se decía es donde trabajamos y vamos agendando los cambios en los 3 estados , estado inicial donde están todos nuestros archivos , estado intermedio donde tenemos el almacén llamado Index o Stage donde actúa de memoria cache y va registrando los cambios antes de que los guardemos en el repositorio final , donde finalmente es llevado al head y se podrá ya actualizar en el repositorio remoto.
Fuente: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/

## D. Copia de trabajo / Working Copy
Se refiere a que si uno quiere hacer una copia de lo que lleva de trabajo se puede hacer mediante el comando checkout así mismo si se quiere eliminar la copia de trabajo se puede mediante el comando release.
Fuente: https://www.osmosislatina.com/cvs_info/wcopy.htm


## E. Área de Preparación / Staging Area
Al agregar nuevos archivos a través del directorio de trabajo, pasan al área de preparación, donde es básicamente un área de confirmación de cambios. Por ejemplo si agregamos un archivo-cambio pasan al área de preparación y quedan esperando la confirmación a través del comando git commit, entonces si por ejemplo agregáramos un archivo por equivocación o simplemente nos arrepentimos, podemos removerlo del área de preparación a través de un reset o bien si queremos agendar bien el cambio/agregado se usa git commit como se decía anteriormente.
Fuente: https://networkfaculty.com/es/video/detail/2604-git---area-de-preparacion-o-indice---standing-area

## F. Preparar Cambios / Stage Changes
Estado en el cual se hacen acciones, ya sean agregar archivos, modificarlos o borrarlos , cuando hace uno de estos se entra en el Stage Changes, ya que se queda esperando a que llegue un commit de confirmación para después seguir a la siguiente etapa.
Fuente: https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git

## G. Confirmar cambios / Commit Changes
Cuando por ejemplo se agrega un archivo, se usa el comando commit para así hacer una confirmación del archivo. Entonces cada vez que vayamos haciendo cambios deberemos pasar por una etapa de commit Changes para así hacer la confirmación de los cambios realizados.
Fuente: https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git

## H. Commit
Comando de git el cual se guardan los cambios hechos a modo local o sea en nuestro repositorio local (nuestro computador).
Fuente: https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git

## I. clone
Se puede empezar un proyecto en git de dos maneras, una es tomando un proyecto o directorio ya existente e importarlo en git y por otro lado se puede clonar un repositorio Git existente desde un servidor. En este caso el comando clone sirve para el segundo caso el cual clona todo, en este se descargará toda la información del repositorio que queramos y podremos empezar a trabajar.
Fuente: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git


## J. pull
Comando de git, con el cual se actualiza nuestro repositorio remoto con la información que tengamos en nuestro Head, esto quiere decir básicamente que no solo se descargara los datos del repositorio remoto, sino que se integran los datos de nuestro repositorio local junto a los del repositorio remoto. Esto puede traer algunas consecuencias, ya que por ejemplo si hicimos una función de sumar y nuestros compañeros también la hicieron se dará un conflicto ya que el codigo estará repetido.
Fuente: https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull


## K. push
Al contrario del comando Commit, el comando push es un comando que sube los cambios hechos en tu ambiente de trabajo ya sean a una rama propia o rama remota del equipo de trabajo con el cual se está trabajando, por lo tanto este comando trabaja a nivel de repositorio.
Fuente: https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git

## L. fetch
Comando de git el cual sirve para descargar nuevos datos del repositorio remoto, pero la información nueva descarada no se integra con los datos de nuestro repositorio local, asi con esto podemos ver que cosas se han hecho en el repositorio local.
Fuente: https://www.git-tower.com/learn/git/faq/difference-between-git-fetch-git-pull

## M. merge
Comando de git, para fusionar ramas, se debe indicar la rama de origen y donde queremos mesclar los datos, cabe destacar que se pueden generar conflictos, git usualmente esta programado para hacer frente a estos conflictos.
Fuente: https://www.youtube.com/watch?v=QGKTdL7GG24


## N. status
Comando de git el cual muestras el estado del directorio de trabajo y área de preparación, nos deja ver que cambios hemos hecho y cuales no, ya sean por ejemplo archivos nuevos agregados , commits hechos , entre otros.
Fuente: https://www.atlassian.com/git/tutorials/inspecting-a-repository
## O. log
Comando de git para examinar el registro de commits hechos, si se usa este comando se muestra un poco del codigo del commit, el autor y una descripción que haya hecho el autor sobre el commit en cuestión.
Fuente: https://www.youtube.com/watch?v=QGKTdL7GG24

## P. checkout
Comando de git que tiene como función valga le redundancia chequear ya sean archivos, commits y branches(ramas). Por lo investigado usualmente se usan más en los bronces, ya sea para ver los cambios hechos en los archivos del directorio de trabajo en la rama que se quiera, con esto podemos ver que esta pasando en cada rama y además también navegar de rama en rama.
Fuente: https://www.atlassian.com/git/tutorials/using-branches/git-checkout
## Q. Rama / Branch
El flujo de trabajo de git esta basado en ramas, por lo tanto vamos de commit en commit hasta llegar al commit principal, las ramas nos dan la ventaja de por ejemplo si estamos trabajando varias personas, cada persona podrá trabajar en una rama diferente, para así separar el trabajo y no trabajar todos en la misma rama.
Fuente: https://www.youtube.com/watch?v=QGKTdL7GG24

## R. Etiqueta / Tag
En git podemos darles nombres/alias a los commits, con esto en vez de recordar un commit en concreto por su codigo, podemos crearle un tag con un nombre más sencillo, fácil y global, con tal de verlo y saber a que corresponde el commit en cuestión.
Fuente: https://www.youtube.com/watch?v=QGKTdL7GG24&t=383s


