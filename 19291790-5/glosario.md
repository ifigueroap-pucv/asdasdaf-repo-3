
#Glosario Ingenier�a Web Taller N�0: Git y Bitbucket

## Control de versiones (VC)
Sistema que registra los cambios realizados sobre uno o varios archivos a trav�s del tiempo. De esta forma se evita destruir trabajo anterior, y a su vez recuperar versiones anteriores en un periodo de desarollo posterior.
[1],[2]


## Control de versiones distribuido (DVC)
Sistema que no hace uso de un servidor central, sino al contrario, cada programador posee una copia completa del repositorio en su equipo. As�, se permite trabajar simult�neamente ya que cuando un programador realice alg�n cambio, este se propagara a todos los dem�s nodos.
[3],[4]

## Repositorio remoto y Repositorio local
### Repositorio remoto
Son versiones de un proyecto que se encuentran alojadas en Internet o alg�n punto de la red.
[5],[6]

 
### Repositorio local 
El repositorio local no necesita de internet, por tanto todos los cambios se almacenaran localmente. Sin embargo, si se pueden enviar los cambios al servidor.
[7],[8]


## Copia de trabajo / Working Copy
Se refiere a los clones que terceros realizan del repositorio, por lo que pueden existir muchas copias al mismo tiempo, y cada qui�n puede trabajar sobre ellas. Todos los cambios o modificaciones realizadas, pueden ser "enviados" de regreso al repositorio, de modo que se mantiene el registro de qu� cambios se realizaron y por qui�n.
[9],[10]

## �rea de Preparaci�n / Staging Area
En esta �rea se preparan los archivos que ser�n actualizados finalmente en el repositorio, en otras palabras, es un tipo de fase intermedia antes de realizar el "push" (Entre el espacio destinado a proporcionar los datos y el que los asegurar�).
[11],[12]

## Preparar Cambios / Stage Changes
Es la preparaci�n para realizar un commit, haciendo uso del Staging Area donde Git contendr� todos los cambios que ser�n parte del pr�ximo commit.
[13],[14]

## Confirmar cambios / Commit Changes
Son los cambios que se han realizado luego de una actualizaci�n en el �rea de preparaci�n. Se lleva a cabo mediante el comando "git commit", en este se especificar� el metadata del objeto sobre el cual se esta trabajando.
[15],[16]

## Commit
Comando para guardar cambios en el repositorio local (�rea de preparaci�n). Es necesario especificar a Git que cambios se desean incluir en el commit antes de utilizar el comando. Lo anterior indica que un archivo que recibi� cambios no ser� incluido autom�ticamente en el pr�ximo commit; en cambio primero se deber�a usar git add para incluir dicho archivo.
[17]

## clone
Comando para clonar o copiar un repositorio objetivo dentro de otra carpeta en la m�quina anfitriona. En base a aquello, todo el contenido perteneciente al repositorio objetivo ser� clonado al almac�n creado previamente.  
[18]

## pull
Comando para descargar datos; seguido de la actualizaci�n de estos mismos, en el respectivo banco de datos. Abreviaci�n de git fetch, seguido de git merge. Git fetch trae los cambios pero los deja en otro branch (rama), hasta que se hace el git merge para traerlos al branch local.
[19]

## push
Comando para subir datos desde un repositorio local a uno remoto, vendr�a a ser la contraparte de git fetch, no obstante fetch importa los commits a los branches locales, mientras que pushing, exporta los commits a ramas remotas.
[20]

## fetch
Comando para descargar datos, obtiene los cambios sin hacerles merge localmente. Dichos cambios se bajar�n a la rama oculta origin/master (es necesario usar comandos espec�ficos, es posible encontrarlos en la documentaci�n de Git).
[21]

## merge
Realiza la solicitud de un resultado respecto a una rama definida previamente, siempre y cuando el trabajo haya sido finalizado. En otras palabras es la acci�n de unir dos o mas ramas de desarrollo. Existe la posibilidad de que existan conflictos en la solicitud o se necesite verificar cambios, para esto ser�a necesario verificar la solicitud localmente y vincularla usando la linea de comandos. Siempre existiendo la posibilidad de cancelar la solicitud.
[22]

## status
Comando que muestra el estado del directorio de trabajo y/o �rea de preparaci�n (Staging Area). Permite conocer que cambios han sido "preparados" (staged) y cuales no, ademas de que archivos no han sido rastreados (tracked) por Git. Documenta informaci�n significativa respecto a lo que ha sido creado y modificado. Este comando no entrega informaci�n respecto a la historia de "compromisos" (commit) con el proyecto.
[23]

## log
Comando que muestra los cambios que se han realizado en un repositorio a lo largo del tiempo. Si no se traspasan arugmentos, git log mostrar� los cambios realizados en orden cronol�gico inverso. En otro caso, ser� necesario indicar que datos se requieren.
[24]

## checkout
Comando que permite cambiar ramas (branches) o restaurar archivos del �rbol de trabajo. Trabaja sobre tres entidades las cuales pueden ser: archivos, commits, y ramas.
[25]

## Rama / Branch
La mayor�a de los sistemas de control de versiones cuentan con alg�n soporte de ramificaci�n; estos corresponden a un sector espec�fico de un proyecto. Aquello significa que no existe interferencia entre el trabajo que se esta realizando y el que se encuentra ya establecido (master � subido al repositorio). En mucho casos la copia de un repositorio completo demanda mucho procesamiento, resultando en per�odos de ejecuci�n poco �ptimos. Por concesi�n se infiere que se esta trabajando sobre proyectos de alta envergadura. Es por esto la necesidad de dominar esta herramienta, ya que de este modo se desarrollar� de la forma mas �ptima.
[27][28]

## Etiqueta / Tag
Funcionalidad que sirve para marcar las versiones de lanzamientos. Existen dos tipos principales de etiquetas: ligeras y anotadas. 
[28]

### Etiqueta ligera
Es muy parecido a una rama que no cambia (simplemente es un puntero a un commit especifico). 

### Etiqueta anotada
Estas se guardan en la base de datos de Git como objetos enteros.

[1]: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
[2]: http://profesores.elo.utfsm.cl/~agv/elo330/2s03/projects/CVS/CVS.PDF
[3]: http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/
[4]: https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones
[5]: https://www.ecodeup.com/instala-y-crea-tu-primer-repositorio-local-con-git-en-windows/
[6]: https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos
[7]: http://www.syncrosvnclient.com/software-archive/InstData4.2/doc/ug-syncroSVNClient/define-working-copy.html
[8]: https://versionsapp.com/documentation/about_svn_kc_workingcopies.html
[9]: http://josepcurto.com/2007/10/15/que-es-una-staging-area/
[10]: https://support.timextender.com/hc/en-us/articles/210438083-What-is-a-Data-Staging-Area-
[11]: https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git
[12]: https://community.atlassian.com/t5/Sourcetree-questions/Staged-vs-Unstaged/qaq-p/127916
[13]: https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository
[14]: https://www.atlassian.com/git/tutorials/saving-changes
[15]: https://www.git-tower.com/learn/git/commands/git-commit
[16]: https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone
[17]: https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git
[18]: https://www.atlassian.com/git/tutorials/syncing/git-push
[19]: https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git
[20]: https://help.github.com/articles/merging-a-pull-request/
[21]: https://www.atlassian.com/git/tutorials/inspecting-a-repository
[22]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones
[23]: https://www.atlassian.com/git/tutorials/using-branches/git-checkout
[24]: https://git-scm.com/book/es/v2/Ramificaciones-en-Git-Gesti%C3%B3n-de-Ramas
[25]: https://www.adictosaltrabajo.com/tutoriales/git-branch-bash/
[26]: https://git-scm.com/book/es/v2/Ramificaciones-en-Git-Gesti%C3%B3n-de-Ramas
[27]: https://www.adictosaltrabajo.com/tutoriales/git-branch-bash/
[28]: https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado



