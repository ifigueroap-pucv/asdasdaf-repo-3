#Glosario
##Juan Carlos Aguirre




######a. Control de versiones (VC): El sistema de control de versiones es un software que admnistra el acceso a un conjunto de ficheros, y mantiene un historia de cambios realizados.[Link](https://www.ecured.cu/Sistemas_de_control_de_versiones)
######b. Control de versiones distribuido (DVC): Permiten gestionar los cambios realizados en el c�digo fuente de programas o documentos en el que cada cliente mantiene su propia copia completa del repositorio y puede trabajar sin estar conectado al servidor[Link](http://www.mclibre.org/consultar/informatica/lecciones/git.html)
######c. Repositorio remoto y Repositorio local: Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en alg�n punto de la red. Puedes tener varios, cada uno de los cuales puede ser de s�lo lectura, o de lectura/escritura, seg�n los permisos que tengas. Mientras que el local es el que tienes en tu ordenador [Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
######d. Copia de trabajo / Working Copy: Cada copia de trabajo funciona como una copia de seguridad remota del codebase y de su historial de cambios, protegi�ndolo de la p�rdida de datos[Link](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)
######e. �rea de Preparaci�n / Staging Area: Es un archiv ubicado en el directorio Git donde guarda informaci�n sobre todo lo que haremos en el siguiente commit[Link](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)
######f. Preparar Cambios / Stage Changes: Es donde ser alamacenan los archivos que estan sujetos a cambios[Link](https://githowto.com/staging_changes)
######g. Confirmar cambios / Commit Changes:Confirma los cambios que tenemos en el staging area,informaci�n, se realiza un commit, para que todos los cambios efectuados en la copia, se agreguen al repositorio. [Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
######h. Commit: El comando commit es usado para cambiar a la cabecera[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
######i. clone: Comando utilizado si deseas obtener una copia de un repositorio existente [Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
######j. pull: Comando utilizado para poder fusionar todos los cambios que se han hecho en el repositorio local trabajando[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
######k. push: Comando push env�a los cambios que se han hecho en la rama principal de los repertorios remotos que est�n asociados con el directorio que est� trabajando[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
######l. fetch: Este comando le permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local que est� trabajando[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
######m. merge: Este comando se usa para fusionar una rama con otra rama activa[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
######n. status: Este comando muestra la lista de los archivos que se han cambiado junto con los archivos que est�n por ser a�adidos o comprometidos.[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
######o. log: Ejecutar este comando muestra una lista de commits en una rama junto con todos los detalles[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
######p. checkout: El comando checkout se puede usar para crear ramas o cambiar entre ellas[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
######q. Rama / Branch: Este comando se usa para listar, crear o borrar ramas.[Link](https://www.hostinger.es/tutoriales/comandos-de-git)
######r. Etiqueta / Tag: Etiquetar se usa para marcar commits espec�ficos con asas simples[Link](https://www.hostinger.es/tutoriales/comandos-de-git)